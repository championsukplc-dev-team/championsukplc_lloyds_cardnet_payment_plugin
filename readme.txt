=== Lloyds Cardnet ===
Contributors: Champions UK plc
Tags: e-commerce, store, sales, sell, woo, shop, cart, checkout, downloadable, downloads, payments, paypal, storefront, Lloyds, Cardnet, woo commerce
Requires at least: 5.5
Tested up to: 6.1
Requires PHP: 7.4
Stable tag: 4.0.6
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

A plugin created for Lloyds Cardnet by Champions UK plc on behalf of Lloyds Cardnet.

== Description ==

A plugin created for Lloyds Cardnet by Champions UK plc on behalf of Lloyds Cardnet.