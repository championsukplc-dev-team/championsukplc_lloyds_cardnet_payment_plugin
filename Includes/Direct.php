<?php

/**

 ***
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\IndexController.
 */

namespace ChukplcCardnet\Includes;

use ChukplcCardnet\ChukplcCardnet;
use ChukplcCardnet\Includes\Utilities\Session\PaymentSession;
use ChukplcCardnet\Includes\Utilities\Services\PaymentsService;
use ChukplcCardnet\Includes\Utilities\Services\LookupService;
use ChukplcCardnet\Includes\Utilities\MainUtility;

class CardnetDirect
{
    /**
     * completePayment function
     *
     * @param [type] $transaction
     * @param [type] $wcCardnet
     * @return void
     */
    public static function completePayment(&$transaction, &$wcCardnet)
    {
        if ($transaction->lloyds3d_gone_to_gateway && ((get_query_var('md') && get_query_var('PaRes')) || get_query_var('cres'))) {
            $returnRespObj = $wcCardnet->getLloydsCardWebServObject();
            $returnRespObj->Confirm3DResponse($transaction->lloyds_response_transaction_ref, get_query_var('md'), get_query_var('PaRes'), get_query_var('cres'), $wcCardnet->buildAddress('shipping'), $wcCardnet->buildAddress());
            if ($returnRespObj->TransactionResult === 'APPROVED' && (((str_starts_with($returnRespObj->ApprovalCode, 'Y:') && (str_ends_with($returnRespObj->ApprovalCode, ':Authenticated'))) || (str_ends_with($returnRespObj->ApprovalCode, ':Attempt'))) || strpos($returnRespObj->ApprovalCode, 'ECI7') !== false) || !str_starts_with($returnRespObj->getApprovalCode(), '?:waiting')) {
                $paymentDescription = str_replace([':'], ' ', $transaction->service_information);

                $confirmpaymentRespObj = $wcCardnet->getLloydsCardWebServObject();
                $confirmpaymentRespObj->Confirm3DPayment(
                    $transaction->lloyds_response_transaction_ref,
                    $wcCardnet->getLloydsCardWebServObject(),
                    $paymentDescription,
                    $transaction->lloyds_transaction_time,
                    $wcCardnet->getLloydsCardConfigObject()->store_id,
                    $wcCardnet->buildAddress('shipping')
                );

                if ($confirmpaymentRespObj->TransactionResult === 'APPROVED' && str_starts_with($returnRespObj->ApprovalCode, 'Y:')) {
                    $transaction->lloyds_pay_response = "success";
                    ChukplcCardnet::add_championsukplc_notification("Payment Successful," . $confirmpaymentRespObj->ErrorMessage . ". Thank you. ");
                    $transaction->lloyds_response_transaction_status = $confirmpaymentRespObj->TransactionResult . '-' . $confirmpaymentRespObj->ApprovalCode;
                } else if ($confirmpaymentRespObj->TransactionResult === 'APPROVED' && str_starts_with($returnRespObj->ApprovalCode, '?:waiting')) {
                    $transaction->lloyds_pay_response = "failed";
                    ChukplcCardnet::add_championsukplc_notification("Payment Not Successful, and our tech team has been notified. Thank you. ", '1');
                    $transaction->lloyds_response_transaction_status = $confirmpaymentRespObj->TransactionResult . '-' . $confirmpaymentRespObj->ApprovalCode;
                } else {
                    $transaction->lloyds_pay_response = "failed";
                    ChukplcCardnet::add_championsukplc_notification("Payment Not Successful, and our tech team has been notified. Thank you. ", '1');
                    $transaction->lloyds_response_transaction_status = $confirmpaymentRespObj->TransactionResult . '-' . $confirmpaymentRespObj->ApprovalCode;
                }
            } else {
                $transaction->lloyds_pay_response = "failed";
                ChukplcCardnet::add_championsukplc_notification("Payment Not Successful and our tech team has been notified. Thank you.", '1');
                $transaction->lloyds_response_transaction_status = $returnRespObj->TransactionResult . '-' . $returnRespObj->ApprovalCode;
            }
        }

        if (!isset($transaction->lloyds_pay_response)) {
            $transaction->lloyds_pay_response = 'failed';
        }
    }


    /**
     * processPayment function
     *
     * @param [type] $transaction
     * @param [type] $wcCardnet
     * @param [type] $redirectUrl
     * @return void
     */
    public static function processPayment(&$transaction, $wcCardnet, $redirectUrl)
    {
        $order = wc_get_order($transaction->service_id);
        $payment_set_for_redirection = true;
        $errorMessage = '';

        $lloydsCardWebServ = $wcCardnet->getLloydsCardWebServObject();

        $creditCardNumber = sanitize_text_field(str_replace(array(' ', '-'), '', $_POST['chukplccardnet-card-number']) ?? '');
        $expDate = sanitize_text_field($_POST['chukplccardnet-card-expiry'] ?? '');
        $cvv2 = sanitize_text_field($_POST['chukplccardnet-card-cvc'] ?? '');

        $validatorObj = Utilities\ValidatorUtility::getInstance();
        //validate the input and return the customer validation process
        $input_validator = function ($input, $case, $label, $required_or_not, $min_lenght = null, $max_lenth = null, $return_message = null) use (&$errorMessage, &$payment_set_for_redirection, $validatorObj) {
            $resp = $validatorObj->isValid($input, $case, $label, $required_or_not, $min_lenght, $max_lenth, $return_message);

            if (!$resp->result) {
                $payment_set_for_redirection = false;
                $errorMessage .= $resp->message;
            }
        };

		
		//transform expiry date incase client theme doesnt structure it properly
		$expDateLength = strlen($expDate);
		if(!str_contains($expDate, '/')){
			if($expDateLength === 4) {
				//add slash to middle
				$month = substr($expDate, 0, 2);
				$year = substr($expDate, 2, 2);
				$expDate = $month . '/' . $year;
			}
			if($expDateLength < 4) {
                $payment_set_for_redirection = false;
                ChukplcCardnet::add_championsukplc_notification(("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY"), 'error');
                throw new \Exception("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY");
			}
			if($expDateLength > 4) {
				if($expDateLength === 6){
					$month = substr($expDate, 0, 2);
					$year = substr($expDate, 4, 2);
					$expDate = $month . '/' . $year;
				} else {
					$payment_set_for_redirection = false;
					ChukplcCardnet::add_championsukplc_notification(("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY"), 'error');
					throw new \Exception("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY");
				}
			}
		} else {
			if($expDateLength > 7 && $expDateLength < 10 ) {
				$expDateParts = explode('/', $expDate);
				$month = trim($expDateParts[0]);
				$year = substr(trim($expDateParts[1]), 2, 2);
				$expDate = $month . '/' . $year;
			}
			if($expDateLength > 9) {
				$payment_set_for_redirection = false;
				ChukplcCardnet::add_championsukplc_notification(("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY"), 'error');
				throw new \Exception("You have supplied invalid inputs! <br />Card Expiry Date must follow this format: MM/YY");
			}
		}
		
        //process direct
        $input_validator($order->get_billing_first_name() . ' ' . $order->get_billing_first_name(), "name", "Name on Card", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($creditCardNumber, "numeric", "Credit Card", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($cvv2, "numeric", "Credit Card CVV2", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($expDate, "none", "Expiry Date", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($order->get_billing_city(), "none", "City", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($order->get_billing_country(), "none", "Country", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($order->get_billing_postcode(), "none", "Post Code", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);
        $input_validator($order->get_billing_address_1() . ' ' . $order->get_billing_address_2(), "none", "Address", $wcCardnet->input_required, $wcCardnet->char_min_lenth, $wcCardnet->char_max_lenth);

        if ($lloydsCardWebServ != null) {
            if (!$payment_set_for_redirection) {
                ChukplcCardnet::add_championsukplc_notification(("You have supplied invalid inputs!" . $errorMessage), 'error');

                PaymentSession::getInstance()->setData($transaction);
                throw new \Exception("You have supplied invalid inputs! <br/>" . $errorMessage);
            }

            $transaction->lloyds_transaction_time = time();

            //lets create the payment records in our database.
            if (!isset($transaction->payment_reference) || !$transaction->payment_reference) {
                $transaction->payment_reference = PaymentsService::getInstance()->createPayment($transaction, Utilities\MainUtility::getUserIp(), $transaction->service_account, 'LloydsCards: ttime:' . $transaction->lloyds_transaction_time);
            }

            if ($transaction->payment_reference) {
                $transaction->payment_gone_to_gateway = true;
            }

            //call the payment lloyds gateway
            $returnObj = $lloydsCardWebServ->Do3DSecure(
                $transaction->payment_reference,
                str_replace([':'], ' ', $transaction->service_information),
                $transaction->service_amount,
                $creditCardNumber,
                $expDate,
                $cvv2,
                $order,
                Utilities\MainUtility::getIsoCurrencyCodeFromCurrencyCode($transaction->service_currency),
                $transaction->lloyds_transaction_time,
                get_site_url(null, WC_Cardnet_Gateway::URL_BASE . WC_Cardnet_Gateway::URL_PROCESS_3DS, __FILE__),
                $wcCardnet->buildAddress('shipping')
            );

            $transaction->lloyds_response_transaction_ref = '';
            $transaction->lloyds_response_transaction_message = '';
            $transaction->lloyds_response_transaction_status = '';
            $transaction->lloyds_response_payer_id = '';

            if (isset($returnObj->TransactionResult)) {
                $transaction->lloyds_response_transaction_ref = $returnObj->getIpgTransactionId();

                if ($returnObj->TransactionResult === 'APPROVED') {
                    if ($returnObj->getSecure3DResponse() && isset($returnObj->getSecure3DResponse()->Secure3DVerificationResponse)) {
                        $verifRedirectResp = $returnObj->getSecure3DResponse()->Secure3DVerificationResponse->VerificationRedirectResponse;

                        $transaction->lloyds3d_c_req = $verifRedirectResp->CReq ?? '';
                        $transaction->lloyds3d_md = $verifRedirectResp->MD ?? '';
                        $transaction->lloyds3d_pa_req = $verifRedirectResp->PaReq ?? '';
                        $transaction->lloyds3d_termurl = $verifRedirectResp->TermUrl ?? '';
                        $transaction->lloyds3d_redirect_url = $verifRedirectResp->AcsURL ?? '';
                        $transaction->lloyds3d_secure = true;
                        $transaction->lloyds3d_gone_to_gateway = true;
                    }

                    if (str_starts_with($returnObj->getApprovalCode(), 'Y:') && (str_ends_with($returnObj->getApprovalCode(), ':Authenticated') || str_ends_with($returnObj->getApprovalCode(), ':Attempt'))) {
                        $transaction->lloyds3d_secure = false;
                        $returnObj = $lloydsCardWebServ->ConfirmAuthenticationTransaction($transaction->lloyds_response_transaction_ref, $wcCardnet->buildAddress(), $wcCardnet->buildAddress('shipping'));
                    } else if (strpos($returnObj->getApprovalCode(), 'waiting 3dsecure') !== false) {
                        if (isset($returnObj->getSecure3DResponse()->Secure3DVerificationResponse)) {
                            $transaction->lloyds_pay_response = "failed";
                        } elseif (isset($returnObj->getSecure3DResponse()->Secure3DMethod)) {
                            $transaction->lloyds3dsframe = true;
                            $transaction->lloyds3dsframedata = $returnObj->getSecure3DResponse()->Secure3DMethod->Secure3DMethodForm;
                            PaymentSession::getInstance()->setData($transaction);
                            return $lloydsCardWebServ->process3DResponse();
                        }
                    } else if (strstr(strtolower($returnObj->getApprovalCode()), 'eci7') || strstr(strtolower($returnObj->getApprovalCode()), 'eci 7')) {
                        return $lloydsCardWebServ->process3DResponse();
                    } else {
                        $transaction->lloyds3d_secure = false;
                        //do normal payment incase the 3d fails
                        $returnObj = $lloydsCardWebServ->DoDirectPayment(
                            $transaction->payment_reference,
                            str_replace([':'], ' ', $transaction->service_information),
                            $transaction->service_amount,
                            $creditCardNumber,
                            $expDate,
                            $cvv2,
                            $order,
                            Utilities\MainUtility::getIsoCurrencyCodeFromCurrencyCode($transaction->service_currency),
                            $transaction->lloyds_transaction_time,
                            $wcCardnet->buildAddress('shipping')
                        );
                    }
                }

                if (is_object($returnObj)) {
                    $transaction->lloyds_response_transaction_ref = $returnObj->getIpgTransactionId();
                    $transaction->lloyds_response_transaction_message = $returnObj->ErrorMessage;
                    $transaction->lloyds_response_transaction_status = $returnObj->getTransactionResult() . '-' . $returnObj->getApprovalCode();
                    $transaction->lloyds_response_payer_id = $returnObj->getOrderId();
                }

                //references to payment gateway for confirmation
                if ($returnObj->getTransactionResult() === 'APPROVED') {
                    $transaction->lloyds_pay_response = "success";
                    ChukplcCardnet::add_championsukplc_notification("Payment Successful");
                    $extra_subject = "Payment Success";
                } else {
                    $transaction->lloyds_pay_response = "failed";
                    ChukplcCardnet::add_championsukplc_notification("Payment was not successful", 'error');
                    $extra_subject = "Payment Failure";
                }
            } else {
                //Display a user friendly Error on the page using any of the following error information returned by Lloyds Cards Net
                ChukplcCardnet::add_championsukplc_notification(("Error occurred (Payment Failed) while trying to access merchant. Lloyds Cards is in-accessible now, our support team has been notified, please bear with us, Thank you. "), 'error');
                $extra_subject = "Error";
            }

            PaymentSession::getInstance()->setData($transaction);
            return ['result' => ((strstr($extra_subject, 'Success')) ? 'success' : 'failure'), 'redirect' => $redirectUrl];
        }
    }
}
