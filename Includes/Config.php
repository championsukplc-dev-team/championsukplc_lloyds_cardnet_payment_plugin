<?php

/**

 ***
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\IndexController.
 */

namespace ChukplcCardnet\Includes;

use ChukplcCardnet\Includes\WC_Cardnet_Gateway;

class CardnetConfig
{
    protected $mode;
    protected $id;
    protected $store_id;
    protected $user_id;
    protected $password;
    protected $cert_password;


    /**
     * __construct function
     *
     * @param [type] $settings
     */
    public function __construct($settings)
    {
        $this->mode = $settings['sel_mode'];
        $this->id = ($settings->mode === WC_Cardnet_Gateway::ENVIRONMENT_LIVE) ? 2 : 1;
        $this->store_id = $settings[strtolower($settings->mode) . '-store-id'];
        $this->user_id = $settings[strtolower($settings->mode) . '-user-id'];
        $this->password = $settings[strtolower($settings->mode) . '-user-password'];
        $this->cert_password = $settings[strtolower($settings->mode) . '-certificate-password'];

        return $this;
    }


    public function __get($name)
    {
        return $this->$name;
    }
}
