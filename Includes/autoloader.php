<?php

spl_autoload_register('ChukplcCardnet_autoloader');
function ChukplcCardnet_autoloader($class_name)
{
    if (false !== strpos($class_name, 'ChukplcCardnet')) {
        $class_file = ltrim(str_replace("\\", "/", $class_name), 'ChukplcCardnet/Includes') . '.php';

        if (file_exists(__DIR__ . '/' . $class_file)) {
            require_once __DIR__ . '/' . $class_file;
        }
    }
}
