<?php

/**

 ***
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\IndexController.
 */

namespace ChukplcCardnet\Includes;

use ChukplcCardnet\Includes\WC_Cardnet_Gateway;
use ChukplcCardnet\ChukplcCardnet;
use ChukplcCardnet\Includes\Utilities\Session\PaymentSession;
use ChukplcCardnet\Includes\Utilities\Services\PaymentsService;
use ChukplcCardnet\Includes\Utilities\MainUtility;

class ThreeDS extends WC_Cardnet_Gateway
{
    protected $paymentSessionObj;
    protected $termURL;



    /**
     * __construct function
     */
    public function __construct()
    {
        parent::__construct();

        $this->paymentSessionObj = PaymentSession::getInstance();
        self::$transactionObj = $this->paymentSessionObj->getData();
        $this->termURL = get_site_url(null, WC_Cardnet_Gateway::URL_BASE . WC_Cardnet_Gateway::URL_PROCESS_3DS, __FILE__);

        $this->controller();
    }



    /**
     * controller function
     *
     * @return void
     */
    public function controller()
    {
        if (self::$transactionObj->lloyds3dsframe === true) {
            echo self::$transactionObj->lloyds3dsframedata;
            echo '<script type="text/javascript">
                    var confirmResponseUrl = "' . site_url('chukplc-cardnet/confirmation/') . '";
                    setTimeout(function(){ window.location = confirmResponseUrl; }, 3000);
                </script>';
        } else if (self::$transactionObj->lloyds3d_redirect_url && ((self::$transactionObj->lloyds3d_pa_req && self::$transactionObj->lloyds3d_md) || self::$transactionObj->lloyds3d_c_req)) {
            echo $this->process3DResponse(
                self::$transactionObj->lloyds3d_redirect_url,
                $this->termURL,
                self::$transactionObj->lloyds3d_c_req,
                self::$transactionObj->lloyds3d_pa_req,
                self::$transactionObj->lloyds3d_md
            );
            die;
        }
    }



    /**
     * process3DResponse function
     *
     * @param string $AcsURL - URL to authenticate with
     * @param string $TermUrl - Return URL
     * @param string $CReq
     * @param string $paReq
     * @param string $md
     * @return void
     */
    public function process3DResponse($AcsURL, $TermUrl, $CReq = '', $paReq = '', $md = '')
    {
        $formToSend = "<form id='lloydscardsnet3d-form' name='lloydscardsnet3d-form' action='$AcsURL' method='post'>";
        $formToSend .= "<input type='hidden' name='TermUrl' value='$TermUrl'/>";

        if ($CReq) {
            $formToSend .= "<input type='hidden' name='creq' value='$CReq'/>";
        }
        if ($paReq) {
            $formToSend .= "<input type='hidden' name='PaReq' value='$paReq'/>";
        }
        if ($md) {
            $formToSend .= "<input type='hidden' name='MD' value='$md'/>";
        }

        $formToSend .= "<input type='submit' id='formButton' value='proceed to complete payment' />
        </form><script type='text/javascript'>document.forms['lloydscardsnet3d-form'].submit();</script>";
        $MainUtility = MainUtility::getInstance();
        $MainUtility::addLog('3DS Form Payment Request/Response Start' . PHP_EOL);
        $MainUtility::addLog($formToSend, true);
        $MainUtility::addLog('3DS Form Payment Request/Response End' . PHP_EOL);
        session_regenerate_id(true);
        session_write_close();
        return $formToSend;
    }



    /**
     * completePayment function
     *
     * @param [type] $transaction
     * @param [type] $wcCardnet
     * @return void
     */
    public static function completePayment(&$transaction, &$wcCardnet)
    {
        if (get_query_var('cres')) {
            $returnRespObj = $wcCardnet->getLloydsCardWebServObject();
            $response = $returnRespObj->Confirm3DResponse($transaction->lloyds_response_transaction_ref, get_query_var('md'), get_query_var('PaRes'), get_query_var('cres'), $wcCardnet->buildAddress('shipping'), $wcCardnet->buildAddress());
            MainUtility::addLog('confirm3dresponse', true);
            MainUtility::addLog($response, true);

            MainUtility::addLog('transResult', true);
            MainUtility::addLog($transaction, true);

            if ($response->TransactionResult === 'APPROVED' && (((str_starts_with($response->ApprovalCode, 'Y:') && (str_ends_with($response->ApprovalCode, ':Authenticated'))) || (str_ends_with($response->ApprovalCode, ':Attempt'))) || strpos($response->ApprovalCode, 'ECI7') !== false)) {
                $paymentDescription = str_replace([':'], ' ', $transaction->service_information);

                $returnRespObj = $wcCardnet->getLloydsCardWebServObject();
                $returnRespObj->Confirm3DPayment(
                    $transaction->lloyds_response_transaction_ref,
                    $transaction->payment_reference,
                    $paymentDescription,
                    $transaction->lloyds_transaction_time,
                    wc_get_order($transaction->service_id),
                    $wcCardnet->buildAddress('shipping')
                );

                if ($response->TransactionResult === 'APPROVED' && str_starts_with($response->ApprovalCode, 'Y:')) {
                    $transaction->lloyds_pay_response = "success";
                    ChukplcCardnet::add_championsukplc_notification("Payment Successful," . $returnRespObj->ErrorMessage . ". Thank you. ");
                    $transaction->lloyds_response_transaction_status = $returnRespObj->TransactionResult . '-' . $returnRespObj->ApprovalCode;
                } else {
                    $transaction->lloyds_pay_response = "failed";
                    ChukplcCardnet::add_championsukplc_notification("Payment Not Successful, and our tech team has been notified. Thank you. ", '1');
                    $transaction->lloyds_response_transaction_status = $returnRespObj->TransactionResult . '-' . $returnRespObj->ApprovalCode;
                }
            } else {
                $transaction->lloyds_pay_response = "failed";
                ChukplcCardnet::add_championsukplc_notification("Payment Not Successful and our tech team has been notified. Thank you.", '1');
                $transaction->lloyds_response_transaction_status = $returnRespObj->TransactionResult . '-' . $returnRespObj->ApprovalCode;
            }
        }

        if (!isset($transaction->lloyds_pay_response)) {
            $transaction->lloyds_pay_response = 'failed';
        }
    }


    /**
     * Undocumented function
     *
     * @param [type] $cres
     * @return void
     */
    public static function getValueFromCres($value)
    {
        $cres = json_decode(base64_decode(get_query_var('cres')));
        return $cres->$value;
    }


    /**
     * paymentCompletion function
     *
     * @return void
     */
    public function paymentCompletion()
    {
        if (isset(self::$transactionObj->payment_gone_to_gateway) && self::$transactionObj->payment_gone_to_gateway) {
            $order = wc_get_order(self::$transactionObj->service_id);
            $processing = false;

            if (self::$transactionObj->service_payment_method->id == 8) {
                //Lloyds
                Utilities\MainUtility::addLog($_REQUEST, true);

                if ($this->settings['sel_type'] === self::PAYMENT_DIRECT) {
                    self::completePayment(self::$transactionObj, $this);
                }

                $paymentObj = PaymentsService::getInstance()->getPaymentByReference(self::$transactionObj->payment_reference);

                $new_data = [
                    'remote_message' => $paymentObj->remote_message . ' ' . self::$transactionObj->lloyds_response_transaction_message,
                    'remote_status_or_code' => self::$transactionObj->lloyds_response_transaction_status,
                    'remote_payer_id' => self::$transactionObj->lloyds_response_payer_id,
                    'status' => self::$transactionObj->lloyds_pay_response == "success" ? 'SUCCESSFUL' : 'FAILED'
                ];

                if (self::$transactionObj->lloyds_pay_response == "success") { //success processing
                    $processing = true;
                    $new_data['remote_reference'] = self::$transactionObj->remote_reference;
                }

                PaymentsService::getInstance()->update($new_data, $paymentObj->id);
            }

            if ($processing) {
                self::$transactionObj->payment_status = ($processing === 'pending') ? "pending" : "success";

                $prefix = (self::$transactionObj->payment_status === 'pending') ? 'Pending' : 'Successful';

                // Mark as on-hold (we're awaiting the payment)
                if ($processing === 'pending') {
                    $order->update_status('on-hold', __($prefix . ' payment confirmation', $this->id));
                } else {
                    $order->update_status('processing', __('Payment ' . $prefix, $this->id));
                    wc_reduce_stock_levels(self::$transactionObj->service_id); // Reduce stock levels
                    WC()->cart->empty_cart(); // Remove cart
                    global $woocommerce;
                    $woocommerce->cart->empty_cart();
                }
            } else {
                ChukplcCardnet::add_championsukplc_notification(("Something went wrong!"), '1');
                self::$transactionObj->payment_status = "failed";
                $order->update_status(self::$transactionObj->payment_status, __('Payment Failed', $this->id));
            }

            PaymentSession::getInstance()->setData(self::$transactionObj);
            if (self::$transactionObj->payment_status === 'pending' || self::$transactionObj->payment_status === "success") {
                return wp_redirect(self::$transactionObj->service_return_url);
            }

            return wp_redirect(self::$transactionObj->service_cancel_url);
        }

        PaymentSession::getInstance()->clearSessionData();
        return wp_redirect("/");
    }



    /**
     * confirmresponse function
     *
     * @return void
     */
    public function confirmresponse()
    {
        if (get_query_var('return') === 'true') {
            $paymentService = PaymentsService::getInstance();
            $paymentSessionObj = PaymentSession::getInstance();
            self::$transactionObj = $paymentSessionObj->getData();

            $ipg_transaction_id = self::$transactionObj->lloyds_response_transaction_ref;
            $lloydsCardsWebServObj = $this->getLloydsCardWebServObject();

            $returnRespObj = $lloydsCardsWebServObj->Confirm3DResponseIframe($ipg_transaction_id, $this->buildAddress('shipping'), $this->buildAddress());

            self::$transactionObj->lloyds_response_transaction_ref = $returnRespObj->getIpgTransactionId();
            self::$transactionObj->lloyds_response_transaction_message = $returnRespObj->ErrorMessage;
            self::$transactionObj->lloyds_response_transaction_status = $returnRespObj->getTransactionResult() . '-' . $returnRespObj->getApprovalCode();
            self::$transactionObj->lloyds_response_payer_id = '';
            self::$transactionObj->lloyds_pay_response = "failed";

            if (is_object($returnRespObj)) {
                if ($returnRespObj->getTransactionResult() === 'APPROVED' && isset($returnRespObj->getSecure3DResponse()->ResponseCode3dSecure)) {
                    self::$transactionObj->lloyds3d_secure = true;
                    if (in_array($returnRespObj->getSecure3DResponse()->ResponseCode3dSecure, [1, 2, 4])) {
                        //success
                        $returnObj = $lloydsCardsWebServObj->ConfirmAuthenticationTransaction($ipg_transaction_id, $this->buildAddress(), $this->buildAddress('shipping'));
                        self::$transactionObj->lloyds_response_transaction_ref = $returnObj->getIpgTransactionId();
                        self::$transactionObj->lloyds_response_transaction_message = $returnObj->ErrorMessage;
                        self::$transactionObj->lloyds_response_transaction_status = $returnRespObj->getTransactionResult() . '-' . $returnRespObj->getApprovalCode();
                        if (is_object($returnObj) && $returnObj->getTransactionResult() === 'APPROVED' && str_starts_with($returnObj->getApprovalCode(), 'Y:')) {
                            self::$transactionObj->lloyds_pay_response = "success";
                            self::$transactionObj->lloyds_response_payer_id = $returnObj->getOrderId();
                        }
                    }
                    $paymentSessionObj->setData(self::$transactionObj);
                } else if (isset($returnRespObj->getSecure3DResponse()->Secure3DVerificationResponse)) {
                    $verifRedirectResp = $returnRespObj->getSecure3DResponse()->Secure3DVerificationResponse->VerificationRedirectResponse;

                    self::$transactionObj->lloyds3d_c_req = $verifRedirectResp->CReq ?? '';
                    self::$transactionObj->lloyds3d_pa_req = $verifRedirectResp->PaReq ?? '';
                    self::$transactionObj->lloyds3d_md = $verifRedirectResp->MD ?? '';
                    self::$transactionObj->lloyds3d_termurl = $verifRedirectResp->TermUrl;
                    self::$transactionObj->lloyds3d_redirect_url = $verifRedirectResp->AcsURL ?? '';
                    self::$transactionObj->lloyds3d_secure = true;
                    self::$transactionObj->lloyds3d_gone_to_gateway = true;
                    $paymentSessionObj->setData(self::$transactionObj);

                    if (self::$transactionObj->lloyds3d_redirect_url) {
                        $lloydsCardsWebServObj->process3DResponseRedirect(
                            self::$transactionObj->lloyds3d_redirect_url,
                            $verifRedirectResp->TermUrl,
                            self::$transactionObj->lloyds3d_c_req,
                            self::$transactionObj->lloyds3d_pa_req,
                            self::$transactionObj->lloyds3d_md,
                        );
                    }
                }
            }

            $new_data = [
                'remote_message' => $paymentService->getPaymentByReference(self::$transactionObj->payment_reference)->remote_message . ' ' . self::$transactionObj->lloyds_response_transaction_message,
                'remote_status_or_code' => self::$transactionObj->lloyds_response_transaction_status,
                'remote_payer_id' => self::$transactionObj->lloyds_response_payer_id,
                'status' => self::$transactionObj->lloyds_pay_response === 'success' ? "SUCCESSFUL" : "FAILED"
            ];

            if (self::$transactionObj->lloyds_pay_response === 'success') {
                self::$transactionObj->transaction_reference = self::$transactionObj->lloyds_response_transaction_ref; //success processing
                $new_data['remote_reference'] = self::$transactionObj->lloyds_response_transaction_ref;
            }

            $paymentService->update($new_data, $paymentService->getPaymentByReference(self::$transactionObj->payment_reference)->id);
            return wp_redirect(site_url(self::URL_BASE . self::URL_PROCESS));
        }
    }
}
