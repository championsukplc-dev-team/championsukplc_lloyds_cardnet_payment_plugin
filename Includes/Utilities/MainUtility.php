<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ChukplcCardnet\Includes\Utilities;

class MainUtility
{
    protected $em;
    protected $sm;

    //variable to hold session data for payment
    protected $session_data;

    private static $instance = null;

    /**
     * getInstance function
     *
     * @return self::$instance
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * __construct function
     * calls Renews session
     */
    public function __construct()
    {
        $this->renewSession();
    }

    /**
     * renewSession function
     * Checks for a session and renews it if possible
     * @return void
     */
    public function renewSession()
    {
        $_SESSION['championukplc_general'] = $this->sessionInitializer('championukplc_general' ?? null);

        //return the reference to the payment session data.
        $this->session_data = &$_SESSION['championukplc_general'];
    }

    public function sessionInitializer($session)
    {
        if (!isset($_SESSION[$session])) {
            return new \stdClass();
        }

        return $_SESSION[$session];
    }

    /**
     * clearSessionData function
     * Alias for clearSession.
     * @param string $sess - Session name
     * @return void
     */
    public function clearSessionData($sess = 'championukplc_general')
    {
        $this->clearSession($sess);
    }


    /**
     * getToken function
     * get token from the system
     * @param string $arg
     * @return string
     */
    public static function getToken($arg)
    {
        return md5(date("Y-m-d H:i:s") . $arg . date("Y-m-d H:i:s"));
    }

    /**
     * deleteFile function
     * delete the file on the file repository
     * @param string $filename
     * @return void
     */
    public static function deletefile($filename)
    {
        if (file_exists($filename)) {
            return unlink($filename);
        }

        return false;
    }

    /**
     * generateFilename function
     * generate file name
     * @param [type] $filename
     * @param [type] $ext
     * @return void
     */
    public static function generateFilename($filename, $ext = null)
    {
        if ($ext != null) {
            return substr(md5($filename), 0, 15) . $ext;
        }
        //get the extention
        $temp = explode('.', $filename);
        //generate the encryted file of name
        return substr(MainUtility::getToken($filename), 0, 15) . '.' . $temp[sizeof($temp) - 1];
    }

    /**
     * uploadfile function
     * upload a file to server
     * @param [type] $target_path
     * @param [type] $file
     * @param [type] $allowedExtensions
     * @param [type] $newname
     * @param boolean $overwrite
     * @param integer $max_file_size
     * @return void
     */
    public static function uploadfile($target_path, $file, $allowedExtensions = null, $newname = null, $overwrite = false, $max_file_size = 3000000)
    {
        if ($allowedExtensions == null || empty($allowedExtensions)) {
            $allowedExtensions = ["rar", "zip", "txt", "docx", "doc", "pdf", "png", "bmp", "jpg", "jpeg", "gif"];
        }

        if (trim(MainUtility::sanitize($_FILES[$file]['name'])) == '') {
            return null;
        }

        if (!file_exists($target_path)) {
            mkdir($target_path, 0755, true);
        }

        if ($newname == null) {
            $newname = MainUtility::sanitize($_FILES[$file]['name']);
        }

        $tname = explode('.', MainUtility::sanitize($_FILES[$file]['name']));

        $ext = (strtolower($tname[sizeof($tname) - 1]));

        if ($_FILES[$file]['size'] <= $max_file_size && !in_array($ext, $allowedExtensions) || $_FILES[$file]['size'] > $max_file_size) {
            MainUtility::deletefile($_FILES[$file]['tmp_name']); //delete the file
            return null;
        }

        $newname = ($overwrite == false) ? MainUtility::generateFilename($newname, "." . $ext) : $newname . "." . $ext;

        try {
            if (move_uploaded_file($_FILES[$file]['tmp_name'], $target_path . $newname)) {
                MainUtility::deletefile($_FILES[$file]['tmp_name']); //delete the file
                return $newname; //$newfilename;
            }

            MainUtility::deletefile($_FILES[$file]['tmp_name']); //delete the file
            return null;
        } catch (\Exception$ex) {
            MainUtility::deletefile($_FILES[$file]['tmp_name']); //delete the file
            echo esc_html($ex);
            return null;
        }
    }

    /**
     * sanitize function
     *
     * @param [type] $var
     * @return void
     */
    public static function sanitize($var)
    {
        try {
            $newvar = strip_tags(substr($var, 0)); //remove tags
            if ($newvar == '' && !is_numeric($var)) {
                return '';
            } else if ($newvar == '' && $var == 0) {
                return 0;
            } else if ($newvar == '') {
                return null;
            }
        } catch (\Exception$ex) {
            return null;
        }
    }


    /**
     * getUserIp function
     * Modified from wordpress/wp-admin/includes/class-wp-community-events::get_unsafe_client_ip
     * Added filter_var
     *
     * @return string|boolean
     */
    public static function getUserIp()
    {
        // In order of preference, with the best ones for this purpose first.
        $address_headers = [
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR',
        ];

        foreach ($address_headers as $header) {
            if (array_key_exists($header, $_SERVER)) {
                $address_chain = explode(',', $_SERVER[$header]);
                return filter_var(trim($address_chain[0]), FILTER_VALIDATE_IP);
            }
        }

        return false;
    }

    /**
     * httpPost function
     * Creates a curl request using WP HTTP_API.
     * @param string $url
     * @param array $params - should include a 'body' value.
     * @return void
     */
    public static function httpPost($url, array $params = [], $credentials = [])
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $params['body']);
        curl_setopt($handle, CURLOPT_USERPWD, $credentials['login'] . ":" . $credentials['password']);
        curl_setopt($handle, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
        curl_setopt($handle, CURLOPT_SSLCERT, $credentials['local_cert']);
        curl_setopt($handle, CURLOPT_SSLCERTPASSWD, $credentials['passphrase']);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "User-Agent: " . $_SERVER["HTTP_USER_AGENT"],
            "Content-length: " . strlen($params['body'] ?? '')
        ]);

        $response = [
            'response' => curl_exec($handle),
            'headers' => [
                'status' => curl_getinfo($handle, CURLINFO_HTTP_CODE)
            ]
        ];

        return $response;
    }


    /**
     * httpPost function
     * Creates a curl request using WP HTTP_API.
     * @param string $url
     * @param array $params - should include a 'body' value.
     * @return void
     */
    public static function httpGet($url, array $params = [])
    {
        $args = [
            'timeout' => '5',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'cookies' => [],
        ];

        $response = wp_remote_get($url, array_merge($args, $params));

        return $response;
    }


    /**
     * getIsoCurrencyCodeFromCurrencyCode function
     *
     * @param [type] $currencyCode
     * @return void
     */
    public static function getIsoCurrencyCodeFromCurrencyCode($currencyCode)
    {
        $isoCurrencyCodeList = [
            "AED" => 784,
            "AFN" => 971,
            "ALL" => 8,
            "AMD" => 51,
            "ANG" => 532,
            "AOA" => 973,
            "ARS" => 32,
            "AUD" => 36,
            "AWG" => 533,
            "AZN" => 944,
            "BAM" => 977,
            "BBD" => 52,
            "BDT" => 50,
            "BGN" => 975,
            "BHD" => 48,
            "BIF" => 108,
            "BMD" => 60,
            "BND" => 96,
            "BOB" => 68,
            "BOV" => 984,
            "BRL" => 986,
            "BSD" => 44,
            "BTN" => 64,
            "BWP" => 72,
            "BYR" => 974,
            "BZD" => 84,
            "CAD" => 124,
            "CDF" => 976,
            "CHE" => 947,
            "CHF" => 756,
            "CHW" => 948,
            "CLF" => 990,
            "CLP" => 152,
            "CNY" => 156,
            "COP" => 170,
            "COU" => 970,
            "CRC" => 188,
            "CUC" => 931,
            "CUP" => 192,
            "CVE" => 132,
            "CZK" => 203,
            "DJF" => 262,
            "DKK" => 208,
            "DOP" => 214,
            "DZD" => 12,
            "EGP" => 818,
            "ERN" => 232,
            "ETB" => 230,
            "EUR" => 978,
            "FJD" => 242,
            "FKP" => 238,
            "GBP" => 826,
            "GEL" => 981,
            "GHS" => 936,
            "GIP" => 292,
            "GMD" => 270,
            "GNF" => 324,
            "GTQ" => 320,
            "GYD" => 328,
            "HKD" => 344,
            "HNL" => 340,
            "HRK" => 191,
            "HTG" => 332,
            "HUF" => 348,
            "IDR" => 360,
            "ILS" => 376,
            "INR" => 356,
            "IQD" => 368,
            "IRR" => 364,
            "ISK" => 352,
            "JMD" => 388,
            "JOD" => 400,
            "JPY" => 392,
            "KES" => 404,
            "KGS" => 417,
            "KHR" => 116,
            "KMF" => 174,
            "KPW" => 408,
            "KRW" => 410,
            "KWD" => 414,
            "KYD" => 136,
            "KZT" => 398,
            "LAK" => 418,
            "LBP" => 422,
            "LKR" => 144,
            "LRD" => 430,
            "LSL" => 426,
            "LTL" => 440,
            "LVL" => 428,
            "LYD" => 434,
            "MAD" => 504,
            "MDL" => 498,
            "MGA" => 969,
            "MKD" => 807,
            "MMK" => 104,
            "MNT" => 496,
            "MOP" => 446,
            "MRO" => 478,
            "MUR" => 480,
            "MVR" => 462,
            "MWK" => 454,
            "MXN" => 484,
            "MXV" => 979,
            "MYR" => 458,
            "MZN" => 943,
            "NAD" => 516,
            "NGN" => 566,
            "NIO" => 558,
            "NOK" => 578,
            "NPR" => 524,
            "NZD" => 554,
            "OMR" => 512,
            "PAB" => 590,
            "PEN" => 604,
            "PGK" => 598,
            "PHP" => 608,
            "PKR" => 586,
            "PLN" => 985,
            "PYG" => 600,
            "QAR" => 634,
            "RON" => 946,
            "RSD" => 941,
            "RUB" => 643,
            "RWF" => 646,
            "SAR" => 682,
            "SBD" => 90,
            "SCR" => 690,
            "SDG" => 938,
            "SEK" => 752,
            "SGD" => 702,
            "SHP" => 654,
            "SLL" => 694,
            "SOS" => 706,
            "SRD" => 968,
            "SSP" => 728,
            "STD" => 678,
            "SYP" => 760,
            "SZL" => 748,
            "THB" => 764,
            "TJS" => 972,
            "TMT" => 934,
            "TND" => 788,
            "TOP" => 776,
            "TRY" => 949,
            "TTD" => 780,
            "TWD" => 901,
            "TZS" => 834,
            "UAH" => 980,
            "UGX" => 800,
            "USD" => 840,
            "USN" => 997,
            "USS" => 998,
            "UYI" => 940,
            "UYU" => 858,
            "UZS" => 860,
            "VEF" => 937,
            "VND" => 704,
            "VUV" => 548,
            "WST" => 882,
            "XAF" => 950,
            "XCD" => 951,
            "XOF" => 952,
            "XPF" => 953,
            "YER" => 886,
            "ZAR" => 710,
            "ZMW" => 967,
        ];

        return isset($isoCurrencyCodeList[trim(strtoupper($currencyCode))]) ? $isoCurrencyCodeList[trim(strtoupper($currencyCode))] : 0;
    }

    /**
     * clearSession function
     *
     * @param [type] $session_name
     * @return void
     */
    public static function clearSession($session_name)
    {
        unset($_SESSION[$session_name]);
    }

    /**
     * addLog function
     *
     * @param [type] $message
     * @param boolean $array
     * @return void
     */
    public static function addLog($message, $array = false)
    {
        $uploads = wp_upload_dir(null, false);
        $logs_dir = $uploads['basedir'] . '/wc-logs';

        if (!is_dir($logs_dir)) {
            mkdir($logs_dir, 0755, true);
        }

        $file = fopen($logs_dir . '/' . 'lloydscardnet.log', 'a');

        fwrite($file, PHP_EOL . date("c") . PHP_EOL);
        if ($array === true) {
            fwrite($file, print_r($message, true));
        } else {
            fwrite($file, $message);
        }
        fclose($file);
    }
}
