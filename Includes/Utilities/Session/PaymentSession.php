<?php

//define the payment session class
namespace ChukplcCardnet\Includes\Utilities\Session;

use ChukplcCardnet\Includes\Utilities\PaymentUtility;

class PaymentSession extends PaymentUtility
{
    private static $instance = null;

    //get the singleton instance for the class
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //create a new constructor
    public function __construct()
    {
        parent::__construct();
    }

    //return all session data
    public function getData()
    {
        $this->renewSession();
        return $this->session_data;
    }

    //return all session data
    public function setData($paymentData)
    {
        return $this->session_data = $paymentData;
    }
}
