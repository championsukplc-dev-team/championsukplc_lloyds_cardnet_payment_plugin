<?php

//define the payment session class
namespace ChukplcCardnet\Includes\Utilities\Session;

use ChukplcCardnet\Includes\Utilities\MainUtility;

class GeneralSession extends MainUtility
{
    private static $instance = null;

    //get the singleton instance for the class
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //create a new constructor
    public function __construct()
    {
        parent::__construct();
    }

    //return all session data
    public function getData()
    {
        return $this->session_data;
    }

    //return all session data
    public function setData($sessionData)
    {
        return $this->session_data = $sessionData;
    }

    //return all session data
    public function setNotification($notification)
    {
        if (!isset($this->session_data->notification)) {
            $this->session_data->notification = '';
        }

        return $this->session_data->notification .= $notification . '<br/>';
    }

    //return all session data
    public function getNotification()
    {
        $notification = null;
        if (isset($this->session_data->notification)) {
            $notification = $this->session_data->notification;
            unset($this->session_data->notification);
        }
        return $notification;
    }
}
