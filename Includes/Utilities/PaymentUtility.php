<?php

namespace ChukplcCardnet\Includes\Utilities;

use ChukplcCardnet\Includes\Utilities\MainUtility;

class PaymentUtility extends MainUtility
{
    private static $instance = null;

    //variable to hold session data for payment
    protected $session_data;

    //get the singleton instance for the class
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //create a new constructor
    public function __construct()
    {
        parent::__construct();
        $this->renewSession();
    }

    public function renewSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        //perform the test to initialize the session object
        $_SESSION['championukplc_payment'] = $this->sessionInitializer('championukplc_payment' ?? null);

        //return the reference to the payment session data.
        $this->session_data = &$_SESSION['championukplc_payment'];
    }

    public function clearSessionData($sess = 'championukplc_payment')
    {
        parent::clearSession($sess);
    }
}
