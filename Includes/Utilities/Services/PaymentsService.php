<?php

namespace ChukplcCardnet\Includes\Utilities\Services;

use ChukplcCardnet\Includes\Utilities\Services\MainService;

class PaymentsService extends MainService
{
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        parent::__construct();
        parent::setEntityClass('championsukplc_cardnet_payments');
    }

    public function isActivePaymentMethod($id)
    {
        return $this->getActivePaymentMethod($id);
    }

    public function getActivePaymentMethod($id)
    {
        $obj = new \stdClass();
        $obj->id = $id;
        $obj->method_name = 'Lloyds Cardnet';

        return $obj;
    }

    public function getPaymentMethod($id)
    {
        $obj = new \stdClass();
        $obj->id = $id;
        $obj->method_name = 'Lloyds Cardnet';

        return $obj;
    }

    public function getPaymentByReference($payment_reference)
    {
        $option = [['field' => 'payment_reference', 'value' => $payment_reference, 'operator' => '=']];
        $objContent = $this->getContentsUsingCustomFieldsConditions($option);

        if ($objContent) {
            return $objContent[0];
        }

        return null;
    }

    public function createPayment($transactionObj, $ip, $userObj, $notes = null)
    {
        $data = array(
            'ip_address' => $ip,
            'service_id' => $transactionObj->service_id,
            'service_type' => $transactionObj->service_type,
            'status' => "NEW",
            'amount' => $transactionObj->service_amount,
            'payment_method' => $transactionObj->service_payment_method->id,
            'remote_message' => $notes,
            'currency' => $transactionObj->service_currency,
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => $transactionObj->service_account->id,
            'modified_by' => $transactionObj->service_account->id,
        );

        $id = $this->insert($data);

        $paymentReference = "CHM-PR-" . $userObj->id . str_pad($id, 4, '0', STR_PAD_LEFT);

        $data = array(
            'payment_reference' => $paymentReference,
        );

        $this->update($data, $id);

        return $paymentReference;
    }
}
