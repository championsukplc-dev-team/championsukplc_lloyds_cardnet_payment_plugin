<?php

namespace ChukplcCardnet\Includes\Utilities\Services;

class MainService
{
    protected $entityClass;
    private static $instance = null;
    protected $conn;

    //get the singleton instance for the class
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //create a new constructor
    public function __construct()
    {
        $PDO_DSN = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET;
        $dbh = new \PDO($PDO_DSN, DB_USER, DB_PASSWORD, null);
        $this->conn = $dbh;
    }

    //set the entity class for the table
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
    }

    //get the entity class for the table
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    public function performSelect($entity, array $columns = array('*'), array $conditions = null, $ordering = array('fields' => array('id'), 'direction' => 'DESC'))
    {
        if ($conditions == null) {
            $conditions_str = '1';
        } else {
            $conditions_str = ' 1 ';
            foreach ($conditions as $condition) {
                $conditions_str .= ' AND ' . $condition;
            }
        }

        $columns_str = implode(',', $columns);

        $ordering_str = '';
        if ($ordering != null) {
            $ordering_str = ' order by ';
            foreach ($ordering['fields'] as $field) {
                $ordering_str .= $field . ',';
            }

            $ordering_str .= ',';
            $ordering_str = str_replace(',,', '', $ordering_str);

            if (isset($ordering['direction'])) {
                $ordering_str .= ' ' . $ordering['direction'];
            }
        }

        $stmt = $this->conn->prepare('SELECT ' . $columns_str . ' FROM ' . $entity . ' WHERE ' . $conditions_str . ' ' . $ordering_str);
        $stmt->execute();
        return $stmt;
    }

    //get only the first content from the database with id
    public function getContent($id)
    {
        $result = $this->performSelect($this->entityClass, array('*'), array("id='" . $id . "'"));
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (is_array($content) && !empty($content)) {
            return $content[0];
        }

        return null;
    }

    //get only the first active content from the database with id
    public function getActiveContent($id)
    {
        $result = $this->performSelect($this->entityClass, array('*'), array("id='" . $id . "'", "status='Active'"));
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (is_array($content) && !empty($content)) {
            return $content[0];
        }

        return null;
    }

    //get only the active contents from the table
    public function getActiveContents()
    {
        $result = $this->performSelect($this->entityClass, array('*'), array("status='Active'"));
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content;
    }

    //get only the active contents from the table
    public function getFirstActiveContent()
    {
        $result = $this->performSelect($this->entityClass, array('*'), array("status='Active'"));
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content[0];
    }

    //get only the active contents from the table
    public function getFirstContent()
    {
        $result = $this->performSelect($this->entityClass);
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content[0];
    }

    //get only the custom fields with values from the table
    public function getContentsUsingCustomFieldsConditions($arrayOptions, $orderField = null, $direction = 'DESC')
    {
        $condition_str = '';

        foreach ($arrayOptions as $option) {
            $condition_str .= ' ' . $option['field'] . ' ' . $option['operator'] . " '" . $option['value'] . "' AND";
        }

        $condition_str .= '--';
        $condition_str = str_replace('AND--', '', $condition_str);

        $ordering = null;

        if ($orderField != null) {
            $ordering = array('fields' => array($orderField), 'direction' => $direction);
        }

        $result = $this->performSelect($this->entityClass, array('*'), array($condition_str), $ordering);
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content;
    }

    //get only the custom fields with values from the table
    public function getContentUsingCustomFieldsConditions($arrayOptions)
    {
        $condition_str = '';

        foreach ($arrayOptions as $option) {
            $condition_str .= ' ' . $option['field'] . ' ' . $option['operator'] . " '" . $option['value'] . "' AND";
        }

        $condition_str .= '--';
        $condition_str = str_replace('AND--', '', $condition_str);

        $ordering = null;
        $result = $this->performSelect($this->entityClass, array('*'), array($condition_str), $ordering);
        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content[0];
    }

    //execute custom query from the table
    public function getContentsUsingCustomQuery($qry)
    {
        $query = $this->conn->query($qry);
        $result = $query->execute();
        $content = null;

        if ($result) {
            $content = $query->fetchAll(\PDO::FETCH_OBJ);
        }

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content;
    }

    //get all contents from the table
    public function getContents()
    {
        $result = $this->performSelect($this->entityClass);

        $content = $result->fetchAll(\PDO::FETCH_OBJ);

        if (!$content || !is_array($content) || empty($content)) {
            return null;
        }

        return $content;
    }


    /**
     * Save an entry in the database.
     *
     *
     * Exception handling is shown in this example. It could be simplified
     * without the try/catch blocks, but since an insert will throw an exception
     * and terminate your application if the exception is not handled, it is best
     * to employ try/catch.
     *
     * @param array $entry
     *   An array containing all the fields of the database record.
     *
     * @return int
     *   The number of updated rows.
     *
     * @throws \Exception
     *   When the database insert fails.
     *
     */
    public function insert(array $entry)
    {
        $return_value = null;
        try {
            $column_str = '';
            $values_str = '';

            foreach ($entry as $key => $item) {
                $column_str .= $key . ",";
                $values_str .= " '" . $item . "',";
            }

            $column_str .= ',';
            $column_str = str_replace(',,', '', $column_str);

            $values_str .= ',';
            $values_str = str_replace(',,', '', $values_str);

            $sql = "INSERT INTO " . $this->entityClass . " (" . $column_str . ") VALUES (" . $values_str . ")";

            // Prepare statement
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // return the UPDATED record
            $return_value = $this->conn->lastInsertId();
        } catch (\Exception $e) {
            throw $e;
        }
        return $return_value;
    }

    /**
     * Update an entry in the database.
     *
     * @param array $entry
     *   An array containing all the fields of the item to be updated.
     *
     * @return int
     *   The number of updated rows.
     *
     */
    public function update(array $entry, $id)
    {
        try {
            $column_str = '';

            foreach ($entry as $key => $item) {
                $column_str .= ' ' . $key . " = '" . $item . "',";
            }

            $column_str .= ',';
            $column_str = str_replace(',,', '', $column_str);

            $sql = "UPDATE " . $this->entityClass . " SET " . $column_str . " WHERE id='" . $id . "'";

            // Prepare statement
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // return the UPDATED record
            return $stmt->rowCount();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteContent($id)
    {
        $query = $this->conn->query("delete from " . $this->entityClass . " where id='" . $id . "'");
        $query->execute();
    }

    public function customQuery($qry)
    {
        return $this->conn->query($qry);
    }
}
