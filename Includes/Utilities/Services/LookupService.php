<?php

namespace ChukplcCardnet\Includes\Utilities\Services;

use ChukplcCardnet\Includes\Utilities\Services\MainService;

/**
 * LookupService class
 * @method mixed getContentsUsingCustomFieldsConditions()
 */
class LookupService extends MainService
{
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        parent::__construct();
        parent::setEntityClass('championsukplc_lookup');
    }

    public static function getActiveLookUpData($lookupType)
    {
        $option = array(
            array('field' => 'status', 'value' => 1, 'operator' => '='),
            array('field' => 'lookup_type', 'value' => $lookupType, 'operator' => '='),
            array('field' => 'lang', 'value' => 'EN', 'operator' => '='),
        );

        $obj = self::getInstance();

        $objContent = $obj->getContentsUsingCustomFieldsConditions($option);

        if ($objContent) {
            return $objContent;
        }

        return null;
    }

    public static function getSingleLookUpDataByValue($lookupType, $lookupValue)
    {
        $option = array(
            array('field' => 'status', 'value' => 1, 'operator' => '='),
            array('field' => 'lookup_type', 'value' => $lookupType, 'operator' => '='),
            array('field' => 'lookup_value', 'value' => $lookupValue, 'operator' => '='),
            array('field' => 'lang', 'value' => 'EN', 'operator' => '='),
        );

        $obj = self::getInstance();

        $objContent = $obj->getContentsUsingCustomFieldsConditions($option);

        if ($objContent) {
            return $objContent[0];
        }

        return null;
    }

    public static function getSingleLookUpDataByName($lookupType, $lookupName)
    {
        $option = array(
            array('field' => 'status', 'value' => 1, 'operator' => '='),
            array('field' => 'lookup_type', 'value' => $lookupType, 'operator' => '='),
            array('field' => 'lookup_name', 'value' => $lookupName, 'operator' => '='),
            array('field' => 'lang', 'value' => 'EN', 'operator' => '='),
        );

        $obj = self::getInstance();

        $objContent = $obj->getContentsUsingCustomFieldsConditions($option);

        if ($objContent) {
            return $objContent[0];
        }

        return null;
    }

    public static function getSingleLookUpDataByAttribute1($lookupType, $attribute1)
    {
        $option = array(
            array('field' => 'status', 'value' => 1, 'operator' => '='),
            array('field' => 'lookup_type', 'value' => $lookupType, 'operator' => '='),
            array('field' => 'attribute1', 'value' => $attribute1, 'operator' => '='),
            array('field' => 'lang', 'value' => 'EN', 'operator' => '='),
        );

        $obj = self::getInstance();

        $objContent = $obj->getContentsUsingCustomFieldsConditions($option);

        if ($objContent) {
            return $objContent[0];
        }

        return null;
    }
}
