<?php

namespace ChukplcCardnet\Includes\Utilities;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

class EmailUtility extends MainUtility
{
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        parent::__construct();
    }

    public static function sendMail($subject, $messageToSend, $toEmail = [], $attachments = [])
    {
        if ($toEmail == null || empty($toEmail) || $messageToSend == null) {
            return false;
        }

        try {
            $receiving_emails = array();

            foreach ($toEmail as $to) {
                $receiving_emails[] = (is_array($to)) ? $to[1] . '<' . $to[0] . '>' : $to;
            }

            return wp_mail($receiving_emails, $subject, $messageToSend, $attachments);
        } catch (\Exception $ex) {
            return false;
        }
    }
}
