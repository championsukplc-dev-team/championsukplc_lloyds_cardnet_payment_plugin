<?php

/**
 * daudu adetokunbo
 */

namespace ChukplcCardnet\Includes\Utilities;

use ChukplcCardnet\Includes\Utilities\MainUtility;

class LloydsCardnetRedirectUtility
{
    const ENVIRONMENT_LIVE = 'live';
    const ENVIRONMENT_TEST = 'test';
    protected $_liveOrTest;
    protected $_sharedSecret;
    private $_liveUrl = "https://www.ipg-online.com/connect/gateway/processing";
    private $_testUrl = "https://test.ipg-online.com/connect/gateway/processing";
    protected $_timeZone;
    protected $_processingUrl;
    protected $_successUrl;
    protected $_failureUrl;

    protected $_billingSurname;
    protected $_billingFirstnames;
    protected $_billingAddress1;
    protected $_billingAddress2;
    protected $_billingCity;
    protected $_billingPostCode;
    protected $_billingCountry;
    protected $_billingState;
    protected $_billingPhone;

    protected $_deliverySurname;
    protected $_deliveryFirstnames;
    protected $_deliveryAddress1;
    protected $_deliveryAddress2;
    protected $_deliveryCity;
    protected $_deliveryPostCode;
    protected $_deliveryCountry;
    protected $_deliveryState;

    protected $_companyName;
    protected $_customerEmail;
    protected $_checkoutOption;
    protected $_orderId;

    protected $_transactionNotificationUrl;

    public function __construct($liveOrTest)
    {
        $this->_liveOrTest = $liveOrTest;
        $this->_processingUrl = ($this->_liveOrTest == self::ENVIRONMENT_LIVE) ? $this->_liveUrl : $this->_testUrl;

        $this->_timeZone = date_default_timezone_get(); //$timeZone='Europe/London'

        return $this;
    }

    public function setSharedSecret($sharedSecret)
    {
        $this->_sharedSecret = $sharedSecret;
        return $this;
    }

    public function setBillingSurname($billingSurname)
    {
        $this->_billingSurname = $billingSurname;
        return $this;
    }

    public function setBillingFirstnames($billingFirstnames)
    {
        $this->_billingFirstnames = $billingFirstnames;
        return $this;
    }

    public function setBillingAddress1($billingAddress1)
    {
        $this->_billingAddress1 = $billingAddress1;
        return $this;
    }

    public function setBillingAddress2($billingAddress2)
    {
        $this->_billingAddress2 = $billingAddress2;
        return $this;
    }

    public function setBillingCity($billingCity)
    {
        $this->_billingCity = $billingCity;
        return $this;
    }

    public function setBillingPostCode($billingPostCode)
    {
        $this->_billingPostCode = $billingPostCode;
        return $this;
    }

    public function setBillingCountry($billingCountry)
    {
        $this->_billingCountry = $billingCountry;
        return $this;
    }

    public function setBillingState($billingState)
    {
        $this->_billingState = $billingState;
        return $this;
    }

    public function setBillingPhone($billingPhone)
    {
        $this->_billingPhone = $billingPhone;
        return $this;
    }

    public function setDeliverySurname($deliverySurname)
    {
        $this->_deliverySurname = $deliverySurname;
        return $this;
    }

    public function setDeliveryFirstnames($deliveryFirstnames)
    {
        $this->_deliveryFirstnames = $deliveryFirstnames;
        return $this;
    }

    public function setDeliveryAddress1($deliveryAddress1)
    {
        $this->_deliveryAddress1 = $deliveryAddress1;
        return $this;
    }

    public function setDeliveryAddress2($deliveryAddress2)
    {
        $this->_deliveryAddress2 = $deliveryAddress2;
        return $this;
    }

    public function setDeliveryCity($deliveryCity)
    {
        $this->_deliveryCity = $deliveryCity;
        return $this;
    }

    public function setDeliveryPostCode($deliveryPostCode)
    {
        $this->_deliveryPostCode = $deliveryPostCode;
        return $this;
    }

    public function setDeliveryCountry($deliveryCountry)
    {
        $this->_deliveryCountry = $deliveryCountry;
        return $this;
    }

    public function setDeliveryState($deliveryState)
    {
        $this->_deliveryState = $deliveryState;
        return $this;
    }

    public function setCompanyName($companyName)
    {
        $this->_companyName = $companyName;
        return $this;
    }

    public function setCustomerEmail($customerEmail)
    {
        $this->_customerEmail = $customerEmail;
        return $this;
    }

    public function setSuccessUrl($successUrl)
    {
        $this->_successUrl = $successUrl;
        return $this;
    }

    public function setFailureUrl($failureUrl)
    {
        $this->_failureUrl = $failureUrl;
        return $this;
    }

    public function setOrderId($orderId)
    {
        $this->_orderId = $orderId;
        return $this;
    }

    /**
     * @param $successUrl
     * @return $this
     */
    public function setTransactionNotificationUrl($transactionNotificationUrl)
    {
        $this->_transactionNotificationUrl = $transactionNotificationUrl;
        return $this;
    }

    public function createHash($storeName, $transactionTime, $chargeTotal, $currency)
    {
        $stringToHash = $storeName . $transactionTime . $chargeTotal . $currency . $this->_sharedSecret;

        return hash('sha256', bin2hex($stringToHash));
    }

    public function verifyResponse($response_hash, $transactionTime, $approvalCode, $chargeTotal, $currency, $storeName)
    {
        $stringToHash = $this->_sharedSecret . $approvalCode . $chargeTotal . $currency . $transactionTime . $storeName;

        if (hash('sha256', bin2hex($stringToHash)) === $response_hash) {
            return true;
        }
        return false;
    }

    /**
     * @param $notification_hash
     * @param $transactionTime
     * @param $approvalCode
     * @param $chargeTotal
     * @param $currency
     * @param $storeName
     * @return bool
     */
    public function verifyResponseNotification($notification_hash, $transactionTime, $approvalCode, $chargeTotal, $currency, $storeName)
    {
        $notificationStringToHash = $chargeTotal . $this->_sharedSecret . $currency . $transactionTime . $storeName . $approvalCode;

        if (hash('sha256', bin2hex($notificationStringToHash)) === $notification_hash) {
            return true;
        }
        return false;
    }

    /**
     * @param $checkoutOption
     * @return $this
     */
    public function setCheckoutOption($checkoutOption)
    {
        $this->_checkoutOption = $checkoutOption;
        return $this;
    }

    public function processRedirectPaymentForm($storeName, $transactionTime, $chargeTotal, $currency, $paymentReference)
    {
        $hashValue = $this->createHash($storeName, $transactionTime, $chargeTotal, $currency);
        $billName = $this->_billingFirstnames . ' ' . $this->_billingSurname;
        $shipName = !empty($this->_deliveryFirstnames) ? $this->_deliveryFirstnames . ' ' . $this->_deliverySurname : '';

        $timezone = $this->_timeZone; // ($this->_timeZone == 'UTC') ? 'Europe/London' : $this->_timeZone;
        $selectedPayment = '';
        if ($this->_checkoutOption === 'classic') {
            $selectedPayment = 'M';
        }

        $formToSend = "
        <form id='lloydscardsnetredirect-form' name='lloydscardsnetredirect-form' action='{$this->_processingUrl}' method='post'>
            <input type='hidden' name='txntype' value='sale'>
            <input type='hidden' name='checkoutoption' value='{$this->_checkoutOption}'/>
            <input type='hidden' name='timezone' value='{$timezone}'/>
            <input type='hidden' name='txndatetime' value='{$transactionTime}'/>
            <input type='hidden' name='hash_algorithm' value='SHA256'/>
            <input type='hidden' name='hash' value='{$hashValue}'/>
            <input type='hidden' name='storename' value='{$storeName}'/>
            <input type='hidden' name='mode' value='payonly'/>
            <input type='hidden' name='bcompany' value='{$this->_companyName}'>
            <input type='hidden' name='bname' value='{$billName}'/>
            <input type='hidden' name='baddr1' value='{$this->_billingAddress1}'/>
            <input type='hidden' name='baddr2' value='{$this->_billingAddress2}'/>
            <input type='hidden' name='bcity' value='{$this->_billingCity}'/>
            <input type='hidden' name='bstate' value='{$this->_billingState}'/>
            <input type='hidden' name='bcountry' value='{$this->_billingCountry}'/>
            <input type='hidden' name='bzip' value='{$this->_billingPostCode}'/>
            <input type='hidden' name='phone' value='{$this->_billingPhone}'/>
            <input type='hidden' name='email' value='{$this->_customerEmail}'/>
            <input type='hidden' name='sname' value='{$shipName}'/>
            <input type='hidden' name='saddr1' value='{$this->_deliveryAddress1}'/>
            <input type='hidden' name='saddr2' value='{$this->_deliveryAddress2}'/>
            <input type='hidden' name='scity' value='{$this->_deliveryCity}'/>
            <input type='hidden' name='sstate' value='{$this->_deliveryState}'/>
            <input type='hidden' name='scountry' value='{$this->_deliveryCountry}'/>
            <input type='hidden' name='szip' value='{$this->_deliveryPostCode}'/>
            <input type='hidden' name='comments' value='Champions Plugin' />
            <input type='hidden' name='paymentMethod' value='{$selectedPayment}'/>
            <input type='hidden' name='transactionNotificationURL' value='{$this->_transactionNotificationUrl}' />
            <input type='hidden' name='chargetotal' value='{$chargeTotal}'/>
            <input type='hidden' name='currency' value='{$currency}'/>
            <input type='hidden' name='merchantTransactionId' value='{$paymentReference}'/>
            <input type='hidden' name='responseFailURL' value='{$this->_failureUrl}'/>
            <input type='hidden' name='responseSuccessURL' value='{$this->_successUrl}'/>
            <input type='hidden' name='authenticateTransaction' value='true'/>
        </form><script type='text/javascript'>

        document.forms['lloydscardsnetredirect-form'].submit();
             </script>";

        MainUtility::addLog('Form To Redirect Start' . PHP_EOL);
        MainUtility::addLog($formToSend . PHP_EOL, true);
        MainUtility::addLog('Form To Redirect End' . PHP_EOL);
        session_regenerate_id(true);
        session_write_close();
        echo wp_kses($formToSend, [
            'form' => [
                'id' => [],
                'name' => [],
                'action' => [],
                'method' => []
            ],
            'input' => [
                'type' => [],
                'name' => [],
                'value' => []
            ],
            'script' => [
                'type' => []
            ]
        ]);
        exit;
    }
}
