<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ClearingElement.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ClearingElement 
{
	public $Brand;

	public $Value;

	public $Count;
    


    public function getBrand()
    {

        return $this->Brand;

    }


    public function setBrand($Brand)
    {

        $this->Brand = $Brand;

        return $this;

    }


    public function getValue()
    {

        return $this->Value;

    }


    public function setValue($Value)
    {

        $this->Value = $Value;

        return $this;

    }


    public function getCount()
    {

        return $this->Count;

    }


    public function setCount($Count)
    {

        $this->Count = $Count;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
