<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\MPCharge.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class MPCharge 
{
	public $MNSP;

	public $MSISDN;

	public $PaymentType;
    


    public function getMNSP()
    {

        return $this->MNSP;

    }


    public function setMNSP($MNSP)
    {

        $this->MNSP = $MNSP;

        return $this;

    }


    public function getMSISDN()
    {

        return $this->MSISDN;

    }


    public function setMSISDN($MSISDN)
    {

        $this->MSISDN = $MSISDN;

        return $this;

    }


    public function getPaymentType()
    {

        return $this->PaymentType;

    }


    public function setPaymentType($PaymentType)
    {

        $this->PaymentType = $PaymentType;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
