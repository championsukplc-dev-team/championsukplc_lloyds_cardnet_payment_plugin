<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\EMVCardPresentRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class EMVCardPresentRequest 
{
	public $CryptData;

	public $EMVRequestData;

	public $TLVData;
    


    public function getCryptData()
    {

        return $this->CryptData;

    }


    public function setCryptData($CryptData)
    {

        $this->CryptData = $CryptData;

        return $this;

    }


    public function getEMVRequestData()
    {

        return $this->EMVRequestData;

    }


    public function setEMVRequestData($EMVRequestData)
    {

        $this->EMVRequestData = $EMVRequestData;

        return $this;

    }


    public function getTLVData()
    {

        return $this->TLVData;

    }


    public function setTLVData($TLVData)
    {

        $this->TLVData = $TLVData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
