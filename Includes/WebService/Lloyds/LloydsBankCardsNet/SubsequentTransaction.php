<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\SubsequentTransaction.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class SubsequentTransaction 
{
	public $IpgTransactionId;

	public $ReferencedMerchantTransactionId;

	public $OrderId;

	public $TDate;

	public $MerchantTransactionId;

	public $Options;

	public $TransactionType;

	public $Payment;

	public $Basket;
    


    public function getIpgTransactionId()
    {

        return $this->IpgTransactionId;

    }


    public function setIpgTransactionId($IpgTransactionId)
    {

        $this->IpgTransactionId = $IpgTransactionId;

        return $this;

    }


    public function getReferencedMerchantTransactionId()
    {

        return $this->ReferencedMerchantTransactionId;

    }


    public function setReferencedMerchantTransactionId($ReferencedMerchantTransactionId)
    {

        $this->ReferencedMerchantTransactionId = $ReferencedMerchantTransactionId;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getTDate()
    {

        return $this->TDate;

    }


    public function setTDate($TDate)
    {

        $this->TDate = $TDate;

        return $this;

    }


    public function getMerchantTransactionId()
    {

        return $this->MerchantTransactionId;

    }


    public function setMerchantTransactionId($MerchantTransactionId)
    {

        $this->MerchantTransactionId = $MerchantTransactionId;

        return $this;

    }


    public function getOptions()
    {

        return $this->Options;

    }


    public function setOptions($Options)
    {

        $this->Options = $Options;

        return $this;

    }


    public function getTransactionType()
    {

        return $this->TransactionType;

    }


    public function setTransactionType($TransactionType)
    {

        $this->TransactionType = $TransactionType;

        return $this;

    }


    public function getPayment()
    {

        return $this->Payment;

    }


    public function setPayment($Payment)
    {

        $this->Payment = $Payment;

        return $this;

    }


    public function getBasket()
    {

        return $this->Basket;

    }


    public function setBasket($Basket)
    {

        $this->Basket = $Basket;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
