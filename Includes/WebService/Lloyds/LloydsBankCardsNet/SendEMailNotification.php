<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\SendEMailNotification.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class SendEMailNotification 
{
	public $StoreId;

	public $OrderId;

	public $TDate;

	public $Email;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getTDate()
    {

        return $this->TDate;

    }


    public function setTDate($TDate)
    {

        $this->TDate = $TDate;

        return $this;

    }


    public function getEmail()
    {

        return $this->Email;

    }


    public function setEmail($Email)
    {

        $this->Email = $Email;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
