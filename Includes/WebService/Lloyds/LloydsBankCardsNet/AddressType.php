<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\AddressType.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class AddressType 
{
	public $Address1;

	public $Address2;

	public $Zip;

	public $City;

	public $State;

	public $Country;
    


    public function getAddress1()
    {

        return $this->Address1;

    }


    public function setAddress1($Address1)
    {

        $this->Address1 = $Address1;

        return $this;

    }


    public function getAddress2()
    {

        return $this->Address2;

    }


    public function setAddress2($Address2)
    {

        $this->Address2 = $Address2;

        return $this;

    }


    public function getZip()
    {

        return $this->Zip;

    }


    public function setZip($Zip)
    {

        $this->Zip = $Zip;

        return $this;

    }


    public function getCity()
    {

        return $this->City;

    }


    public function setCity($City)
    {

        $this->City = $City;

        return $this;

    }


    public function getState()
    {

        return $this->State;

    }


    public function setState($State)
    {

        $this->State = $State;

        return $this;

    }


    public function getCountry()
    {

        return $this->Country;

    }


    public function setCountry($Country)
    {

        $this->Country = $Country;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
