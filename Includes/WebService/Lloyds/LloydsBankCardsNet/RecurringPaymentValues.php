<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\RecurringPaymentValues.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class RecurringPaymentValues 
{
	public $State;

	public $CreationDate;

	public $FailureCount;

	public $NextAttemptDate;

	public $RunCount;

	public $CreditCardData;

	public $DE_DirectDebitData;

	public $HostedDataID;

	public $HostedDataStoreID;

	public $SubTotal;

	public $ValueAddedTax;

	public $localTax;

	public $DeliveryAmount;

	public $CashbackAmount;

	public $ChargeTotal;

	public $Currency;

	public $TransactionOrigin;

	public $InvoiceNumber;

	public $PONumber;

	public $Comments;
    


    public function getState()
    {

        return $this->State;

    }


    public function setState($State)
    {

        $this->State = $State;

        return $this;

    }


    public function getCreationDate()
    {

        return $this->CreationDate;

    }


    public function setCreationDate($CreationDate)
    {

        $this->CreationDate = $CreationDate;

        return $this;

    }


    public function getFailureCount()
    {

        return $this->FailureCount;

    }


    public function setFailureCount($FailureCount)
    {

        $this->FailureCount = $FailureCount;

        return $this;

    }


    public function getNextAttemptDate()
    {

        return $this->NextAttemptDate;

    }


    public function setNextAttemptDate($NextAttemptDate)
    {

        $this->NextAttemptDate = $NextAttemptDate;

        return $this;

    }


    public function getRunCount()
    {

        return $this->RunCount;

    }


    public function setRunCount($RunCount)
    {

        $this->RunCount = $RunCount;

        return $this;

    }


    public function getCreditCardData()
    {

        return $this->CreditCardData;

    }


    public function setCreditCardData($CreditCardData)
    {

        $this->CreditCardData = $CreditCardData;

        return $this;

    }


    public function getDE_DirectDebitData()
    {

        return $this->DE_DirectDebitData;

    }


    public function setDE_DirectDebitData($DE_DirectDebitData)
    {

        $this->DE_DirectDebitData = $DE_DirectDebitData;

        return $this;

    }


    public function getHostedDataID()
    {

        return $this->HostedDataID;

    }


    public function setHostedDataID($HostedDataID)
    {

        $this->HostedDataID = $HostedDataID;

        return $this;

    }


    public function getHostedDataStoreID()
    {

        return $this->HostedDataStoreID;

    }


    public function setHostedDataStoreID($HostedDataStoreID)
    {

        $this->HostedDataStoreID = $HostedDataStoreID;

        return $this;

    }


    public function getSubTotal()
    {

        return $this->SubTotal;

    }


    public function setSubTotal($SubTotal)
    {

        $this->SubTotal = $SubTotal;

        return $this;

    }


    public function getValueAddedTax()
    {

        return $this->ValueAddedTax;

    }


    public function setValueAddedTax($ValueAddedTax)
    {

        $this->ValueAddedTax = $ValueAddedTax;

        return $this;

    }


    public function getLocalTax()
    {

        return $this->localTax;

    }


    public function setLocalTax($localTax)
    {

        $this->localTax = $localTax;

        return $this;

    }


    public function getDeliveryAmount()
    {

        return $this->DeliveryAmount;

    }


    public function setDeliveryAmount($DeliveryAmount)
    {

        $this->DeliveryAmount = $DeliveryAmount;

        return $this;

    }


    public function getCashbackAmount()
    {

        return $this->CashbackAmount;

    }


    public function setCashbackAmount($CashbackAmount)
    {

        $this->CashbackAmount = $CashbackAmount;

        return $this;

    }


    public function getChargeTotal()
    {

        return $this->ChargeTotal;

    }


    public function setChargeTotal($ChargeTotal)
    {

        $this->ChargeTotal = $ChargeTotal;

        return $this;

    }


    public function getCurrency()
    {

        return $this->Currency;

    }


    public function setCurrency($Currency)
    {

        $this->Currency = $Currency;

        return $this;

    }


    public function getTransactionOrigin()
    {

        return $this->TransactionOrigin;

    }


    public function setTransactionOrigin($TransactionOrigin)
    {

        $this->TransactionOrigin = $TransactionOrigin;

        return $this;

    }


    public function getInvoiceNumber()
    {

        return $this->InvoiceNumber;

    }


    public function setInvoiceNumber($InvoiceNumber)
    {

        $this->InvoiceNumber = $InvoiceNumber;

        return $this;

    }


    public function getPONumber()
    {

        return $this->PONumber;

    }


    public function setPONumber($PONumber)
    {

        $this->PONumber = $PONumber;

        return $this;

    }


    public function getComments()
    {

        return $this->Comments;

    }


    public function setComments($Comments)
    {

        $this->Comments = $Comments;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
