<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CreatePaymentURL.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CreatePaymentURL 
{
	public $Transaction;

	public $TransactionNotificationURL;

	public $AuthenticateTransaction;

	public $Expiration;

	public $MCC6012Details;
    


    public function getTransaction()
    {

        return $this->Transaction;

    }


    public function setTransaction($Transaction)
    {

        $this->Transaction = $Transaction;

        return $this;

    }


    public function getTransactionNotificationURL()
    {

        return $this->TransactionNotificationURL;

    }


    public function setTransactionNotificationURL($TransactionNotificationURL)
    {

        $this->TransactionNotificationURL = $TransactionNotificationURL;

        return $this;

    }


    public function getAuthenticateTransaction()
    {

        return $this->AuthenticateTransaction;

    }


    public function setAuthenticateTransaction($AuthenticateTransaction)
    {

        $this->AuthenticateTransaction = $AuthenticateTransaction;

        return $this;

    }


    public function getExpiration()
    {

        return $this->Expiration;

    }


    public function setExpiration($Expiration)
    {

        $this->Expiration = $Expiration;

        return $this;

    }


    public function getMCC6012Details()
    {

        return $this->MCC6012Details;

    }


    public function setMCC6012Details($MCC6012Details)
    {

        $this->MCC6012Details = $MCC6012Details;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
