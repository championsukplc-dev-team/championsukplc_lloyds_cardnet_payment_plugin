<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\MACType.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class MACType 
{
	public $Value;

	public $KSN;
    


    public function getValue()
    {

        return $this->Value;

    }


    public function setValue($Value)
    {

        $this->Value = $Value;

        return $this;

    }


    public function getKSN()
    {

        return $this->KSN;

    }


    public function setKSN($KSN)
    {

        $this->KSN = $KSN;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
