<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Secure3DVerificationResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Secure3DVerificationResponse 
{
	public $VerificationRedirectResponse;

	public $IVRVerificationResponse;
    


    public function getVerificationRedirectResponse()
    {

        return $this->VerificationRedirectResponse;

    }


    public function setVerificationRedirectResponse($VerificationRedirectResponse)
    {

        $this->VerificationRedirectResponse = $VerificationRedirectResponse;

        return $this;

    }


    public function getIVRVerificationResponse()
    {

        return $this->IVRVerificationResponse;

    }


    public function setIVRVerificationResponse($IVRVerificationResponse)
    {

        $this->IVRVerificationResponse = $IVRVerificationResponse;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
