<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Action.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class Action
{
    public $InitiateClearing;
    public $InquiryCardInformation;
    public $InquiryOrder;
    public $InquiryTransaction;
    public $StoreHostedData;
    public $RecurringPayment;
    public $Validate;
    public $GetExternalTransactionStatus;
    public $GetExternalConsumerInformation;
    public $SendEMailNotification;
    public $GetLastOrders;
    public $GetLastTransactions;
    public $ManageProducts;
    public $ManageProductStock;
    public $RequestCardRateForDCC;
    public $RequestMerchantRateForDynamicPricing;
    public $CreatePaymentURL;
    public $ClientLocale;

    public function getInitiateClearing()
    {
        return $this->InitiateClearing;
    }

    public function setInitiateClearing($InitiateClearing)
    {
        $this->InitiateClearing = $InitiateClearing;

        return $this;
    }

    public function getInquiryCardInformation()
    {
        return $this->InquiryCardInformation;
    }

    public function setInquiryCardInformation($InquiryCardInformation)
    {
        $this->InquiryCardInformation = $InquiryCardInformation;

        return $this;
    }

    public function getInquiryOrder()
    {
        return $this->InquiryOrder;
    }

    public function setInquiryOrder($InquiryOrder)
    {
        $this->InquiryOrder = $InquiryOrder;

        return $this;
    }

    public function getInquiryTransaction()
    {
        return $this->InquiryTransaction;
    }

    public function setInquiryTransaction($InquiryTransaction)
    {
        $this->InquiryTransaction = $InquiryTransaction;

        return $this;
    }

    public function getStoreHostedData()
    {
        return $this->StoreHostedData;
    }

    public function setStoreHostedData($StoreHostedData)
    {
        $this->StoreHostedData = $StoreHostedData;

        return $this;
    }

    public function getRecurringPayment()
    {
        return $this->RecurringPayment;
    }

    public function setRecurringPayment($RecurringPayment)
    {
        $this->RecurringPayment = $RecurringPayment;

        return $this;
    }

    public function getValidate()
    {
        return $this->Validate;
    }

    public function setValidate($Validate)
    {
        $this->Validate = $Validate;

        return $this;
    }

    public function getGetExternalTransactionStatus()
    {
        return $this->GetExternalTransactionStatus;
    }

    public function setGetExternalTransactionStatus($GetExternalTransactionStatus)
    {
        $this->GetExternalTransactionStatus = $GetExternalTransactionStatus;

        return $this;
    }

    public function getGetExternalConsumerInformation()
    {
        return $this->GetExternalConsumerInformation;
    }

    public function setGetExternalConsumerInformation($GetExternalConsumerInformation)
    {
        $this->GetExternalConsumerInformation = $GetExternalConsumerInformation;

        return $this;
    }

    public function getSendEMailNotification()
    {
        return $this->SendEMailNotification;
    }

    public function setSendEMailNotification($SendEMailNotification)
    {
        $this->SendEMailNotification = $SendEMailNotification;

        return $this;
    }

    public function getGetLastOrders()
    {
        return $this->GetLastOrders;
    }

    public function setGetLastOrders($GetLastOrders)
    {
        $this->GetLastOrders = $GetLastOrders;

        return $this;
    }

    public function getGetLastTransactions()
    {
        return $this->GetLastTransactions;
    }

    public function setGetLastTransactions($GetLastTransactions)
    {
        $this->GetLastTransactions = $GetLastTransactions;

        return $this;
    }

    public function getManageProducts()
    {
        return $this->ManageProducts;
    }

    public function setManageProducts($ManageProducts)
    {
        $this->ManageProducts = $ManageProducts;

        return $this;
    }

    public function getManageProductStock()
    {
        return $this->ManageProductStock;
    }

    public function setManageProductStock($ManageProductStock)
    {
        $this->ManageProductStock = $ManageProductStock;

        return $this;
    }

    public function getRequestCardRateForDCC()
    {
        return $this->RequestCardRateForDCC;
    }

    public function setRequestCardRateForDCC($RequestCardRateForDCC)
    {
        $this->RequestCardRateForDCC = $RequestCardRateForDCC;

        return $this;
    }

    public function getRequestMerchantRateForDynamicPricing()
    {
        return $this->RequestMerchantRateForDynamicPricing;
    }

    public function setRequestMerchantRateForDynamicPricing($RequestMerchantRateForDynamicPricing)
    {
        $this->RequestMerchantRateForDynamicPricing = $RequestMerchantRateForDynamicPricing;

        return $this;
    }

    public function getCreatePaymentURL()
    {
        return $this->CreatePaymentURL;
    }

    public function setCreatePaymentURL($CreatePaymentURL)
    {
        $this->CreatePaymentURL = $CreatePaymentURL;

        return $this;
    }

    public function getClientLocale()
    {
        return $this->ClientLocale;
    }

    public function setClientLocale($ClientLocale)
    {
        $this->ClientLocale = $ClientLocale;

        return $this;
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

}
