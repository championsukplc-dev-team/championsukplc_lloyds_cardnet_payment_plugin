<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\InquiryTransaction.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class InquiryTransaction 
{
	public $StoreId;

	public $OrderId;

	public $TDate;

	public $IpgTransactionId;

	public $MerchantTransactionId;

	public $MerchantDetails;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getTDate()
    {

        return $this->TDate;

    }


    public function setTDate($TDate)
    {

        $this->TDate = $TDate;

        return $this;

    }


    public function getIpgTransactionId()
    {

        return $this->IpgTransactionId;

    }


    public function setIpgTransactionId($IpgTransactionId)
    {

        $this->IpgTransactionId = $IpgTransactionId;

        return $this;

    }


    public function getMerchantTransactionId()
    {

        return $this->MerchantTransactionId;

    }


    public function setMerchantTransactionId($MerchantTransactionId)
    {

        $this->MerchantTransactionId = $MerchantTransactionId;

        return $this;

    }


    public function getMerchantDetails()
    {

        return $this->MerchantDetails;

    }


    public function setMerchantDetails($MerchantDetails)
    {

        $this->MerchantDetails = $MerchantDetails;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
