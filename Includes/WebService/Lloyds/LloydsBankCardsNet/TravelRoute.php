<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\TravelRoute.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class TravelRoute
{
    public $DepartureDate;
    public $Origin;
    public $Destination;
    public $CarrierCode;
    public $ServiceClass;
    public $StopoverType;
    public $FareBasisCode;
    public $DepartureTax;
    public $FlightNumber;

    public function getDepartureDate()
    {
        return $this->DepartureDate;
    }

    public function setDepartureDate($DepartureDate)
    {
        $this->DepartureDate = $DepartureDate;
        return $this;
    }

    public function getOrigin()
    {
        return $this->Origin;
    }

    public function setOrigin($Origin)
    {
        $this->Origin = $Origin;
        return $this;
    }

    public function getDestination()
    {
        return $this->Destination;
    }

    public function setDestination($Destination)
    {
        $this->Destination = $Destination;
        return $this;
    }

    public function getCarrierCode()
    {
        return $this->CarrierCode;
    }

    public function setCarrierCode($CarrierCode)
    {
        $this->CarrierCode = $CarrierCode;
        return $this;
    }

    public function getServiceClass()
    {
        return $this->ServiceClass;
    }

    public function setServiceClass($ServiceClass)
    {
        $this->ServiceClass = $ServiceClass;
        return $this;
    }

    public function getStopoverType()
    {
        return $this->StopoverType;
    }

    public function setStopoverType($StopoverType)
    {
        $this->StopoverType = $StopoverType;
        return $this;
    }

    public function getFareBasisCode()
    {
        return $this->FareBasisCode;
    }

    public function setFareBasisCode($FareBasisCode)
    {
        $this->FareBasisCode = $FareBasisCode;
        return $this;
    }

    public function getDepartureTax()
    {
        return $this->DepartureTax;
    }

    public function setDepartureTax($DepartureTax)
    {
        $this->DepartureTax = $DepartureTax;
        return $this;
    }

    public function getFlightNumber()
    {
        return $this->FlightNumber;
    }

    public function setFlightNumber($FlightNumber)
    {
        $this->FlightNumber = $FlightNumber;
        return $this;
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
