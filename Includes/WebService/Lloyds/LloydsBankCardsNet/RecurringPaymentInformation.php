<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\RecurringPaymentInformation.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class RecurringPaymentInformation 
{
	public $RecurringStartDate;

	public $InstallmentCount;

	public $MaximumFailures;

	public $InstallmentFrequency;

	public $InstallmentPeriod;
    


    public function getRecurringStartDate()
    {

        return $this->RecurringStartDate;

    }


    public function setRecurringStartDate($RecurringStartDate)
    {

        $this->RecurringStartDate = $RecurringStartDate;

        return $this;

    }


    public function getInstallmentCount()
    {

        return $this->InstallmentCount;

    }


    public function setInstallmentCount($InstallmentCount)
    {

        $this->InstallmentCount = $InstallmentCount;

        return $this;

    }


    public function getMaximumFailures()
    {

        return $this->MaximumFailures;

    }


    public function setMaximumFailures($MaximumFailures)
    {

        $this->MaximumFailures = $MaximumFailures;

        return $this;

    }


    public function getInstallmentFrequency()
    {

        return $this->InstallmentFrequency;

    }


    public function setInstallmentFrequency($InstallmentFrequency)
    {

        $this->InstallmentFrequency = $InstallmentFrequency;

        return $this;

    }


    public function getInstallmentPeriod()
    {

        return $this->InstallmentPeriod;

    }


    public function setInstallmentPeriod($InstallmentPeriod)
    {

        $this->InstallmentPeriod = $InstallmentPeriod;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
