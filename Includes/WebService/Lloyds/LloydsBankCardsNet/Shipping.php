<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Shipping.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Shipping 
{
	public $Type;

	public $Name;

	public $Address1;

	public $Address2;

	public $City;

	public $State;

	public $Zip;

	public $Country;
    
    public $Firstname;

    public $Surname;


    public function getType()
    {

        return $this->Type;

    }


    public function setType($Type)
    {

        $this->Type = $Type;

        return $this;

    }


    public function getName()
    {

        return $this->Name;

    }


    public function setName($Name)
    {

        $this->Name = $Name;

        return $this;

    }


    public function getAddress1()
    {

        return $this->Address1;

    }


    public function setAddress1($Address1)
    {

        $this->Address1 = $Address1;

        return $this;

    }


    public function getAddress2()
    {

        return $this->Address2;

    }


    public function setAddress2($Address2)
    {

        $this->Address2 = $Address2;

        return $this;

    }


    public function getCity()
    {

        return $this->City;

    }


    public function setCity($City)
    {

        $this->City = $City;

        return $this;

    }


    public function getState()
    {

        return $this->State;

    }


    public function setState($State)
    {

        $this->State = $State;

        return $this;

    }


    public function getZip()
    {

        return $this->Zip;

    }


    public function setZip($Zip)
    {

        $this->Zip = $Zip;

        return $this;

    }


    public function getCountry()
    {

        return $this->Country;

    }


    public function setCountry($Country)
    {

        $this->Country = $Country;

        return $this;

    }

    public function getFirstname()
    {

        return $this->Firstname;

    }


    public function setFirstname($Firstname)
    {

        $this->Firstname = $Firstname;

        return $this;

    }


    public function getSurname()
    {

        return $this->Surname;

    }


    public function setSurname($Surname)
    {

        $this->Surname = $Surname;

        return $this;

    }

    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
