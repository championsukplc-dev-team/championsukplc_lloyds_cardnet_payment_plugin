<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\VerificationRedirectResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class VerificationRedirectResponse
{
    public $AcsURL;
    public $PaReq;
    public $TermUrl;
    public $MD;

    public function getAcsURL()
    {
        return $this->AcsURL;
    }


    public function setAcsURL($AcsURL)
    {
        $this->AcsURL = $AcsURL;
        return $this;
    }


    public function getPaReq()
    {
        return $this->PaReq;
    }


    public function setPaReq($PaReq)
    {
        $this->PaReq = $PaReq;
        return $this;
    }


    public function getTermUrl()
    {
        return $this->TermUrl;
    }


    public function setTermUrl($TermUrl)
    {
        $this->TermUrl = $TermUrl;
        return $this;
    }


    public function getMD()
    {
        return $this->MD;
    }


    public function setMD($MD)
    {
        $this->MD = $MD;
        return $this;
    }


    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
