<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\SplitShipment.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class SplitShipment 
{
	public $SequenceCount;

	public $FinalShipment;
    


    public function getSequenceCount()
    {

        return $this->SequenceCount;

    }


    public function setSequenceCount($SequenceCount)
    {

        $this->SequenceCount = $SequenceCount;

        return $this;

    }


    public function getFinalShipment()
    {

        return $this->FinalShipment;

    }


    public function setFinalShipment($FinalShipment)
    {

        $this->FinalShipment = $FinalShipment;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
