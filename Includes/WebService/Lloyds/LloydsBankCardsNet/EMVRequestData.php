<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\EMVRequestData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class EMVRequestData 
{
	/*public $AdditionalTerminalCapabilities-9F40;

	public $AdditionalTerminalReadCapabilities-DF8301;

	public $AmountAuthorised-9F02;

	public $AmountOther-9F03;

	public $ApplicationCryptogram-9F26;

	public $ApplicationIdentifier-4F;

	public $ApplicationIdentifierTerminal-9F06;

	public $ApplicationInterchangeProfile-82;

	public $ApplicationLabel-50;

	public $ApplicationPANSequenceNumber-5F34;

	public $ApplicationTransactionCounter-9F36;

	public $ApplicationUsageControl-9F07;

	public $ApplicationVersionNumber-9F09;

	public $CardType-DF60;

	public $CommandDataInternalAuthenticate-DF03;

	public $Container-FF04;

	public $Container-FF05;

	public $CryptogramInformationData-9F27;

	public $CVMResults-9F34;

	public $DedicatedFileName-84;

	public $DynamicDataAuthenticationObjectList-9F49;

	public $EMV-9F6E;

	public $EMV-9F7C;

	public $EMV-9F67;

	public $ErrorDetection-DF02;

	public $InterfaceDeviceSerialNumber-9F1E;

	public $IssuerApplicationData-9F10;

	public $IssuerAuthenticationData-91;

	public $IssuerAuthorizationResponseCode-8A;

	public $IssuerScriptResults-DF01;

	public $MerchantIdentifier-9F16;

	public $PointOfServiceEntryMode-9F39;

	public $ReceiptNumber-DF8161;

	public $ResponseDataInternalAuthenticate-DF04;

	public $TerminalApplicationVersionNumber-9F09;

	public $TerminalCapabilities-9F33;

	public $TerminalCountry-9F1A;

	public $TerminalType-9F35;

	public $TerminalVerificationResults-95;

	public $TraceNumber-DF8260;

	public $TransactionCategoryCode-9F53;

	public $TransactionCertificateHashValue-98;

	public $TransactionCurrencyCode-5F2A;

	public $TransactionDate-9A;

	public $TransactionTime-9F21;

	public $TransactionSequenceCounter-9F41;

	public $TransactionStatusInformation-9B;

	public $TransactionType-9C;

	public $UnpredictableNumber-9F37;

	public $IssuerScriptResults-DF31;
    


    public function getAdditionalTerminalCapabilities-9F40()
    {

        return $this->AdditionalTerminalCapabilities-9F40;

    }


    public function setAdditionalTerminalCapabilities-9F40($AdditionalTerminalCapabilities-9F40)
    {

        $this->AdditionalTerminalCapabilities-9F40 = $AdditionalTerminalCapabilities-9F40;

        return $this;

    }


    public function getAdditionalTerminalReadCapabilities-DF8301()
    {

        return $this->AdditionalTerminalReadCapabilities-DF8301;

    }


    public function setAdditionalTerminalReadCapabilities-DF8301($AdditionalTerminalReadCapabilities-DF8301)
    {

        $this->AdditionalTerminalReadCapabilities-DF8301 = $AdditionalTerminalReadCapabilities-DF8301;

        return $this;

    }


    public function getAmountAuthorised-9F02()
    {

        return $this->AmountAuthorised-9F02;

    }


    public function setAmountAuthorised-9F02($AmountAuthorised-9F02)
    {

        $this->AmountAuthorised-9F02 = $AmountAuthorised-9F02;

        return $this;

    }


    public function getAmountOther-9F03()
    {

        return $this->AmountOther-9F03;

    }


    public function setAmountOther-9F03($AmountOther-9F03)
    {

        $this->AmountOther-9F03 = $AmountOther-9F03;

        return $this;

    }


    public function getApplicationCryptogram-9F26()
    {

        return $this->ApplicationCryptogram-9F26;

    }


    public function setApplicationCryptogram-9F26($ApplicationCryptogram-9F26)
    {

        $this->ApplicationCryptogram-9F26 = $ApplicationCryptogram-9F26;

        return $this;

    }


    public function getApplicationIdentifier-4F()
    {

        return $this->ApplicationIdentifier-4F;

    }


    public function setApplicationIdentifier-4F($ApplicationIdentifier-4F)
    {

        $this->ApplicationIdentifier-4F = $ApplicationIdentifier-4F;

        return $this;

    }


    public function getApplicationIdentifierTerminal-9F06()
    {

        return $this->ApplicationIdentifierTerminal-9F06;

    }


    public function setApplicationIdentifierTerminal-9F06($ApplicationIdentifierTerminal-9F06)
    {

        $this->ApplicationIdentifierTerminal-9F06 = $ApplicationIdentifierTerminal-9F06;

        return $this;

    }


    public function getApplicationInterchangeProfile-82()
    {

        return $this->ApplicationInterchangeProfile-82;

    }


    public function setApplicationInterchangeProfile-82($ApplicationInterchangeProfile-82)
    {

        $this->ApplicationInterchangeProfile-82 = $ApplicationInterchangeProfile-82;

        return $this;

    }


    public function getApplicationLabel-50()
    {

        return $this->ApplicationLabel-50;

    }


    public function setApplicationLabel-50($ApplicationLabel-50)
    {

        $this->ApplicationLabel-50 = $ApplicationLabel-50;

        return $this;

    }


    public function getApplicationPANSequenceNumber-5F34()
    {

        return $this->ApplicationPANSequenceNumber-5F34;

    }


    public function setApplicationPANSequenceNumber-5F34($ApplicationPANSequenceNumber-5F34)
    {

        $this->ApplicationPANSequenceNumber-5F34 = $ApplicationPANSequenceNumber-5F34;

        return $this;

    }


    public function getApplicationTransactionCounter-9F36()
    {

        return $this->ApplicationTransactionCounter-9F36;

    }


    public function setApplicationTransactionCounter-9F36($ApplicationTransactionCounter-9F36)
    {

        $this->ApplicationTransactionCounter-9F36 = $ApplicationTransactionCounter-9F36;

        return $this;

    }


    public function getApplicationUsageControl-9F07()
    {

        return $this->ApplicationUsageControl-9F07;

    }


    public function setApplicationUsageControl-9F07($ApplicationUsageControl-9F07)
    {

        $this->ApplicationUsageControl-9F07 = $ApplicationUsageControl-9F07;

        return $this;

    }


    public function getApplicationVersionNumber-9F09()
    {

        return $this->ApplicationVersionNumber-9F09;

    }


    public function setApplicationVersionNumber-9F09($ApplicationVersionNumber-9F09)
    {

        $this->ApplicationVersionNumber-9F09 = $ApplicationVersionNumber-9F09;

        return $this;

    }


    public function getCardType-DF60()
    {

        return $this->CardType-DF60;

    }


    public function setCardType-DF60($CardType-DF60)
    {

        $this->CardType-DF60 = $CardType-DF60;

        return $this;

    }


    public function getCommandDataInternalAuthenticate-DF03()
    {

        return $this->CommandDataInternalAuthenticate-DF03;

    }


    public function setCommandDataInternalAuthenticate-DF03($CommandDataInternalAuthenticate-DF03)
    {

        $this->CommandDataInternalAuthenticate-DF03 = $CommandDataInternalAuthenticate-DF03;

        return $this;

    }


    public function getContainer-FF04()
    {

        return $this->Container-FF04;

    }


    public function setContainer-FF04($Container-FF04)
    {

        $this->Container-FF04 = $Container-FF04;

        return $this;

    }


    public function getContainer-FF05()
    {

        return $this->Container-FF05;

    }


    public function setContainer-FF05($Container-FF05)
    {

        $this->Container-FF05 = $Container-FF05;

        return $this;

    }


    public function getCryptogramInformationData-9F27()
    {

        return $this->CryptogramInformationData-9F27;

    }


    public function setCryptogramInformationData-9F27($CryptogramInformationData-9F27)
    {

        $this->CryptogramInformationData-9F27 = $CryptogramInformationData-9F27;

        return $this;

    }


    public function getCVMResults-9F34()
    {

        return $this->CVMResults-9F34;

    }


    public function setCVMResults-9F34($CVMResults-9F34)
    {

        $this->CVMResults-9F34 = $CVMResults-9F34;

        return $this;

    }


    public function getDedicatedFileName-84()
    {

        return $this->DedicatedFileName-84;

    }


    public function setDedicatedFileName-84($DedicatedFileName-84)
    {

        $this->DedicatedFileName-84 = $DedicatedFileName-84;

        return $this;

    }


    public function getDynamicDataAuthenticationObjectList-9F49()
    {

        return $this->DynamicDataAuthenticationObjectList-9F49;

    }


    public function setDynamicDataAuthenticationObjectList-9F49($DynamicDataAuthenticationObjectList-9F49)
    {

        $this->DynamicDataAuthenticationObjectList-9F49 = $DynamicDataAuthenticationObjectList-9F49;

        return $this;

    }


    public function getEMV-9F6E()
    {

        return $this->EMV-9F6E;

    }


    public function setEMV-9F6E($EMV-9F6E)
    {

        $this->EMV-9F6E = $EMV-9F6E;

        return $this;

    }


    public function getEMV-9F7C()
    {

        return $this->EMV-9F7C;

    }


    public function setEMV-9F7C($EMV-9F7C)
    {

        $this->EMV-9F7C = $EMV-9F7C;

        return $this;

    }


    public function getEMV-9F67()
    {

        return $this->EMV-9F67;

    }


    public function setEMV-9F67($EMV-9F67)
    {

        $this->EMV-9F67 = $EMV-9F67;

        return $this;

    }


    public function getErrorDetection-DF02()
    {

        return $this->ErrorDetection-DF02;

    }


    public function setErrorDetection-DF02($ErrorDetection-DF02)
    {

        $this->ErrorDetection-DF02 = $ErrorDetection-DF02;

        return $this;

    }


    public function getInterfaceDeviceSerialNumber-9F1E()
    {

        return $this->InterfaceDeviceSerialNumber-9F1E;

    }


    public function setInterfaceDeviceSerialNumber-9F1E($InterfaceDeviceSerialNumber-9F1E)
    {

        $this->InterfaceDeviceSerialNumber-9F1E = $InterfaceDeviceSerialNumber-9F1E;

        return $this;

    }


    public function getIssuerApplicationData-9F10()
    {

        return $this->IssuerApplicationData-9F10;

    }


    public function setIssuerApplicationData-9F10($IssuerApplicationData-9F10)
    {

        $this->IssuerApplicationData-9F10 = $IssuerApplicationData-9F10;

        return $this;

    }


    public function getIssuerAuthenticationData-91()
    {

        return $this->IssuerAuthenticationData-91;

    }


    public function setIssuerAuthenticationData-91($IssuerAuthenticationData-91)
    {

        $this->IssuerAuthenticationData-91 = $IssuerAuthenticationData-91;

        return $this;

    }


    public function getIssuerAuthorizationResponseCode-8A()
    {

        return $this->IssuerAuthorizationResponseCode-8A;

    }


    public function setIssuerAuthorizationResponseCode-8A($IssuerAuthorizationResponseCode-8A)
    {

        $this->IssuerAuthorizationResponseCode-8A = $IssuerAuthorizationResponseCode-8A;

        return $this;

    }


    public function getIssuerScriptResults-DF01()
    {

        return $this->IssuerScriptResults-DF01;

    }


    public function setIssuerScriptResults-DF01($IssuerScriptResults-DF01)
    {

        $this->IssuerScriptResults-DF01 = $IssuerScriptResults-DF01;

        return $this;

    }


    public function getMerchantIdentifier-9F16()
    {

        return $this->MerchantIdentifier-9F16;

    }


    public function setMerchantIdentifier-9F16($MerchantIdentifier-9F16)
    {

        $this->MerchantIdentifier-9F16 = $MerchantIdentifier-9F16;

        return $this;

    }


    public function getPointOfServiceEntryMode-9F39()
    {

        return $this->PointOfServiceEntryMode-9F39;

    }


    public function setPointOfServiceEntryMode-9F39($PointOfServiceEntryMode-9F39)
    {

        $this->PointOfServiceEntryMode-9F39 = $PointOfServiceEntryMode-9F39;

        return $this;

    }


    public function getReceiptNumber-DF8161()
    {

        return $this->ReceiptNumber-DF8161;

    }


    public function setReceiptNumber-DF8161($ReceiptNumber-DF8161)
    {

        $this->ReceiptNumber-DF8161 = $ReceiptNumber-DF8161;

        return $this;

    }


    public function getResponseDataInternalAuthenticate-DF04()
    {

        return $this->ResponseDataInternalAuthenticate-DF04;

    }


    public function setResponseDataInternalAuthenticate-DF04($ResponseDataInternalAuthenticate-DF04)
    {

        $this->ResponseDataInternalAuthenticate-DF04 = $ResponseDataInternalAuthenticate-DF04;

        return $this;

    }


    public function getTerminalApplicationVersionNumber-9F09()
    {

        return $this->TerminalApplicationVersionNumber-9F09;

    }


    public function setTerminalApplicationVersionNumber-9F09($TerminalApplicationVersionNumber-9F09)
    {

        $this->TerminalApplicationVersionNumber-9F09 = $TerminalApplicationVersionNumber-9F09;

        return $this;

    }


    public function getTerminalCapabilities-9F33()
    {

        return $this->TerminalCapabilities-9F33;

    }


    public function setTerminalCapabilities-9F33($TerminalCapabilities-9F33)
    {

        $this->TerminalCapabilities-9F33 = $TerminalCapabilities-9F33;

        return $this;

    }


    public function getTerminalCountry-9F1A()
    {

        return $this->TerminalCountry-9F1A;

    }


    public function setTerminalCountry-9F1A($TerminalCountry-9F1A)
    {

        $this->TerminalCountry-9F1A = $TerminalCountry-9F1A;

        return $this;

    }


    public function getTerminalType-9F35()
    {

        return $this->TerminalType-9F35;

    }


    public function setTerminalType-9F35($TerminalType-9F35)
    {

        $this->TerminalType-9F35 = $TerminalType-9F35;

        return $this;

    }


    public function getTerminalVerificationResults-95()
    {

        return $this->TerminalVerificationResults-95;

    }


    public function setTerminalVerificationResults-95($TerminalVerificationResults-95)
    {

        $this->TerminalVerificationResults-95 = $TerminalVerificationResults-95;

        return $this;

    }


    public function getTraceNumber-DF8260()
    {

        return $this->TraceNumber-DF8260;

    }


    public function setTraceNumber-DF8260($TraceNumber-DF8260)
    {

        $this->TraceNumber-DF8260 = $TraceNumber-DF8260;

        return $this;

    }


    public function getTransactionCategoryCode-9F53()
    {

        return $this->TransactionCategoryCode-9F53;

    }


    public function setTransactionCategoryCode-9F53($TransactionCategoryCode-9F53)
    {

        $this->TransactionCategoryCode-9F53 = $TransactionCategoryCode-9F53;

        return $this;

    }


    public function getTransactionCertificateHashValue-98()
    {

        return $this->TransactionCertificateHashValue-98;

    }


    public function setTransactionCertificateHashValue-98($TransactionCertificateHashValue-98)
    {

        $this->TransactionCertificateHashValue-98 = $TransactionCertificateHashValue-98;

        return $this;

    }


    public function getTransactionCurrencyCode-5F2A()
    {

        return $this->TransactionCurrencyCode-5F2A;

    }


    public function setTransactionCurrencyCode-5F2A($TransactionCurrencyCode-5F2A)
    {

        $this->TransactionCurrencyCode-5F2A = $TransactionCurrencyCode-5F2A;

        return $this;

    }


    public function getTransactionDate-9A()
    {

        return $this->TransactionDate-9A;

    }


    public function setTransactionDate-9A($TransactionDate-9A)
    {

        $this->TransactionDate-9A = $TransactionDate-9A;

        return $this;

    }


    public function getTransactionTime-9F21()
    {

        return $this->TransactionTime-9F21;

    }


    public function setTransactionTime-9F21($TransactionTime-9F21)
    {

        $this->TransactionTime-9F21 = $TransactionTime-9F21;

        return $this;

    }


    public function getTransactionSequenceCounter-9F41()
    {

        return $this->TransactionSequenceCounter-9F41;

    }


    public function setTransactionSequenceCounter-9F41($TransactionSequenceCounter-9F41)
    {

        $this->TransactionSequenceCounter-9F41 = $TransactionSequenceCounter-9F41;

        return $this;

    }


    public function getTransactionStatusInformation-9B()
    {

        return $this->TransactionStatusInformation-9B;

    }


    public function setTransactionStatusInformation-9B($TransactionStatusInformation-9B)
    {

        $this->TransactionStatusInformation-9B = $TransactionStatusInformation-9B;

        return $this;

    }


    public function getTransactionType-9C()
    {

        return $this->TransactionType-9C;

    }


    public function setTransactionType-9C($TransactionType-9C)
    {

        $this->TransactionType-9C = $TransactionType-9C;

        return $this;

    }


    public function getUnpredictableNumber-9F37()
    {

        return $this->UnpredictableNumber-9F37;

    }


    public function setUnpredictableNumber-9F37($UnpredictableNumber-9F37)
    {

        $this->UnpredictableNumber-9F37 = $UnpredictableNumber-9F37;

        return $this;

    }


    public function getIssuerScriptResults-DF31()
    {

        return $this->IssuerScriptResults-DF31;

    }


    public function setIssuerScriptResults-DF31($IssuerScriptResults-DF31)
    {

        $this->IssuerScriptResults-DF31 = $IssuerScriptResults-DF31;

        return $this;

    }*/




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
