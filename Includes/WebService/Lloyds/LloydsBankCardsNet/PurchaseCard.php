<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\PurchaseCard.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class PurchaseCard 
{
	public $CustomerReferenceID;

	public $SupplierInvoiceNumber;

	public $SupplierVATRegistrationNumber;

	public $TotalDiscountAmountAndRate;

	public $VATShippingAmountAndRate;

	public $LineItemData;
    


    public function getCustomerReferenceID()
    {

        return $this->CustomerReferenceID;

    }


    public function setCustomerReferenceID($CustomerReferenceID)
    {

        $this->CustomerReferenceID = $CustomerReferenceID;

        return $this;

    }


    public function getSupplierInvoiceNumber()
    {

        return $this->SupplierInvoiceNumber;

    }


    public function setSupplierInvoiceNumber($SupplierInvoiceNumber)
    {

        $this->SupplierInvoiceNumber = $SupplierInvoiceNumber;

        return $this;

    }


    public function getSupplierVATRegistrationNumber()
    {

        return $this->SupplierVATRegistrationNumber;

    }


    public function setSupplierVATRegistrationNumber($SupplierVATRegistrationNumber)
    {

        $this->SupplierVATRegistrationNumber = $SupplierVATRegistrationNumber;

        return $this;

    }


    public function getTotalDiscountAmountAndRate()
    {

        return $this->TotalDiscountAmountAndRate;

    }


    public function setTotalDiscountAmountAndRate($TotalDiscountAmountAndRate)
    {

        $this->TotalDiscountAmountAndRate = $TotalDiscountAmountAndRate;

        return $this;

    }


    public function getVATShippingAmountAndRate()
    {

        return $this->VATShippingAmountAndRate;

    }


    public function setVATShippingAmountAndRate($VATShippingAmountAndRate)
    {

        $this->VATShippingAmountAndRate = $VATShippingAmountAndRate;

        return $this;

    }


    public function getLineItemData()
    {

        return $this->LineItemData;

    }


    public function setLineItemData($LineItemData)
    {

        $this->LineItemData = $LineItemData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
