<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Transaction.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Transaction 
{
	public $CreditCardTxType;

	public $CreditCardData;

	public $CreditCard3DSecure;

	public $MCC6012Details;

	public $EMVCardPresentRequest;

	public $cardFunction;

	public $recurringType;

	public $PaymentFacilitator;

	public $SubMerchant;

	public $Wallet;

	public $CustomerCardTxType;

	public $CustomerCardData;

	public $DE_DirectDebitTxType;

	public $DE_DirectDebitData;

	public $DE_DirectDebitEMV;

	public $IdealTxType;

	public $PayPalTxType;

	public $SofortTxType;

	public $TopUpTxType;

	public $PaymentUrlTxType;

	public $KlarnaTxType;

	public $KlarnaPClassID;

	public $SepaTxType;

	public $SepaData;

	public $Payment;

	public $TransactionDetails;

	public $Billing;

	public $Shipping;

	public $ClientLocale;

	public $Basket;
    


    public function getCreditCardTxType()
    {

        return $this->CreditCardTxType;

    }


    public function setCreditCardTxType($CreditCardTxType)
    {

        $this->CreditCardTxType = $CreditCardTxType;

        return $this;

    }


    public function getCreditCardData()
    {

        return $this->CreditCardData;

    }


    public function setCreditCardData($CreditCardData)
    {

        $this->CreditCardData = $CreditCardData;

        return $this;

    }


    public function getCreditCard3DSecure()
    {

        return $this->CreditCard3DSecure;

    }


    public function setCreditCard3DSecure($CreditCard3DSecure)
    {

        $this->CreditCard3DSecure = $CreditCard3DSecure;

        return $this;

    }


    public function getMCC6012Details()
    {

        return $this->MCC6012Details;

    }


    public function setMCC6012Details($MCC6012Details)
    {

        $this->MCC6012Details = $MCC6012Details;

        return $this;

    }


    public function getEMVCardPresentRequest()
    {

        return $this->EMVCardPresentRequest;

    }


    public function setEMVCardPresentRequest($EMVCardPresentRequest)
    {

        $this->EMVCardPresentRequest = $EMVCardPresentRequest;

        return $this;

    }


    public function getCardFunction()
    {

        return $this->cardFunction;

    }


    public function setCardFunction($cardFunction)
    {

        $this->cardFunction = $cardFunction;

        return $this;

    }


    public function getRecurringType()
    {

        return $this->recurringType;

    }


    public function setRecurringType($recurringType)
    {

        $this->recurringType = $recurringType;

        return $this;

    }


    public function getPaymentFacilitator()
    {

        return $this->PaymentFacilitator;

    }


    public function setPaymentFacilitator($PaymentFacilitator)
    {

        $this->PaymentFacilitator = $PaymentFacilitator;

        return $this;

    }


    public function getSubMerchant()
    {

        return $this->SubMerchant;

    }


    public function setSubMerchant($SubMerchant)
    {

        $this->SubMerchant = $SubMerchant;

        return $this;

    }


    public function getWallet()
    {

        return $this->Wallet;

    }


    public function setWallet($Wallet)
    {

        $this->Wallet = $Wallet;

        return $this;

    }


    public function getCustomerCardTxType()
    {

        return $this->CustomerCardTxType;

    }


    public function setCustomerCardTxType($CustomerCardTxType)
    {

        $this->CustomerCardTxType = $CustomerCardTxType;

        return $this;

    }


    public function getCustomerCardData()
    {

        return $this->CustomerCardData;

    }


    public function setCustomerCardData($CustomerCardData)
    {

        $this->CustomerCardData = $CustomerCardData;

        return $this;

    }


    public function getDE_DirectDebitTxType()
    {

        return $this->DE_DirectDebitTxType;

    }


    public function setDE_DirectDebitTxType($DE_DirectDebitTxType)
    {

        $this->DE_DirectDebitTxType = $DE_DirectDebitTxType;

        return $this;

    }


    public function getDE_DirectDebitData()
    {

        return $this->DE_DirectDebitData;

    }


    public function setDE_DirectDebitData($DE_DirectDebitData)
    {

        $this->DE_DirectDebitData = $DE_DirectDebitData;

        return $this;

    }


    public function getDE_DirectDebitEMV()
    {

        return $this->DE_DirectDebitEMV;

    }


    public function setDE_DirectDebitEMV($DE_DirectDebitEMV)
    {

        $this->DE_DirectDebitEMV = $DE_DirectDebitEMV;

        return $this;

    }


    public function getIdealTxType()
    {

        return $this->IdealTxType;

    }


    public function setIdealTxType($IdealTxType)
    {

        $this->IdealTxType = $IdealTxType;

        return $this;

    }


    public function getPayPalTxType()
    {

        return $this->PayPalTxType;

    }


    public function setPayPalTxType($PayPalTxType)
    {

        $this->PayPalTxType = $PayPalTxType;

        return $this;

    }


    public function getSofortTxType()
    {

        return $this->SofortTxType;

    }


    public function setSofortTxType($SofortTxType)
    {

        $this->SofortTxType = $SofortTxType;

        return $this;

    }


    public function getTopUpTxType()
    {

        return $this->TopUpTxType;

    }


    public function setTopUpTxType($TopUpTxType)
    {

        $this->TopUpTxType = $TopUpTxType;

        return $this;

    }


    public function getPaymentUrlTxType()
    {

        return $this->PaymentUrlTxType;

    }


    public function setPaymentUrlTxType($PaymentUrlTxType)
    {

        $this->PaymentUrlTxType = $PaymentUrlTxType;

        return $this;

    }


    public function getKlarnaTxType()
    {

        return $this->KlarnaTxType;

    }


    public function setKlarnaTxType($KlarnaTxType)
    {

        $this->KlarnaTxType = $KlarnaTxType;

        return $this;

    }


    public function getKlarnaPClassID()
    {

        return $this->KlarnaPClassID;

    }


    public function setKlarnaPClassID($KlarnaPClassID)
    {

        $this->KlarnaPClassID = $KlarnaPClassID;

        return $this;

    }


    public function getSepaTxType()
    {

        return $this->SepaTxType;

    }


    public function setSepaTxType($SepaTxType)
    {

        $this->SepaTxType = $SepaTxType;

        return $this;

    }


    public function getSepaData()
    {

        return $this->SepaData;

    }


    public function setSepaData($SepaData)
    {

        $this->SepaData = $SepaData;

        return $this;

    }


    public function getPayment()
    {

        return $this->Payment;

    }


    public function setPayment($Payment)
    {

        $this->Payment = $Payment;

        return $this;

    }


    public function getTransactionDetails()
    {

        return $this->TransactionDetails;

    }


    public function setTransactionDetails($TransactionDetails)
    {

        $this->TransactionDetails = $TransactionDetails;

        return $this;

    }


    public function getBilling()
    {

        return $this->Billing;

    }


    public function setBilling($Billing)
    {

        $this->Billing = $Billing;

        return $this;

    }


    public function getShipping()
    {

        return $this->Shipping;

    }


    public function setShipping($Shipping)
    {

        $this->Shipping = $Shipping;

        return $this;

    }


    public function getClientLocale()
    {

        return $this->ClientLocale;

    }


    public function setClientLocale($ClientLocale)
    {

        $this->ClientLocale = $ClientLocale;

        return $this;

    }


    public function getBasket()
    {

        return $this->Basket;

    }


    public function setBasket($Basket)
    {

        $this->Basket = $Basket;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
