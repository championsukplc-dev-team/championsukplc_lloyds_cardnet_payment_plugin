<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\MCC6012Details.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class MCC6012Details 
{
	public $BirthDate;

	public $AccountFirst6;

	public $AccountLast4;

	public $AccountNumber;

	public $PostCode;

	public $Surname;
    


    public function getBirthDate()
    {

        return $this->BirthDate;

    }


    public function setBirthDate($BirthDate)
    {

        $this->BirthDate = $BirthDate;

        return $this;

    }


    public function getAccountFirst6()
    {

        return $this->AccountFirst6;

    }


    public function setAccountFirst6($AccountFirst6)
    {

        $this->AccountFirst6 = $AccountFirst6;

        return $this;

    }


    public function getAccountLast4()
    {

        return $this->AccountLast4;

    }


    public function setAccountLast4($AccountLast4)
    {

        $this->AccountLast4 = $AccountLast4;

        return $this;

    }


    public function getAccountNumber()
    {

        return $this->AccountNumber;

    }


    public function setAccountNumber($AccountNumber)
    {

        $this->AccountNumber = $AccountNumber;

        return $this;

    }


    public function getPostCode()
    {

        return $this->PostCode;

    }


    public function setPostCode($PostCode)
    {

        $this->PostCode = $PostCode;

        return $this;

    }


    public function getSurname()
    {

        return $this->Surname;

    }


    public function setSurname($Surname)
    {

        $this->Surname = $Surname;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
