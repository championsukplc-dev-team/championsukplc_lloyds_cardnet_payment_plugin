<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\GetExternalConsumerInformation.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class GetExternalConsumerInformation 
{
	public $StoreId;

	public $OrderId;

	public $DataProvider;

	public $FirstName;

	public $Surname;

	public $Birthday;

	public $Street;

	public $HouseNumber;

	public $PostCode;

	public $City;

	public $Country;

	public $DisplayProcessorMessages;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getDataProvider()
    {

        return $this->DataProvider;

    }


    public function setDataProvider($DataProvider)
    {

        $this->DataProvider = $DataProvider;

        return $this;

    }


    public function getFirstName()
    {

        return $this->FirstName;

    }


    public function setFirstName($FirstName)
    {

        $this->FirstName = $FirstName;

        return $this;

    }


    public function getSurname()
    {

        return $this->Surname;

    }


    public function setSurname($Surname)
    {

        $this->Surname = $Surname;

        return $this;

    }


    public function getBirthday()
    {

        return $this->Birthday;

    }


    public function setBirthday($Birthday)
    {

        $this->Birthday = $Birthday;

        return $this;

    }


    public function getStreet()
    {

        return $this->Street;

    }


    public function setStreet($Street)
    {

        $this->Street = $Street;

        return $this;

    }


    public function getHouseNumber()
    {

        return $this->HouseNumber;

    }


    public function setHouseNumber($HouseNumber)
    {

        $this->HouseNumber = $HouseNumber;

        return $this;

    }


    public function getPostCode()
    {

        return $this->PostCode;

    }


    public function setPostCode($PostCode)
    {

        $this->PostCode = $PostCode;

        return $this;

    }


    public function getCity()
    {

        return $this->City;

    }


    public function setCity($City)
    {

        $this->City = $City;

        return $this;

    }


    public function getCountry()
    {

        return $this->Country;

    }


    public function setCountry($Country)
    {

        $this->Country = $Country;

        return $this;

    }


    public function getDisplayProcessorMessages()
    {

        return $this->DisplayProcessorMessages;

    }


    public function setDisplayProcessorMessages($DisplayProcessorMessages)
    {

        $this->DisplayProcessorMessages = $DisplayProcessorMessages;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
