<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\EMVCardPresentResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class EMVCardPresentResponse 
{
	public $EMVResponseData;

	public $TLVData;

	public $CryptData;
    


    public function getEMVResponseData()
    {

        return $this->EMVResponseData;

    }


    public function setEMVResponseData($EMVResponseData)
    {

        $this->EMVResponseData = $EMVResponseData;

        return $this;

    }


    public function getTLVData()
    {

        return $this->TLVData;

    }


    public function setTLVData($TLVData)
    {

        $this->TLVData = $TLVData;

        return $this;

    }


    public function getCryptData()
    {

        return $this->CryptData;

    }


    public function setCryptData($CryptData)
    {

        $this->CryptData = $CryptData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
