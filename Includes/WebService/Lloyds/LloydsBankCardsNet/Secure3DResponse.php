<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Secure3DResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Secure3DResponse 
{
	public $ResponseCode3dSecure;

	public $Secure3DVerificationResponse;
    


    public function getResponseCode3dSecure()
    {

        return $this->ResponseCode3dSecure;

    }


    public function setResponseCode3dSecure($ResponseCode3dSecure)
    {

        $this->ResponseCode3dSecure = $ResponseCode3dSecure;

        return $this;

    }


    public function getSecure3DVerificationResponse()
    {

        return $this->Secure3DVerificationResponse;

    }


    public function setSecure3DVerificationResponse($Secure3DVerificationResponse)
    {

        $this->Secure3DVerificationResponse = $Secure3DVerificationResponse;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
