<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\TransactionValues.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class TransactionValues
{
    public $IPGApiOrderResponse;
    public $ReceiptNumber;
    public $ResponseCode;
    public $TraceNumber;
    public $Brand;
    public $TransactionType;
    public $TransactionState;
    public $UserID;
    public $GiroPayTxType;
    public $GiroPayData;
    public $SubmissionComponent;
    public $AccountOwner;
    public $IBAN;
    public $BIC;
    public $AdditionalRequestParameters;

    public function getIPGApiOrderResponse()
    {
        return $this->IPGApiOrderResponse;
    }

    public function setIPGApiOrderResponse($IPGApiOrderResponse)
    {
        $this->IPGApiOrderResponse = $IPGApiOrderResponse;
        return $this;
    }

    public function getReceiptNumber()
    {
        return $this->ReceiptNumber;
    }

    public function setReceiptNumber($ReceiptNumber)
    {
        $this->ReceiptNumber = $ReceiptNumber;
        return $this;
    }

    public function getResponseCode()
    {
        return $this->ResponseCode;
    }

    public function setResponseCode($ResponseCode)
    {
        $this->ResponseCode = $ResponseCode;
        return $this;
    }

    public function getTraceNumber()
    {
        return $this->TraceNumber;
    }

    public function setTraceNumber($TraceNumber)
    {
        $this->TraceNumber = $TraceNumber;
        return $this;
    }

    public function getBrand()
    {
        return $this->Brand;
    }

    public function setBrand($Brand)
    {
        $this->Brand = $Brand;
        return $this;
    }

    public function getTransactionType()
    {
        return $this->TransactionType;
    }

    public function setTransactionType($TransactionType)
    {
        $this->TransactionType = $TransactionType;
        return $this;
    }

    public function getTransactionState()
    {
        return $this->TransactionState;
    }

    public function setTransactionState($TransactionState)
    {
        $this->TransactionState = $TransactionState;
        return $this;
    }

    public function getUserID()
    {
        return $this->UserID;
    }

    public function setUserID($UserID)
    {
        $this->UserID = $UserID;
        return $this;
    }

    public function getGiroPayTxType()
    {
        return $this->GiroPayTxType;
    }

    public function setGiroPayTxType($GiroPayTxType)
    {
        $this->GiroPayTxType = $GiroPayTxType;
        return $this;
    }

    public function getGiroPayData()
    {
        return $this->GiroPayData;
    }

    public function setGiroPayData($GiroPayData)
    {
        $this->GiroPayData = $GiroPayData;
        return $this;
    }

    public function getSubmissionComponent()
    {
        return $this->SubmissionComponent;
    }

    public function setSubmissionComponent($SubmissionComponent)
    {
        $this->SubmissionComponent = $SubmissionComponent;
        return $this;
    }

    public function getAccountOwner()
    {
        return $this->AccountOwner;
    }

    public function setAccountOwner($AccountOwner)
    {
        $this->AccountOwner = $AccountOwner;
        return $this;
    }

    public function getIBAN()
    {
        return $this->IBAN;
    }

    public function setIBAN($IBAN)
    {
        $this->IBAN = $IBAN;
        return $this;
    }

    public function getBIC()
    {
        return $this->BIC;
    }

    public function setBIC($BIC)
    {
        $this->BIC = $BIC;
        return $this;
    }

    public function getAdditionalRequestParameters()
    {
        return $this->AdditionalRequestParameters;
    }

    public function setAdditionalRequestParameters($AdditionalRequestParameters)
    {
        $this->AdditionalRequestParameters = $AdditionalRequestParameters;
        return $this;
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
