<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\DataStorageItem.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class DataStorageItem 
{
	public $CreditCardData;

	public $DE_DirectDebitData;

	public $OrderId;

	public $Function;

	public $HostedDataID;

	public $DeclineHostedDataDuplicates;

	public $TokenType;

	public $AssignToken;

	public $cardFunction;
    


    public function getCreditCardData()
    {

        return $this->CreditCardData;

    }


    public function setCreditCardData($CreditCardData)
    {

        $this->CreditCardData = $CreditCardData;

        return $this;

    }


    public function getDE_DirectDebitData()
    {

        return $this->DE_DirectDebitData;

    }


    public function setDE_DirectDebitData($DE_DirectDebitData)
    {

        $this->DE_DirectDebitData = $DE_DirectDebitData;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getFunction()
    {

        return $this->Function;

    }


    public function setFunction($Function)
    {

        $this->Function = $Function;

        return $this;

    }


    public function getHostedDataID()
    {

        return $this->HostedDataID;

    }


    public function setHostedDataID($HostedDataID)
    {

        $this->HostedDataID = $HostedDataID;

        return $this;

    }


    public function getDeclineHostedDataDuplicates()
    {

        return $this->DeclineHostedDataDuplicates;

    }


    public function setDeclineHostedDataDuplicates($DeclineHostedDataDuplicates)
    {

        $this->DeclineHostedDataDuplicates = $DeclineHostedDataDuplicates;

        return $this;

    }


    public function getTokenType()
    {

        return $this->TokenType;

    }


    public function setTokenType($TokenType)
    {

        $this->TokenType = $TokenType;

        return $this;

    }


    public function getAssignToken()
    {

        return $this->AssignToken;

    }


    public function setAssignToken($AssignToken)
    {

        $this->AssignToken = $AssignToken;

        return $this;

    }


    public function getCardFunction()
    {

        return $this->cardFunction;

    }


    public function setCardFunction($cardFunction)
    {

        $this->cardFunction = $cardFunction;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
