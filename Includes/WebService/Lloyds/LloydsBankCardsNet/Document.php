<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Document.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Document 
{
	public $Type;

	public $Number;
    


    public function getType()
    {

        return $this->Type;

    }


    public function setType($Type)
    {

        $this->Type = $Type;

        return $this;

    }


    public function getNumber()
    {

        return $this->Number;

    }


    public function setNumber($Number)
    {

        $this->Number = $Number;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
