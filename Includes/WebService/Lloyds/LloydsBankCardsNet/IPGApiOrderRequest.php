<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IPGApiOrderRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IPGApiOrderRequest 
{
	public $Transaction;

	public $SubsequentTransaction;

	public $MerchantDetails;
    


    public function getTransaction()
    {

        return $this->Transaction;

    }


    public function setTransaction($Transaction)
    {

        $this->Transaction = $Transaction;

        return $this;

    }


    public function getSubsequentTransaction()
    {

        return $this->SubsequentTransaction;

    }


    public function setSubsequentTransaction($SubsequentTransaction)
    {

        $this->SubsequentTransaction = $SubsequentTransaction;

        return $this;

    }


    public function getMerchantDetails()
    {

        return $this->MerchantDetails;

    }


    public function setMerchantDetails($MerchantDetails)
    {

        $this->MerchantDetails = $MerchantDetails;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
