<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\AdditionalRequestParameters.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class AdditionalRequestParameters 
{
	public $keyValuePair;

    public function getKeyValuePair()
    {
        return $this->keyValuePair;
    }

    public function setKeyValuePair($keyValuePair)
    {
        $this->keyValuePair = $keyValuePair;
        return $this;
    }

    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
