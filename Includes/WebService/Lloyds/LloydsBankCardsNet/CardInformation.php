<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CardInformation.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CardInformation 
{
	public $Brand;

	public $CardFunction;
    


    public function getBrand()
    {

        return $this->Brand;

    }


    public function setBrand($Brand)
    {

        $this->Brand = $Brand;

        return $this;

    }


    public function getCardFunction()
    {

        return $this->CardFunction;

    }


    public function setCardFunction($CardFunction)
    {

        $this->CardFunction = $CardFunction;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
