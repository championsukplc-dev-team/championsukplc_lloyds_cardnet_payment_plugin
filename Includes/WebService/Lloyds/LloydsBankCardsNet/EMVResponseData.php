<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\EMVResponseData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class EMVResponseData 
{
	/*public $AuthorizationIdentification-DF817E;

	public $AuthorizationParameter-DF817D;

	public $IssuerAuthenticationData-91;

	public $IssuerScriptTemplate1-71;

	public $IssuerScriptTemplate2-72;

	public $IssuerAuthorizationResponseCode-8A;

	public $MessageControlField-DF4F;

	public $ReceiptNumber-DF8161;

	public $SpecialDataBlock-DF817F;

	public $TraceNumber-DF8260;
    


    public function getAuthorizationIdentification-DF817E()
    {

        return $this->AuthorizationIdentification-DF817E;

    }


    public function setAuthorizationIdentification-DF817E($AuthorizationIdentification-DF817E)
    {

        $this->AuthorizationIdentification-DF817E = $AuthorizationIdentification-DF817E;

        return $this;

    }


    public function getAuthorizationParameter-DF817D()
    {

        return $this->AuthorizationParameter-DF817D;

    }


    public function setAuthorizationParameter-DF817D($AuthorizationParameter-DF817D)
    {

        $this->AuthorizationParameter-DF817D = $AuthorizationParameter-DF817D;

        return $this;

    }


    public function getIssuerAuthenticationData-91()
    {

        return $this->IssuerAuthenticationData-91;

    }


    public function setIssuerAuthenticationData-91($IssuerAuthenticationData-91)
    {

        $this->IssuerAuthenticationData-91 = $IssuerAuthenticationData-91;

        return $this;

    }


    public function getIssuerScriptTemplate1-71()
    {

        return $this->IssuerScriptTemplate1-71;

    }


    public function setIssuerScriptTemplate1-71($IssuerScriptTemplate1-71)
    {

        $this->IssuerScriptTemplate1-71 = $IssuerScriptTemplate1-71;

        return $this;

    }


    public function getIssuerScriptTemplate2-72()
    {

        return $this->IssuerScriptTemplate2-72;

    }


    public function setIssuerScriptTemplate2-72($IssuerScriptTemplate2-72)
    {

        $this->IssuerScriptTemplate2-72 = $IssuerScriptTemplate2-72;

        return $this;

    }


    public function getIssuerAuthorizationResponseCode-8A()
    {

        return $this->IssuerAuthorizationResponseCode-8A;

    }


    public function setIssuerAuthorizationResponseCode-8A($IssuerAuthorizationResponseCode-8A)
    {

        $this->IssuerAuthorizationResponseCode-8A = $IssuerAuthorizationResponseCode-8A;

        return $this;

    }


    public function getMessageControlField-DF4F()
    {

        return $this->MessageControlField-DF4F;

    }


    public function setMessageControlField-DF4F($MessageControlField-DF4F)
    {

        $this->MessageControlField-DF4F = $MessageControlField-DF4F;

        return $this;

    }


    public function getReceiptNumber-DF8161()
    {

        return $this->ReceiptNumber-DF8161;

    }


    public function setReceiptNumber-DF8161($ReceiptNumber-DF8161)
    {

        $this->ReceiptNumber-DF8161 = $ReceiptNumber-DF8161;

        return $this;

    }


    public function getSpecialDataBlock-DF817F()
    {

        return $this->SpecialDataBlock-DF817F;

    }


    public function setSpecialDataBlock-DF817F($SpecialDataBlock-DF817F)
    {

        $this->SpecialDataBlock-DF817F = $SpecialDataBlock-DF817F;

        return $this;

    }


    public function getTraceNumber-DF8260()
    {

        return $this->TraceNumber-DF8260;

    }


    public function setTraceNumber-DF8260($TraceNumber-DF8260)
    {

        $this->TraceNumber-DF8260 = $TraceNumber-DF8260;

        return $this;

    }*/




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
