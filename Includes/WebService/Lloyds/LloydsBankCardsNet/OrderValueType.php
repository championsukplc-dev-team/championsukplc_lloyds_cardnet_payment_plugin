<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\OrderValueType.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class OrderValueType 
{
	public $OrderId;

	public $OrderDate;

	public $Basket;

	public $Billing;

	public $MandateReference;

	public $Shipping;

	public $TransactionValues;
    


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getOrderDate()
    {

        return $this->OrderDate;

    }


    public function setOrderDate($OrderDate)
    {

        $this->OrderDate = $OrderDate;

        return $this;

    }


    public function getBasket()
    {

        return $this->Basket;

    }


    public function setBasket($Basket)
    {

        $this->Basket = $Basket;

        return $this;

    }


    public function getBilling()
    {

        return $this->Billing;

    }


    public function setBilling($Billing)
    {

        $this->Billing = $Billing;

        return $this;

    }


    public function getMandateReference()
    {

        return $this->MandateReference;

    }


    public function setMandateReference($MandateReference)
    {

        $this->MandateReference = $MandateReference;

        return $this;

    }


    public function getShipping()
    {

        return $this->Shipping;

    }


    public function setShipping($Shipping)
    {

        $this->Shipping = $Shipping;

        return $this;

    }


    public function getTransactionValues()
    {

        return $this->TransactionValues;

    }


    public function setTransactionValues($TransactionValues)
    {

        $this->TransactionValues = $TransactionValues;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
