<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IVRAuthUserData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IVRAuthUserData 
{
	public $IVRUserDataName;

	public $IVRUserDataValue;

	public $IVRUserDataStatus;

	public $IVRUserDataEncrypted;
    


    public function getIVRUserDataName()
    {

        return $this->IVRUserDataName;

    }


    public function setIVRUserDataName($IVRUserDataName)
    {

        $this->IVRUserDataName = $IVRUserDataName;

        return $this;

    }


    public function getIVRUserDataValue()
    {

        return $this->IVRUserDataValue;

    }


    public function setIVRUserDataValue($IVRUserDataValue)
    {

        $this->IVRUserDataValue = $IVRUserDataValue;

        return $this;

    }


    public function getIVRUserDataStatus()
    {

        return $this->IVRUserDataStatus;

    }


    public function setIVRUserDataStatus($IVRUserDataStatus)
    {

        $this->IVRUserDataStatus = $IVRUserDataStatus;

        return $this;

    }


    public function getIVRUserDataEncrypted()
    {

        return $this->IVRUserDataEncrypted;

    }


    public function setIVRUserDataEncrypted($IVRUserDataEncrypted)
    {

        $this->IVRUserDataEncrypted = $IVRUserDataEncrypted;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
