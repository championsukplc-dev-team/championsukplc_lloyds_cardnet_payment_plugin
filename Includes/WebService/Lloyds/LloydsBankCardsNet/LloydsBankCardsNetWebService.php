<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

use ChukplcCardnet\Includes\Utilities\MainUtility;

/**
 * ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\LloydsBankCardsNetWebService.
 * SDK provided by ipg-online, extended and documented by Champions UK
 * @property string $url https://test.ipg-online.com/ipgapi/services/order.wsdl
 * @property SoapClient $client */

class LloydsBankCardsNetWebService
{
    private static $instance = null;
    private $client = null;
    private $test_service_url = 'https://test.ipg-online.com/ipgapi/services/order.wsdl';
    private $live_service_url = 'https://www.ipg-online.com/ipgapi/services/order.wsdl';
    private $test_service_endpoint = 'https://test.ipg-online.com/ipgapi/services';
    private $live_service_endpoint = 'https://www.ipg-online.com/ipgapi/services';
    private $connectingOption = null;
    public $TransactionResult = null;
    public $ApprovalCode = null;
    public $ErrorMessage = null;


    /**
     * getInstance function
     *
     * @param [type] $certificate_password
     * @param [type] $certificate_url
     * @param [type] $username
     * @param [type] $user_password
     * @param boolean $sandbox
     * @return void
     */
    public static function getInstance($certificate_password, $certificate_url, $username, $user_password, $sandbox = true)
    {
        if (self::$instance == null) {
            self::$instance = new self($certificate_password, $certificate_url, $username, $user_password, $sandbox);
        }

        return self::$instance;
    }


    /**
     * __construct function
     *
     * @param [type] $certificate_password
     * @param [type] $certificate_url
     * @param [type] $username
     * @param [type] $user_password
     * @param [type] $sandbox
     */
    public function __construct($certificate_password, $certificate_url, $username, $user_password, $sandbox)
    {
        $opts = ['http' => [
            'user_agent' => 'PHPSoapClient'
        ]];

        $this->connectingOption = array(
            'location' => ($sandbox === true) ? $this->test_service_endpoint : $this->live_service_endpoint,
            'uri' => ($sandbox === true) ? $this->test_service_url : $this->live_service_url,
            'keep_alive' => true,
            'trace' => true,
            'local_cert' => $certificate_url,
            'passphrase' => $certificate_password,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'login' => $username,
            'password' => $user_password,
            'stream_context' => stream_context_create($opts)
        );

        $this->client = new \SoapClient($this->connectingOption['uri'], $this->connectingOption);
    }


    /**
     * populateBilling function
     *
     * @param [type] $order
     * @return void
     */
    private function populateBilling($order)
    {
        //prepare billing information*/
        $transactionBilling = new Billing();
        $transactionBilling->setAddress1($order->get_billing_address_1() . ' ' . $order->get_billing_address_2());
        $transactionBilling->setCompany($order->get_billing_company());
        $transactionBilling->setCity($order->get_billing_city());
        $transactionBilling->setCountry($order->get_billing_country());
        $transactionBilling->setEmail($order->get_billing_email());
        $transactionBilling->setFirstname($order->get_billing_first_name());
        $transactionBilling->setSurname($order->get_billing_last_name());
        $transactionBilling->setName($order->get_billing_first_name() . ' ' . $order->get_billing_last_name());
        $transactionBilling->setZip($order->get_billing_postcode());
        $transactionBilling->setPhone($order->get_billing_phone());
        $transactionBilling->setMobilePhone($order->get_billing_phone());

        return $transactionBilling;
    }


    /**
     * populateCreditCard function
     *
     * @param [type] $cvv2
     * @param [type] $creditCardNumber
     * @param [type] $exp
     * @return void
     */
    private function populateCreditCard($cvv2, $creditCardNumber, $exp)
    {
        $expDateArr = explode('/', $exp);

        $creditCardData = new CreditCardData();
        $creditCardData->setCardCodeValue($cvv2);
        $creditCardData->setCardNumber($creditCardNumber);
        $creditCardData->setExpMonth(trim($expDateArr[0]));
        $creditCardData->setExpYear(trim($expDateArr[1]));

        return $creditCardData;
    }


    /**
     * populateTransactionDetails function
     *
     * @param [type] $reference
     * @param [type] $transactionTime
     * @param [type] $description
     * @param [type] $ipg_transaction_id
     * @return void
     */
    private function populateTransactionDetails($reference, $transactionTime, $description, $ipg_transaction_id = null)
    {
        $transDetails = new TransactionDetails();
        $transDetails->setMerchantTransactionId($reference);
        $transDetails->setTDate($transactionTime);
        $transDetails->setComments($description . ' Champions Plugin');

        if (!is_null($ipg_transaction_id)) {
            $transDetails->setIpgTransactionId($ipg_transaction_id);
        }

        return $transDetails;
    }


    /**
     * handleResponse function
     * @todo Needs converting to WP http request api.
     * @param [type] $xml_request
     * @return void
     */
    private function handleResponse($xml_request)
    {
        try {
            $response = $this->tryCurl($xml_request);
            MainUtility::addLog($response->data, true);

            if ($response->data['headers']['status'] === 200 || $response->data['headers']['status'] === 202) {
                $resp = $this->extractNormalOrderResponse($response->data['response']);
            } else {
                $resp = $this->extractErrorOrderResponse($response->data['response']);
            }

            if (is_object($resp)) {
                $resp = $this->responseToObject($resp, IPGApiOrderResponse::class);
            }
        } catch (\Exception$ex) {
            MainUtility::addLog($ex, true);
            if (isset($ex->detail) && isset($ex->detail->IPGApiOrderResponse)) {
                $resp = $this->responseToObject($ex->detail->IPGApiOrderResponse, IPGApiOrderResponse::class);
            } else {
                $resp = $ex;
            }
        }

        return $resp;
    }


    /**
     * DoDirectPayment function
     *
     * @param [type] $payment_reference
     * @param [type] $paymentDescription
     * @param [type] $paymentAmount
     * @param [type] $creditCardNumber
     * @param [type] $expDate
     * @param [type] $cvv2
     * @param [type] $order
     * @param [type] $country
     * @param [type] $currency
     * @param [type] $transactionTime
     * @param array $shippingArray
     * @return object
     */
    public function DoDirectPayment($payment_reference, $paymentDescription, $paymentAmount, $creditCardNumber, $expDate, $cvv2, $order, $currency, $transactionTime, $shippingArray = array())
    {
        //prepare the Order request
        $IPGApiOrderRequest = new IPGApiOrderRequest();

        //prepare the transaction object
        $orderTransaction = new Transaction();

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("sale");

        //prepare payment data
        $transactionPaymentData = new Payment();
        $transactionPaymentData->setChargeTotal($paymentAmount);
        $transactionPaymentData->setCurrency($currency);

        //prepare transaction details
        $transDetails = $this->populateTransactionDetails($payment_reference, $transactionTime, $paymentDescription);

        //set transaction parameters
        //$orderTransaction->setBasket($transactionBasket);
        $orderTransaction->setBilling($this->populateBilling($order));
        $orderTransaction->setShipping($transactionShipping);
        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setCreditCardData($this->populateCreditCard($cvv2, $creditCardNumber, $expDate));
        $orderTransaction->setPayment($transactionPaymentData);
        $orderTransaction->setTransactionDetails($transDetails);

        //set the transaction
        $IPGApiOrderRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($IPGApiOrderRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiOrderRequest, 'ipgapi:IPGApiOrderRequest', 'xmlns:v1="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ipgapi="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'v1:');

        MainUtility::addLog('Direct Payment Request/Response Start' . PHP_EOL);

        $logRequest = $orderTransaction;
        $unsetCreditdata = new CreditCardData();
        $logRequest->setCreditCardData($unsetCreditdata);
        $logOrderRequest = new IPGApiOrderRequest();
        $logOrderRequest->setTransaction($logRequest);
        $logXMLRequest = $this->generateValidXmlFromObj($logOrderRequest, 'ipgapi:IPGApiOrderRequest', 'xmlns:v1="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ipgapi="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'v1:');

        MainUtility::addLog($logXMLRequest, true);

        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Direct Payment Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * Do3DSecure function
     *
     * @param [type] $payment_reference
     * @param [type] $paymentDescription
     * @param [type] $paymentAmount
     * @param [type] $creditCardNumber
     * @param [type] $expDate
     * @param [type] $cvv2
     * @param [type] $order
     * @param [type] $currency
     * @param [type] $transactionTime
     * @param [type] $initUrlSecurity
     * @param array $shippingArray
     * @return object
     */
    public function Do3DSecure($payment_reference, $paymentDescription, $paymentAmount, $creditCardNumber, $expDate, $cvv2, $order, $currency, $transactionTime, $initUrlSecurity, array $shippingArray = array())
    {
        //prepare the Order request
        $IPGApiOrder3DRequest = new IPGApiOrderRequest();

        //prepare the transaction object
        $orderTransaction = new Transaction();

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("payerAuth"); //for 3D secure
        //$creditCardTxType->setStoreId($store_id);

        //prepare 3d secure data
        $card3DSecureData = new CreditCard3DSecure();
        $card3DSecureData->setAuthenticateTransaction("true");
        $card3DSecureData->setThreeDSRequestorChallengeIndicator('01');
        $card3DSecureData->setThreeDSRequestorChallengeWindowSize('01');
        $card3DSecureData->setTermUrl($initUrlSecurity);

        //prepare payment data
        $transactionPaymentData = new Payment();
        $transactionPaymentData->setChargeTotal($paymentAmount);
        $transactionPaymentData->setCurrency($currency);
        //prepare transaction details
        $transDetails = $this->populateTransactionDetails($payment_reference, $transactionTime, $paymentDescription);

        //set transaction parameters
        $orderTransaction->setBilling($this->populateBilling($order));
        $orderTransaction->setShipping($transactionShipping);
        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setCreditCardData($this->populateCreditCard($cvv2, $creditCardNumber, $expDate));
        $orderTransaction->setCreditCard3DSecure($card3DSecureData);
        $orderTransaction->setPayment($transactionPaymentData);
        $orderTransaction->setTransactionDetails($transDetails);

        //set the transaction
        $IPGApiOrder3DRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($IPGApiOrder3DRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiOrder3DRequest, 'ns4:IPGApiOrderRequest', 'xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/a1"', 'node', 'ns2:');

        MainUtility::addLog('Do 3d Secure Payment Request/Response Start' . PHP_EOL);

        $logRequest = $orderTransaction;
        $unsetCreditdata = new CreditCardData();
        $logRequest->setCreditCardData($unsetCreditdata);
        $logOrderRequest = new IPGApiOrderRequest();
        $logOrderRequest->setTransaction($logRequest);
        $logXMLRequest = $this->generateValidXmlFromObj($logOrderRequest, 'ipgapi:IPGApiOrderRequest', 'xmlns:v1="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ipgapi="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'v1:');

        MainUtility::addLog($logXMLRequest, true);
        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Do 3d Secure Payment Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * Confirm3DResponseIframe function
     *
     * @param [type] $ipg_transaction_id
     * @param array $shippingArray
     * @param array $billingArray
     * @return object
     */
    public function Confirm3DResponseIframe($ipg_transaction_id, $shippingArray = array(), $billingArray = array())
    {
        $IPGApiOrder3DRequest = new IPGApiOrderRequest();
        $orderTransaction = new Transaction();

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("payerAuth");

        $transactionBilling = new Billing();
        $transactionBilling = $this->getAddressObj($billingArray, $transactionBilling);

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        $creditCard3Dsecure = new CreditCard3DSecure();
        $creditCard3Dsecure->setSecure3DMethodNotificationStatus('NOT_EXPECTED');
        $transDetails = new TransactionDetails();
        $transDetails->setIpgTransactionId($ipg_transaction_id);

        $orderTransaction->setBilling($transactionBilling);
        $orderTransaction->setShipping($transactionShipping);
        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setTransactionDetails($transDetails);
        $orderTransaction->setCreditCard3DSecure($creditCard3Dsecure);

        //set the transaction
        $IPGApiOrder3DRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($IPGApiOrder3DRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiOrder3DRequest, 'ns4:IPGApiOrderRequest', 'xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/a1"', 'node', 'ns2:');
        MainUtility::addLog('Confirm3DResponseIframe Request/Response Start' . PHP_EOL);
        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Confirm3DResponseIframe Request/Response End' . PHP_EOL);
        return $resp;
    }



    /**
     * Get the return object/param with this function.
     * @param $store_id
     * @param $ipg_transaction_id
     * @param $paReq
     * @param $md
     * @param $shippingArray
     * @param $billingArray
     * @return IPGApiOrderResponse
     */
    public function Confirm3DResponse($ipg_transaction_id, $md = '', $paReq = '', $creq = '', $shippingArray = array(), $billingArray = array())
    {
        //prepare the Order request
        $IPGApiOrder3DRequest = new IPGApiOrderRequest();

        //prepare the transaction object
        $orderTransaction = new Transaction();

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("payerAuth");

        //prepare asc response
        $ascResponse = new AcsResponse();
        if ($md) {
            $ascResponse->setMD($md);
        }
        if ($paReq) {
            $ascResponse->setPaRes($paReq);
        }
        if ($creq) {
            $ascResponse->setCRes($creq);
        }

        //prepare secure 3d authentication request
        $secure3DAuthenticationRequest = new Secure3DAuthenticationRequest();
        $secure3DAuthenticationRequest->setAcsResponse($ascResponse);

        //prepare 3D secure request
        $secure3DRequest = new Secure3DRequest();
        $secure3DRequest->setSecure3DAuthenticationRequest($secure3DAuthenticationRequest);

        //prepare 3d secure call parameters
        $creditCard3Dsecure = new CreditCard3DSecure();
        $creditCard3Dsecure->setSecure3DRequest($secure3DRequest);

        //prepare transaction details
        $transDetails = new TransactionDetails();
        $transDetails->setIpgTransactionId($ipg_transaction_id);

        $transactionBilling = new Billing();
        $transactionBilling = $this->getAddressObj($billingArray, $transactionBilling);

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        //set transaction parameters
        $orderTransaction->setBilling($transactionBilling);
        $orderTransaction->setShipping($transactionShipping);

        //set transaction parameters
        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setTransactionDetails($transDetails);
        $orderTransaction->setCreditCard3DSecure($creditCard3Dsecure);

        //set the transaction
        $IPGApiOrder3DRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($IPGApiOrder3DRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiOrder3DRequest, 'ns4:IPGApiOrderRequest', 'xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/a1"', 'node', 'ns2:');
        MainUtility::addLog('Confirm3DResponse Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Confirm3DResponse Request/Response End' . PHP_EOL);

        return $resp;
    }


    /**
     * ConfirmAuthenticationTransaction function
     *
     * @param [type] $ipg_transaction_id
     * @param array $billingArray
     * @param array $shippingArray
     * @return object
     */
    public function ConfirmAuthenticationTransaction($ipg_transaction_id, $billingArray = array(), $shippingArray = array())
    {
        $ipgOrderRequest = new IPGApiOrderRequest();
        $orderTransaction = new Transaction();

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("sale");
        $transactionBilling = new Billing();
        $transactionBilling = $this->getAddressObj($billingArray, $transactionBilling);

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        //prepare transaction details
        $transDetails = new TransactionDetails();
        $transDetails->setIpgTransactionId($ipg_transaction_id);

        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setTransactionDetails($transDetails);
        $orderTransaction->setBilling($transactionBilling);
        $orderTransaction->setShipping($transactionShipping);

        $ipgOrderRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($ipgOrderRequest);
        $xml_request = $this->generateValidXmlFromObj($ipgOrderRequest, 'ns3:IPGApiOrderRequest', 'xmlns="http://ipg-online.com/ipgapi/schemas/a1" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'ns2:');
        MainUtility::addLog('ConfirmAuthenticationTransaction Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        try {
            $response = $this->tryCurl($xml_request);
            MainUtility::addLog($response, true);
            if ($response->data['headers']['status'] === 200) {
                $resp = $this->extractNormalOrderResponse($response->data['response']);
            } else {
                $resp = $this->extractErrorOrderResponse($response->data['response']);
            }

            if (is_object($resp)) {
                $resp = $this->responseToObject($resp, IPGApiOrderResponse::class);
            }
        } catch (\Exception$ex) {
            MainUtility::addLog($ex, true);
            if (isset($ex->detail)) {
                if (isset($ex->detail->IPGApiOrderResponse)) {
                    $resp = $this->responseToObject($ex->detail->IPGApiOrderResponse, IPGApiOrderResponse::class);
                } else {
                    $resp = $ex;
                }
            } else {
                $resp = $ex;
            }
        }
        MainUtility::addLog('ConfirmAuthenticationTransaction Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * Get the return object/param with this function.
     * @param $ipg_transaction_id
     * @param $store_id
     * @return IPGApiOrderResponse
     */
    public function Confirm3DPayment($ipg_transaction_id, $payment_reference, $paymentDescription, $transactionTime, $order, $shippingArray = array())
    {
        //prepare the Order request
        $IPGApiOrder3DRequest = new IPGApiOrderRequest();

        $transactionShipping = new Shipping();
        $transactionShipping = $this->getAddressObj($shippingArray, $transactionShipping, 'shipping');

        //prepare the transaction object
        $orderTransaction = new Transaction();

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("sale");

        //set transaction parameters
        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setTransactionDetails($this->populateTransactionDetails($payment_reference, $transactionTime, $paymentDescription, $ipg_transaction_id));
        $orderTransaction->setBilling($this->populateBilling($order));
        $orderTransaction->setShipping($transactionShipping);

        //set the transaction
        $IPGApiOrder3DRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($IPGApiOrder3DRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiOrder3DRequest, 'ns3:IPGApiOrderRequest', 'xmlns="http://ipg-online.com/ipgapi/schemas/a1" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'ns2:');
        MainUtility::addLog('Confirm3DPayment Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Confirm3DPayment Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * DoRefund function
     *
     * @param [type] $orderId
     * @param [type] $amount
     * @param [type] $currency
     * @return void
     */
    public function DoRefund($orderId, $amount, $currency)
    {
        $ipgOrderRequest = new IPGApiOrderRequest();
        $orderTransaction = new Transaction();

        //prepare credit card transaction type
        $creditCardTxType = new CreditCardTxType();
        $creditCardTxType->setType("return");

        $transDetails = new TransactionDetails();
        $transDetails->setOrderId($orderId);

        $transactionPaymentData = new Payment();
        $transactionPaymentData->setChargeTotal($amount);
        $transactionPaymentData->setCurrency($currency);

        $orderTransaction->setCreditCardTxType($creditCardTxType);
        $orderTransaction->setTransactionDetails($transDetails);
        $orderTransaction->setPayment($transactionPaymentData);

        $ipgOrderRequest->setTransaction($orderTransaction);

        $this->unsetPropertyThatIsNull($ipgOrderRequest);
        $xml_request = $this->generateValidXmlFromObj($ipgOrderRequest, 'ns3:IPGApiOrderRequest', 'xmlns="http://ipg-online.com/ipgapi/schemas/a1" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/v1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/ipgapi"', 'node', 'ns2:');
        MainUtility::addLog('Refund Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        $resp = $this->handleResponse($xml_request);
        MainUtility::addLog('Refund Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * Get the return object/param with this function.
     * @param $order_id
     * @return IPGApiActionResponse
     */
    public function InquireOrder($order_id)
    {
        //prepare the Order action request
        $IPGApiActionRequest = new IPGApiActionRequest();

        //prepare the inquiry object
        $inquiryOrder = new InquiryOrder();

        $inquiryOrder->setOrderId($order_id);

        //prepare action
        $action = new Action();

        //set the inquiry order
        $action->setInquiryOrder($inquiryOrder);

        //set the action
        $IPGApiActionRequest->setAction($action);

        $this->unsetPropertyThatIsNull($IPGApiActionRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiActionRequest, 'ns4:IPGApiActionRequest', 'xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/a1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/v1"', 'node', 'ns2:');
        MainUtility::addLog('InquireOrder Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        try {
            $response = $this->tryCurl($xml_request);
            MainUtility::addLog($response->data, true);
            if ($response->data['headers']['status'] === 200) {
                $resp = $this->extractNormalActionResponse($response->data['response']);
            } else {
                $resp = $this->extractErrorActionResponse($response->data['response']);
            }

            if (is_object($resp)) {
                $resp = $this->responseToObject($resp, IPGApiActionResponse::class);
            }
        } catch (\Exception$ex) {
            MainUtility::addLog($ex, true);
            if (isset($ex->detail) && isset($ex->detail->IPGApiActionResponse)) {
                $resp = $this->responseToObject($ex->detail->IPGApiActionResponse, IPGApiActionResponse::class);
            } else {
                $resp = $ex;
            }
        }
        MainUtility::addLog('InquireOrder Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * Get the return object/param with this function.
     * @param $transaction_id
     * @return IPGApiActionResponse
     */
    public function InquireTransaction($transaction_id)
    {
        //prepare the Order action request
        $IPGApiActionRequest = new IPGApiActionRequest();

        //prepare the inquiry object
        $inquiryTransaction = new InquiryTransaction();

        $inquiryTransaction->setIpgTransactionId($transaction_id);

        //prepare action
        $action = new Action();

        //set the inquiry order
        $action->setInquiryTransaction($inquiryTransaction);

        //set the action
        $IPGApiActionRequest->setAction($action);

        $this->unsetPropertyThatIsNull($IPGApiActionRequest);
        $xml_request = $this->generateValidXmlFromObj($IPGApiActionRequest, 'ns4:IPGApiActionRequest', 'xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi" xmlns:ns2="http://ipg-online.com/ipgapi/schemas/a1" xmlns:ns3="http://ipg-online.com/ipgapi/schemas/v1"', 'node', 'ns2:');
        MainUtility::addLog('InquireTransaction Request/Response Start' . PHP_EOL);
        MainUtility::addLog($xml_request, true);
        try {
            $response = $this->tryCurl($xml_request);
            MainUtility::addLog($response, true);
            if ($response->data['headers']['status'] === 200) {
                $resp = $this->extractNormalActionResponse($response->data['response']);
            } else {
                $resp = $this->extractErrorActionResponse($response->data['response']);
            }

            if (is_object($resp)) {
                $resp = $this->responseToObject($resp, IPGApiActionResponse::class);
            }
        } catch (\Exception$ex) {
            MainUtility::addLog($ex, true);
            if (isset($ex->detail) && isset($ex->detail->IPGApiActionResponse)) {
                $resp = $this->responseToObject($ex->detail->IPGApiActionResponse, IPGApiActionResponse::class);
            } else {
                $resp = $ex;
            }
        }
        MainUtility::addLog('InquireTransaction Request/Response End' . PHP_EOL);
        return $resp;
    }


    /**
     * process3DResponse function
     *
     * @return array
     */
    public function process3DResponse()
    {
        return ['result' => 'success', 'redirect' => site_url('chukplc-cardnet/threed')];
    }


    /**
     * process3DResponseRedirect function
     *
     * @param [type] $lloyds_redirect_url_str
     * @param [type] $termUrl
     * @param [type] $CReq
     * @param [type] $paReq
     * @param [type] $md
     * @return void
     */
    public function process3DResponseRedirect($lloyds_redirect_url_str, $termUrl, $CReq, $paReq, $md)
    {
        $formToSend = "<form id='lloydscardsnet3d-form' name='lloydscardsnet3d-form' action='$lloyds_redirect_url_str' method='post'>";
        $formToSend .= "<input type='hidden' name='TermUrl' value='$termUrl'/>";
        if ($CReq) {
            $formToSend .= "<input type='hidden' name='creq' value='$CReq'/>";
        }
        if ($paReq) {
            $formToSend .= "<input type='hidden' name='PaReq' value='$paReq'/>";
        }
        if ($md) {
            $formToSend .= "<input type='hidden' name='MD' value='$md'/>";
        }

        $formToSend .= "<input type='submit' id='formButton' value='proceed to complete payment' />
        </form><script type='text/javascript'>document.forms['lloydscardsnet3d-form'].submit();</script>";
        MainUtility::addLog('3DS Form Payment Request/Response Start' . PHP_EOL);
        MainUtility::addLog($formToSend, true);
        MainUtility::addLog('3DS Form Payment Request/Response End' . PHP_EOL);
        session_regenerate_id(true);
        session_write_close();
        echo $formToSend;
        die();
    }


    /**
     * createHash function
     *
     * @param [type] $chargetotal
     * @param [type] $currency
     * @param [type] $storename
     * @param [type] $sharedSecret
     * @return void
     */
    public function createHash($chargetotal, $currency, $storename, $sharedSecret)
    {
        $stringToHash = $storename . date("Y:m:d-H:i:s") . $chargetotal . $currency . $sharedSecret;
        return hash('sha256', bin2hex($stringToHash));
    }


    /**
     * extractNormalOrderResponse function
     *
     * @param [type] $response
     * @return void
     */
    public function extractNormalOrderResponse($response)
    {
        $arr_replacement = [
            [['<v1:', '<a1:', '<ipgapi:'], '<'],
            [['</v1:', '</a1:', '</ipgapi:'], '</'],
            [
                [
                    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">',
                    '</SOAP-ENV:Envelope>',
                    '<SOAP-ENV:Header/>',
                    '<SOAP-ENV:Header>',
                    '</SOAP-ENV:Header>',
                    '<SOAP-ENV:Body>',
                    '</SOAP-ENV:Body>',
                ],
                '',
            ],
        ];

        return $this->extractNormalResponse($response, $arr_replacement, 'IPGApiOrderResponse');
    }


    /**
     * extractErrorOrderResponse function
     *
     * @param [type] $response
     * @return void
     */
    public function extractErrorOrderResponse($response)
    {
        $arr_replacement = [
            [['<v1:', '<a1:', '<ipgapi:'], '<'],
            [['</v1:', '</a1:', '</ipgapi:'], '</'],
            [['SOAP-ENV:Fault>'], 'Fault>'],
            [
                [
                    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">',
                    '</SOAP-ENV:Envelope>',
                    '<SOAP-ENV:Header/>',
                    '<SOAP-ENV:Header>',
                    '</SOAP-ENV:Header>',
                    '<SOAP-ENV:Body>',
                    '</SOAP-ENV:Body>',
                ],
                '',
            ],
        ];

        return $this->extractErrorResponse($response, $arr_replacement, 'IPGApiOrderResponse');
    }


    /**
     * extractNormalActionResponse function
     *
     * @param [type] $response
     * @return void
     */
    public function extractNormalActionResponse($response)
    {

        $arr_replacement = [
            [['<v1:', '<a1:', '<ipgapi:'], '<'],
            [['</v1:', '</a1:', '</ipgapi:'], '</'],
            [
                [
                    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">',
                    '</SOAP-ENV:Envelope>',
                    '<SOAP-ENV:Header/>',
                    '<SOAP-ENV:Header>',
                    '</SOAP-ENV:Header>',
                    '<SOAP-ENV:Body>',
                    '</SOAP-ENV:Body>',
                ],
                '',
            ],
        ];

        return $this->extractNormalResponse($response, $arr_replacement, 'IPGApiActionResponse');
    }


    /**
     * extractErrorActionResponse function
     *
     * @param [type] $response
     * @return void
     */
    public function extractErrorActionResponse($response)
    {
        $arr_replacement = [
            [['<v1:', '<a1:', '<ipgapi:'], '<'],
            [['</v1:', '</a1:', '</ipgapi:'], '</'],
            [['SOAP-ENV:Fault>'], 'Fault>'],
            [
                [
                    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">',
                    '</SOAP-ENV:Envelope>',
                    '<SOAP-ENV:Header/>',
                    '<SOAP-ENV:Header>',
                    '</SOAP-ENV:Header>',
                    '<SOAP-ENV:Body>',
                    '</SOAP-ENV:Body>',
                ],
                '',
            ],
        ];

        return $this->extractErrorResponse($response, $arr_replacement, 'IPGApiActionResponse');
    }


    /**
     * extractNormalResponse function
     *
     * @param [type] $xml_str
     * @param [type] $arr_replacement
     * @param [type] $response_tag
     * @return void
     */
    private function extractNormalResponse($xml_str, $arr_replacement, $response_tag)
    {
        $xml_str = (strstr($xml_str, '<?xml')) ? $xml_str : '<?xml version="1.0" encoding="UTF-8"?>' . $xml_str;

        foreach ($arr_replacement as $item) {
            $xml_str = str_replace($item[0], $item[1], $xml_str);
        }

        $xml = new \SimpleXMLElement($xml_str);

        if (strstr($xml_str, $response_tag)) {
            return json_decode(json_encode((array) $xml->xpath('//' . $response_tag)[0]));
        }

        return (string) $xml;
    }

    private function extractErrorResponse($xml_str, $arr_replacement, $response_tag)
    {
        $xml_str = (strstr($xml_str, '<?xml')) ? $xml_str : '<?xml version="1.0" encoding="UTF-8"?>' . $xml_str;

        foreach ($arr_replacement as $item) {
            $xml_str = str_replace($item[0], $item[1], $xml_str);
        }

        $xml = new \SimpleXMLElement($xml_str);
        $detail = $xml->xpath('//Fault')[0]->xpath('//detail')[0];

        if (strstr($xml_str, $response_tag)) {
            $body = $detail->xpath('//' . $response_tag)[0];
            return json_decode(json_encode((array) $body));
        }

        return (string) $detail;
    }


    /**
     * tryCurl function
     * @todo Needs converting to WP http api.
     * @param [type] $xml_request_str
     * @return object
     */
    private function tryCurl($xml_request_str)
    {
        $ch = curl_init();

        if (false === $ch) {
            throw new \Exception('failed to initialize');
        }

        add_action('http_api_curl', [$this, 'setCurlHeaders'], 99, 3);

        $response = MainUtility::httpPost(
            $this->connectingOption['location'],
            ['body' => $xml_request_str],
            $this->connectingOption
        );

        $retReponse = new \stdClass();
        $retReponse->statusCode = $response['headers']['status'];
        $retReponse->data = $response;

        return $retReponse;
    }


    public function setCurlHeaders(&$handle, $params = [], $url = '')
    {
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $params['body']);
        curl_setopt($handle, CURLOPT_USERPWD, $this->connectingOption['login'] . ":" . $this->connectingOption['password']);
        curl_setopt($handle, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
        curl_setopt($handle, CURLOPT_SSLCERT, $this->connectingOption['local_cert']);
        curl_setopt($handle, CURLOPT_SSLCERTPASSWD, $this->connectingOption['passphrase']);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [
            "Content-type: text/xml;charset=\"utf-8\"",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "User-Agent: " . $_SERVER["HTTP_USER_AGENT"]
        ]);

    }

    private function unsetPropertyThatIsNull(&$obj)
    {
        foreach ($obj as $key => $value) {
            if (is_object($value)) {
                $obj->$key = $this->unsetPropertyThatIsNull($value);
            } else if ($value == null) {
                unset($obj->$key);
            }
        }

        return $obj;
    }

    private function responseToObject($sourceObject, $destination)
    {
        if (is_string($destination)) {
            $destination = new $destination();
        }

        $sourceReflection = new \ReflectionObject($sourceObject);
        $destinationReflection = new \ReflectionObject($destination);
        foreach ($sourceReflection->getProperties() as $sourceProperty) {
            $sourceProperty->setAccessible(true);
            $name = $sourceProperty->getName();
            $value = $sourceProperty->getValue($sourceObject);
            if ($destinationReflection->hasProperty($name)) {
                $propDest = $destinationReflection->getProperty($name);
                $propDest->setAccessible(true);
                $propDest->setValue($destination, $value);
            } else {
                $destination->$name = $value;
            }
        }
        return $destination;
    }

    // functions adopted from http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/


    /**
     * generateValidXmlFromObj function
     *
     * @param [type] $obj
     * @param string $node_block
     * @param string $node_block_properties
     * @param string $node_name
     * @param string $extra
     * @return void
     */
    public static function generateValidXmlFromObj($obj, $node_block = 'nodes', $node_block_properties = 'node_block_properties', $node_name = 'node', $extra = 'v1:')
    {
        return self::generateValidXmlFromArray(get_object_vars($obj), $node_block, $node_block_properties, $node_name, $extra);
    }


    /**
     * generateValidXmlFromArray function
     *
     * @param [type] $array
     * @param string $node_block
     * @param string $node_block_properties
     * @param string $node_name
     * @param string $extra
     * @return void
     */
    public static function generateValidXmlFromArray($array, $node_block = 'nodes', $node_block_properties = 'node_block_properties', $node_name = 'node', $extra = 'v1:')
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">';
        $xml .= '<SOAP-ENV:Header/>';
        $xml .= '<SOAP-ENV:Body>';

        $xml .= '<' . $node_block . ' ' . $node_block_properties . '>';
        $xml .= self::generateXmlFromArray($array, $node_name, $extra);
        $xml .= '</' . $node_block . '>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';

        return $xml;
    }


    /**
     * generateXmlFromArray function
     *
     * @param [type] $array
     * @param [type] $node_name
     * @param string $extra
     * @return void
     */
    private static function generateXmlFromArray($array, $node_name, $extra = "")
    {
        $xml = '';

        if (is_array($array) || is_object($array)) {
            foreach ($array as $key => $value) {
                $key = (is_numeric($key)) ? $node_name : $key;
                $xml .= '<' . $extra . $key . '>' . self::generateXmlFromArray($value, $node_name, $extra) . '</' . $extra . $key . '>';
            }

            return $xml;
        }

        return htmlspecialchars($array, ENT_QUOTES);
    }


    /**
     * getAddressObj function
     *
     * @param array $addressArray
     * @param [type] $obj
     * @param string $type
     * @return void
     */
    public function getAddressObj($addressArray, $obj, $type = "billing")
    {
        if (null !== $addressArray['name']) {
            $obj->setName($addressArray['name']);
        }
        if (null !== $addressArray['address1']) {
            $obj->setAddress1($addressArray['address1']);
        }
        if (null !== $addressArray['city']) {
            $obj->setCity($addressArray['city']);
        }
        if (null !== $addressArray['state']) {
            $obj->setState($addressArray['state']);
        }
        if (null !== $addressArray['zip']) {
            $obj->setZip($addressArray['zip']);
        }
        if (null !== $addressArray['country']) {
            $obj->setCountry($addressArray['country']);
        }
        if ($type === 'billing') {
            if (null !== $addressArray['company']) {
                $obj->setCompany($addressArray['company']);
            }
            if (null !== $addressArray['fname']) {
                $obj->setFirstname($addressArray['fname']);
            }
            if (null !== $addressArray['lname']) {
                $obj->setSurname($addressArray['lname']);
            }
            if (null !== $addressArray['phone']) {
                $obj->setPhone($addressArray['phone']);
                $obj->setMobilePhone($addressArray['phone']);
            }
            if (null !== $addressArray['email']) {
                $obj->setEmail($addressArray['email']);
            }
        }
        return $obj;
    }
}
