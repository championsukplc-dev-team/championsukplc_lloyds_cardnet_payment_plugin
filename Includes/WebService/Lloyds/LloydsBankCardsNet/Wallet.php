<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Wallet.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 **/
class Wallet
{
    public $WalletType;
    public $WalletID;
    
    public function getWalletType()
    {
        return $this->WalletType;
    }


    public function setWalletType($WalletType)
    {
        $this->WalletType = $WalletType;
        return $this;
    }


    public function getWalletID()
    {
        return $this->WalletID;
    }


    public function setWalletID($WalletID)
    {
        $this->WalletID = $WalletID;
        return $this;
    }


    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
