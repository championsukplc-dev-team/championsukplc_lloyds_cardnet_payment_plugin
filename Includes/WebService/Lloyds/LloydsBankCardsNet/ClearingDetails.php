<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ClearingDetails.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ClearingDetails 
{
	public $ClearingElement;

	public $BatchTimeStamp;

	public $receiptNumberFrom;

	public $receiptNumberTo;

	public $traceNumber;
    


    public function getClearingElement()
    {

        return $this->ClearingElement;

    }


    public function setClearingElement($ClearingElement)
    {

        $this->ClearingElement = $ClearingElement;

        return $this;

    }


    public function getBatchTimeStamp()
    {

        return $this->BatchTimeStamp;

    }


    public function setBatchTimeStamp($BatchTimeStamp)
    {

        $this->BatchTimeStamp = $BatchTimeStamp;

        return $this;

    }


    public function getReceiptNumberFrom()
    {

        return $this->receiptNumberFrom;

    }


    public function setReceiptNumberFrom($receiptNumberFrom)
    {

        $this->receiptNumberFrom = $receiptNumberFrom;

        return $this;

    }


    public function getReceiptNumberTo()
    {

        return $this->receiptNumberTo;

    }


    public function setReceiptNumberTo($receiptNumberTo)
    {

        $this->receiptNumberTo = $receiptNumberTo;

        return $this;

    }


    public function getTraceNumber()
    {

        return $this->traceNumber;

    }


    public function setTraceNumber($traceNumber)
    {

        $this->traceNumber = $traceNumber;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
