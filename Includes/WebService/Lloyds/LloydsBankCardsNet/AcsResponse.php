<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\AcsResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class AcsResponse 
{
	public $MD;
	public $PaRes;
    public $CRes;

    public function getMD() {
        return $this->MD;
    }


    public function setMD($MD) {
        $this->MD = $MD;
        return $this;
    }


    public function getPaRes() {
        return $this->PaRes;
    }


    public function setPaRes($PaRes) {
        $this->PaRes = $PaRes;
        return $this;
    }

    public function getCRes() {   
        return $this->CRes; 
    }

    public function setCRes($CRes) {   
        $this->CRes = $CRes;    
        return $this;   
    }


    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property) {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value) {
        $this->$property = $value;
    }
}
