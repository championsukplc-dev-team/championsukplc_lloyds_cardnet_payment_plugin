<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\SepaData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class SepaData 
{
	public $IBAN;

	public $Mandate;
    


    public function getIBAN()
    {

        return $this->IBAN;

    }


    public function setIBAN($IBAN)
    {

        $this->IBAN = $IBAN;

        return $this;

    }


    public function getMandate()
    {

        return $this->Mandate;

    }


    public function setMandate($Mandate)
    {

        $this->Mandate = $Mandate;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
