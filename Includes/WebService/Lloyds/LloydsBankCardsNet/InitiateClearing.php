<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\InitiateClearing.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class InitiateClearing 
{
	public $StoreId;

	public $GetLastResult;

	public $Terminal;

	public $traceNumber;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getGetLastResult()
    {

        return $this->GetLastResult;

    }


    public function setGetLastResult($GetLastResult)
    {

        $this->GetLastResult = $GetLastResult;

        return $this;

    }


    public function getTerminal()
    {

        return $this->Terminal;

    }


    public function setTerminal($Terminal)
    {

        $this->Terminal = $Terminal;

        return $this;

    }


    public function getTraceNumber()
    {

        return $this->traceNumber;

    }


    public function setTraceNumber($traceNumber)
    {

        $this->traceNumber = $traceNumber;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
