<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\PaymentFacilitator.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class PaymentFacilitator 
{
	public $ExternalMerchantID;

	public $ID;

	public $SaleOrganizationID;

	public $Name;
    


    public function getExternalMerchantID()
    {

        return $this->ExternalMerchantID;

    }


    public function setExternalMerchantID($ExternalMerchantID)
    {

        $this->ExternalMerchantID = $ExternalMerchantID;

        return $this;

    }


    public function getID()
    {

        return $this->ID;

    }


    public function setID($ID)
    {

        $this->ID = $ID;

        return $this;

    }


    public function getSaleOrganizationID()
    {

        return $this->SaleOrganizationID;

    }


    public function setSaleOrganizationID($SaleOrganizationID)
    {

        $this->SaleOrganizationID = $SaleOrganizationID;

        return $this;

    }


    public function getName()
    {

        return $this->Name;

    }


    public function setName($Name)
    {

        $this->Name = $Name;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
