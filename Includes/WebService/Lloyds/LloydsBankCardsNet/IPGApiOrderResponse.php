<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IPGApiOrderResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IPGApiOrderResponse 
{
	public $DebugInformation;

	public $ApprovalCode;

	public $AVSResponse;

	public $Brand;

	public $Country;

	public $CommercialServiceProvider;

	public $ErrorMessage;

	public $FraudScore;

	public $OrderId;

	public $IpgTransactionId;

	public $PayerSecurityLevel;

	public $PaymentType;

	public $ProcessorApprovalCode;

	public $ProcessorReceiptNumber;

	public $ProcessorCCVResponse;

	public $ProcessorReferenceNumber;

	public $ProcessorResponseCode;

	public $ProcessorResponseMessage;

	public $ProcessorTraceNumber;

	public $ProcessorInstallmentFirstAmount;

	public $ProcessorInstallmentOtherAmount;

	public $ProcessorInstallmentIssuerFeeAmount;

	public $ProcessorInstallmentTaxesAmount;

	public $ProcessorInstallmentInsuranceAmount;

	public $ProcessorInstallmentOtherExpensesAmount;

	public $ProcessorInstallmentTotalAmount;

	public $ProcessorInstallmentRatePerYear;

	public $ProcessorVoucherRemainingAmount;

	public $ProcessorVoucherType;

	public $ReferencedTDate;

	public $TDate;

	public $TDateFormatted;

	public $TerminalID;

	public $TransactionResult;

	public $TransactionTime;

	public $EMVCardPresentResponse;

	public $MandateReference;

	public $HostedData;

	public $Secure3DResponse;

	public $ConvenienceFee;
    


    public function getDebugInformation()
    {

        return $this->DebugInformation;

    }


    public function setDebugInformation($DebugInformation)
    {

        $this->DebugInformation = $DebugInformation;

        return $this;

    }


    public function getApprovalCode()
    {

        return $this->ApprovalCode;

    }


    public function setApprovalCode($ApprovalCode)
    {

        $this->ApprovalCode = $ApprovalCode;

        return $this;

    }


    public function getAVSResponse()
    {

        return $this->AVSResponse;

    }


    public function setAVSResponse($AVSResponse)
    {

        $this->AVSResponse = $AVSResponse;

        return $this;

    }


    public function getBrand()
    {

        return $this->Brand;

    }


    public function setBrand($Brand)
    {

        $this->Brand = $Brand;

        return $this;

    }


    public function getCountry()
    {

        return $this->Country;

    }


    public function setCountry($Country)
    {

        $this->Country = $Country;

        return $this;

    }


    public function getCommercialServiceProvider()
    {

        return $this->CommercialServiceProvider;

    }


    public function setCommercialServiceProvider($CommercialServiceProvider)
    {

        $this->CommercialServiceProvider = $CommercialServiceProvider;

        return $this;

    }


    public function getErrorMessage()
    {

        return $this->ErrorMessage;

    }


    public function setErrorMessage($ErrorMessage)
    {

        $this->ErrorMessage = $ErrorMessage;

        return $this;

    }


    public function getFraudScore()
    {

        return $this->FraudScore;

    }


    public function setFraudScore($FraudScore)
    {

        $this->FraudScore = $FraudScore;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getIpgTransactionId()
    {

        return $this->IpgTransactionId;

    }


    public function setIpgTransactionId($IpgTransactionId)
    {

        $this->IpgTransactionId = $IpgTransactionId;

        return $this;

    }


    public function getPayerSecurityLevel()
    {

        return $this->PayerSecurityLevel;

    }


    public function setPayerSecurityLevel($PayerSecurityLevel)
    {

        $this->PayerSecurityLevel = $PayerSecurityLevel;

        return $this;

    }


    public function getPaymentType()
    {

        return $this->PaymentType;

    }


    public function setPaymentType($PaymentType)
    {

        $this->PaymentType = $PaymentType;

        return $this;

    }


    public function getProcessorApprovalCode()
    {

        return $this->ProcessorApprovalCode;

    }


    public function setProcessorApprovalCode($ProcessorApprovalCode)
    {

        $this->ProcessorApprovalCode = $ProcessorApprovalCode;

        return $this;

    }


    public function getProcessorReceiptNumber()
    {

        return $this->ProcessorReceiptNumber;

    }


    public function setProcessorReceiptNumber($ProcessorReceiptNumber)
    {

        $this->ProcessorReceiptNumber = $ProcessorReceiptNumber;

        return $this;

    }


    public function getProcessorCCVResponse()
    {

        return $this->ProcessorCCVResponse;

    }


    public function setProcessorCCVResponse($ProcessorCCVResponse)
    {

        $this->ProcessorCCVResponse = $ProcessorCCVResponse;

        return $this;

    }


    public function getProcessorReferenceNumber()
    {

        return $this->ProcessorReferenceNumber;

    }


    public function setProcessorReferenceNumber($ProcessorReferenceNumber)
    {

        $this->ProcessorReferenceNumber = $ProcessorReferenceNumber;

        return $this;

    }


    public function getProcessorResponseCode()
    {

        return $this->ProcessorResponseCode;

    }


    public function setProcessorResponseCode($ProcessorResponseCode)
    {

        $this->ProcessorResponseCode = $ProcessorResponseCode;

        return $this;

    }


    public function getProcessorResponseMessage()
    {

        return $this->ProcessorResponseMessage;

    }


    public function setProcessorResponseMessage($ProcessorResponseMessage)
    {

        $this->ProcessorResponseMessage = $ProcessorResponseMessage;

        return $this;

    }


    public function getProcessorTraceNumber()
    {

        return $this->ProcessorTraceNumber;

    }


    public function setProcessorTraceNumber($ProcessorTraceNumber)
    {

        $this->ProcessorTraceNumber = $ProcessorTraceNumber;

        return $this;

    }


    public function getProcessorInstallmentFirstAmount()
    {

        return $this->ProcessorInstallmentFirstAmount;

    }


    public function setProcessorInstallmentFirstAmount($ProcessorInstallmentFirstAmount)
    {

        $this->ProcessorInstallmentFirstAmount = $ProcessorInstallmentFirstAmount;

        return $this;

    }


    public function getProcessorInstallmentOtherAmount()
    {

        return $this->ProcessorInstallmentOtherAmount;

    }


    public function setProcessorInstallmentOtherAmount($ProcessorInstallmentOtherAmount)
    {

        $this->ProcessorInstallmentOtherAmount = $ProcessorInstallmentOtherAmount;

        return $this;

    }


    public function getProcessorInstallmentIssuerFeeAmount()
    {

        return $this->ProcessorInstallmentIssuerFeeAmount;

    }


    public function setProcessorInstallmentIssuerFeeAmount($ProcessorInstallmentIssuerFeeAmount)
    {

        $this->ProcessorInstallmentIssuerFeeAmount = $ProcessorInstallmentIssuerFeeAmount;

        return $this;

    }


    public function getProcessorInstallmentTaxesAmount()
    {

        return $this->ProcessorInstallmentTaxesAmount;

    }


    public function setProcessorInstallmentTaxesAmount($ProcessorInstallmentTaxesAmount)
    {

        $this->ProcessorInstallmentTaxesAmount = $ProcessorInstallmentTaxesAmount;

        return $this;

    }


    public function getProcessorInstallmentInsuranceAmount()
    {

        return $this->ProcessorInstallmentInsuranceAmount;

    }


    public function setProcessorInstallmentInsuranceAmount($ProcessorInstallmentInsuranceAmount)
    {

        $this->ProcessorInstallmentInsuranceAmount = $ProcessorInstallmentInsuranceAmount;

        return $this;

    }


    public function getProcessorInstallmentOtherExpensesAmount()
    {

        return $this->ProcessorInstallmentOtherExpensesAmount;

    }


    public function setProcessorInstallmentOtherExpensesAmount($ProcessorInstallmentOtherExpensesAmount)
    {

        $this->ProcessorInstallmentOtherExpensesAmount = $ProcessorInstallmentOtherExpensesAmount;

        return $this;

    }


    public function getProcessorInstallmentTotalAmount()
    {

        return $this->ProcessorInstallmentTotalAmount;

    }


    public function setProcessorInstallmentTotalAmount($ProcessorInstallmentTotalAmount)
    {

        $this->ProcessorInstallmentTotalAmount = $ProcessorInstallmentTotalAmount;

        return $this;

    }


    public function getProcessorInstallmentRatePerYear()
    {

        return $this->ProcessorInstallmentRatePerYear;

    }


    public function setProcessorInstallmentRatePerYear($ProcessorInstallmentRatePerYear)
    {

        $this->ProcessorInstallmentRatePerYear = $ProcessorInstallmentRatePerYear;

        return $this;

    }


    public function getProcessorVoucherRemainingAmount()
    {

        return $this->ProcessorVoucherRemainingAmount;

    }


    public function setProcessorVoucherRemainingAmount($ProcessorVoucherRemainingAmount)
    {

        $this->ProcessorVoucherRemainingAmount = $ProcessorVoucherRemainingAmount;

        return $this;

    }


    public function getProcessorVoucherType()
    {

        return $this->ProcessorVoucherType;

    }


    public function setProcessorVoucherType($ProcessorVoucherType)
    {

        $this->ProcessorVoucherType = $ProcessorVoucherType;

        return $this;

    }


    public function getReferencedTDate()
    {

        return $this->ReferencedTDate;

    }


    public function setReferencedTDate($ReferencedTDate)
    {

        $this->ReferencedTDate = $ReferencedTDate;

        return $this;

    }


    public function getTDate()
    {

        return $this->TDate;

    }


    public function setTDate($TDate)
    {

        $this->TDate = $TDate;

        return $this;

    }


    public function getTDateFormatted()
    {

        return $this->TDateFormatted;

    }


    public function setTDateFormatted($TDateFormatted)
    {

        $this->TDateFormatted = $TDateFormatted;

        return $this;

    }


    public function getTerminalID()
    {

        return $this->TerminalID;

    }


    public function setTerminalID($TerminalID)
    {

        $this->TerminalID = $TerminalID;

        return $this;

    }


    public function getTransactionResult()
    {

        return $this->TransactionResult;

    }


    public function setTransactionResult($TransactionResult)
    {

        $this->TransactionResult = $TransactionResult;

        return $this;

    }


    public function getTransactionTime()
    {

        return $this->TransactionTime;

    }


    public function setTransactionTime($TransactionTime)
    {

        $this->TransactionTime = $TransactionTime;

        return $this;

    }


    public function getEMVCardPresentResponse()
    {

        return $this->EMVCardPresentResponse;

    }


    public function setEMVCardPresentResponse($EMVCardPresentResponse)
    {

        $this->EMVCardPresentResponse = $EMVCardPresentResponse;

        return $this;

    }


    public function getMandateReference()
    {

        return $this->MandateReference;

    }


    public function setMandateReference($MandateReference)
    {

        $this->MandateReference = $MandateReference;

        return $this;

    }


    public function getHostedData()
    {

        return $this->HostedData;

    }


    public function setHostedData($HostedData)
    {

        $this->HostedData = $HostedData;

        return $this;

    }


    public function getSecure3DResponse()
    {

        return $this->Secure3DResponse;

    }


    public function setSecure3DResponse($Secure3DResponse)
    {

        $this->Secure3DResponse = $Secure3DResponse;

        return $this;

    }


    public function getConvenienceFee()
    {

        return $this->ConvenienceFee;

    }


    public function setConvenienceFee($ConvenienceFee)
    {

        $this->ConvenienceFee = $ConvenienceFee;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
