<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\AirlineDetails.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class AirlineDetails 
{
	public $PassengerName;

	public $TicketNumber;

	public $IssuingCarrier;

	public $CarrierName;

	public $TravelAgencyCodeOrIATACode;

	public $TravelAgencyName;

	public $AirlinePlanNumber;

	public $AirlineInvoiceNumber;

	public $ComputerizedReservationSystem;

	public $Restricted;

	public $TravelRoute;

	public $RelatedTicketNumber;

	public $AncillaryServiceCategory;

	public $TicketPurchase;
    


    public function getPassengerName()
    {

        return $this->PassengerName;

    }


    public function setPassengerName($PassengerName)
    {

        $this->PassengerName = $PassengerName;

        return $this;

    }


    public function getTicketNumber()
    {

        return $this->TicketNumber;

    }


    public function setTicketNumber($TicketNumber)
    {

        $this->TicketNumber = $TicketNumber;

        return $this;

    }


    public function getIssuingCarrier()
    {

        return $this->IssuingCarrier;

    }


    public function setIssuingCarrier($IssuingCarrier)
    {

        $this->IssuingCarrier = $IssuingCarrier;

        return $this;

    }


    public function getCarrierName()
    {

        return $this->CarrierName;

    }


    public function setCarrierName($CarrierName)
    {

        $this->CarrierName = $CarrierName;

        return $this;

    }


    public function getTravelAgencyCodeOrIATACode()
    {

        return $this->TravelAgencyCodeOrIATACode;

    }


    public function setTravelAgencyCodeOrIATACode($TravelAgencyCodeOrIATACode)
    {

        $this->TravelAgencyCodeOrIATACode = $TravelAgencyCodeOrIATACode;

        return $this;

    }


    public function getTravelAgencyName()
    {

        return $this->TravelAgencyName;

    }


    public function setTravelAgencyName($TravelAgencyName)
    {

        $this->TravelAgencyName = $TravelAgencyName;

        return $this;

    }


    public function getAirlinePlanNumber()
    {

        return $this->AirlinePlanNumber;

    }


    public function setAirlinePlanNumber($AirlinePlanNumber)
    {

        $this->AirlinePlanNumber = $AirlinePlanNumber;

        return $this;

    }


    public function getAirlineInvoiceNumber()
    {

        return $this->AirlineInvoiceNumber;

    }


    public function setAirlineInvoiceNumber($AirlineInvoiceNumber)
    {

        $this->AirlineInvoiceNumber = $AirlineInvoiceNumber;

        return $this;

    }


    public function getComputerizedReservationSystem()
    {

        return $this->ComputerizedReservationSystem;

    }


    public function setComputerizedReservationSystem($ComputerizedReservationSystem)
    {

        $this->ComputerizedReservationSystem = $ComputerizedReservationSystem;

        return $this;

    }


    public function getRestricted()
    {

        return $this->Restricted;

    }


    public function setRestricted($Restricted)
    {

        $this->Restricted = $Restricted;

        return $this;

    }


    public function getTravelRoute()
    {

        return $this->TravelRoute;

    }


    public function setTravelRoute($TravelRoute)
    {

        $this->TravelRoute = $TravelRoute;

        return $this;

    }


    public function getRelatedTicketNumber()
    {

        return $this->RelatedTicketNumber;

    }


    public function setRelatedTicketNumber($RelatedTicketNumber)
    {

        $this->RelatedTicketNumber = $RelatedTicketNumber;

        return $this;

    }


    public function getAncillaryServiceCategory()
    {

        return $this->AncillaryServiceCategory;

    }


    public function setAncillaryServiceCategory($AncillaryServiceCategory)
    {

        $this->AncillaryServiceCategory = $AncillaryServiceCategory;

        return $this;

    }


    public function getTicketPurchase()
    {

        return $this->TicketPurchase;

    }


    public function setTicketPurchase($TicketPurchase)
    {

        $this->TicketPurchase = $TicketPurchase;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
