<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IPGApiActionResponse.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IPGApiActionResponse 
{
	public $successfully;

	public $OrderId;

	public $TransactionId;

	public $Error;

	public $ResultInfo;

	public $Basket;

	public $Billing;

	public $Product;

	public $ProductStock;

	public $MandateReference;

	public $Mandate;

	public $Shipping;

	public $TransactionValues;

	public $RecurringPaymentInformation;

	public $DataStorageItem;

	public $ProcessorResponseCode;

	public $ProcessorRequestMessage;

	public $ProcessorResponseMessage;

	public $OrderValues;

	public $CardRateForDCC;

	public $MerchantRateForDynamicPricing;

	public $CardInformation;

	public $ClearingDetails;

	public $paymentUrl;
    


    public function getSuccessfully()
    {

        return $this->successfully;

    }


    public function setSuccessfully($successfully)
    {

        $this->successfully = $successfully;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getTransactionId()
    {

        return $this->TransactionId;

    }


    public function setTransactionId($TransactionId)
    {

        $this->TransactionId = $TransactionId;

        return $this;

    }


    public function getError()
    {

        return $this->Error;

    }


    public function setError($Error)
    {

        $this->Error = $Error;

        return $this;

    }


    public function getResultInfo()
    {

        return $this->ResultInfo;

    }


    public function setResultInfo($ResultInfo)
    {

        $this->ResultInfo = $ResultInfo;

        return $this;

    }


    public function getBasket()
    {

        return $this->Basket;

    }


    public function setBasket($Basket)
    {

        $this->Basket = $Basket;

        return $this;

    }


    public function getBilling()
    {

        return $this->Billing;

    }


    public function setBilling($Billing)
    {

        $this->Billing = $Billing;

        return $this;

    }


    public function getProduct()
    {

        return $this->Product;

    }


    public function setProduct($Product)
    {

        $this->Product = $Product;

        return $this;

    }


    public function getProductStock()
    {

        return $this->ProductStock;

    }


    public function setProductStock($ProductStock)
    {

        $this->ProductStock = $ProductStock;

        return $this;

    }


    public function getMandateReference()
    {

        return $this->MandateReference;

    }


    public function setMandateReference($MandateReference)
    {

        $this->MandateReference = $MandateReference;

        return $this;

    }


    public function getMandate()
    {

        return $this->Mandate;

    }


    public function setMandate($Mandate)
    {

        $this->Mandate = $Mandate;

        return $this;

    }


    public function getShipping()
    {

        return $this->Shipping;

    }


    public function setShipping($Shipping)
    {

        $this->Shipping = $Shipping;

        return $this;

    }


    public function getTransactionValues()
    {

        return $this->TransactionValues;

    }


    public function setTransactionValues($TransactionValues)
    {

        $this->TransactionValues = $TransactionValues;

        return $this;

    }


    public function getRecurringPaymentInformation()
    {

        return $this->RecurringPaymentInformation;

    }


    public function setRecurringPaymentInformation($RecurringPaymentInformation)
    {

        $this->RecurringPaymentInformation = $RecurringPaymentInformation;

        return $this;

    }


    public function getDataStorageItem()
    {

        return $this->DataStorageItem;

    }


    public function setDataStorageItem($DataStorageItem)
    {

        $this->DataStorageItem = $DataStorageItem;

        return $this;

    }


    public function getProcessorResponseCode()
    {

        return $this->ProcessorResponseCode;

    }


    public function setProcessorResponseCode($ProcessorResponseCode)
    {

        $this->ProcessorResponseCode = $ProcessorResponseCode;

        return $this;

    }


    public function getProcessorRequestMessage()
    {

        return $this->ProcessorRequestMessage;

    }


    public function setProcessorRequestMessage($ProcessorRequestMessage)
    {

        $this->ProcessorRequestMessage = $ProcessorRequestMessage;

        return $this;

    }


    public function getProcessorResponseMessage()
    {

        return $this->ProcessorResponseMessage;

    }


    public function setProcessorResponseMessage($ProcessorResponseMessage)
    {

        $this->ProcessorResponseMessage = $ProcessorResponseMessage;

        return $this;

    }


    public function getOrderValues()
    {

        return $this->OrderValues;

    }


    public function setOrderValues($OrderValues)
    {

        $this->OrderValues = $OrderValues;

        return $this;

    }


    public function getCardRateForDCC()
    {

        return $this->CardRateForDCC;

    }


    public function setCardRateForDCC($CardRateForDCC)
    {

        $this->CardRateForDCC = $CardRateForDCC;

        return $this;

    }


    public function getMerchantRateForDynamicPricing()
    {

        return $this->MerchantRateForDynamicPricing;

    }


    public function setMerchantRateForDynamicPricing($MerchantRateForDynamicPricing)
    {

        $this->MerchantRateForDynamicPricing = $MerchantRateForDynamicPricing;

        return $this;

    }


    public function getCardInformation()
    {

        return $this->CardInformation;

    }


    public function setCardInformation($CardInformation)
    {

        $this->CardInformation = $CardInformation;

        return $this;

    }


    public function getClearingDetails()
    {

        return $this->ClearingDetails;

    }


    public function setClearingDetails($ClearingDetails)
    {

        $this->ClearingDetails = $ClearingDetails;

        return $this;

    }


    public function getPaymentUrl()
    {

        return $this->paymentUrl;

    }


    public function setPaymentUrl($paymentUrl)
    {

        $this->paymentUrl = $paymentUrl;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
