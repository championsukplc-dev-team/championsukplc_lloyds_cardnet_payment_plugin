<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CarRental.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CarRental 
{
	public $AgreementNumber;

	public $RenterName;

	public $ReturnCity;

	public $ReturnDate;

	public $PickupDate;

	public $RentalClassID;

	public $ExtraCharges;

	public $NoShowIndicator;
    


    public function getAgreementNumber()
    {

        return $this->AgreementNumber;

    }


    public function setAgreementNumber($AgreementNumber)
    {

        $this->AgreementNumber = $AgreementNumber;

        return $this;

    }


    public function getRenterName()
    {

        return $this->RenterName;

    }


    public function setRenterName($RenterName)
    {

        $this->RenterName = $RenterName;

        return $this;

    }


    public function getReturnCity()
    {

        return $this->ReturnCity;

    }


    public function setReturnCity($ReturnCity)
    {

        $this->ReturnCity = $ReturnCity;

        return $this;

    }


    public function getReturnDate()
    {

        return $this->ReturnDate;

    }


    public function setReturnDate($ReturnDate)
    {

        $this->ReturnDate = $ReturnDate;

        return $this;

    }


    public function getPickupDate()
    {

        return $this->PickupDate;

    }


    public function setPickupDate($PickupDate)
    {

        $this->PickupDate = $PickupDate;

        return $this;

    }


    public function getRentalClassID()
    {

        return $this->RentalClassID;

    }


    public function setRentalClassID($RentalClassID)
    {

        $this->RentalClassID = $RentalClassID;

        return $this;

    }


    public function getExtraCharges()
    {

        return $this->ExtraCharges;

    }


    public function setExtraCharges($ExtraCharges)
    {

        $this->ExtraCharges = $ExtraCharges;

        return $this;

    }


    public function getNoShowIndicator()
    {

        return $this->NoShowIndicator;

    }


    public function setNoShowIndicator($NoShowIndicator)
    {

        $this->NoShowIndicator = $NoShowIndicator;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
