<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\DE_DirectDebitData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class DE_DirectDebitData 
{
	public $BIC;

	public $IBAN;

	public $TrackData;

	public $BankCode;

	public $AccountNumber;

	public $MandateReference;

	public $MandateType;

	public $DateOfMandate;
    


    public function getBIC()
    {

        return $this->BIC;

    }


    public function setBIC($BIC)
    {

        $this->BIC = $BIC;

        return $this;

    }


    public function getIBAN()
    {

        return $this->IBAN;

    }


    public function setIBAN($IBAN)
    {

        $this->IBAN = $IBAN;

        return $this;

    }


    public function getTrackData()
    {

        return $this->TrackData;

    }


    public function setTrackData($TrackData)
    {

        $this->TrackData = $TrackData;

        return $this;

    }


    public function getBankCode()
    {

        return $this->BankCode;

    }


    public function setBankCode($BankCode)
    {

        $this->BankCode = $BankCode;

        return $this;

    }


    public function getAccountNumber()
    {

        return $this->AccountNumber;

    }


    public function setAccountNumber($AccountNumber)
    {

        $this->AccountNumber = $AccountNumber;

        return $this;

    }


    public function getMandateReference()
    {

        return $this->MandateReference;

    }


    public function setMandateReference($MandateReference)
    {

        $this->MandateReference = $MandateReference;

        return $this;

    }


    public function getMandateType()
    {

        return $this->MandateType;

    }


    public function setMandateType($MandateType)
    {

        $this->MandateType = $MandateType;

        return $this;

    }


    public function getDateOfMandate()
    {

        return $this->DateOfMandate;

    }


    public function setDateOfMandate($DateOfMandate)
    {

        $this->DateOfMandate = $DateOfMandate;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
