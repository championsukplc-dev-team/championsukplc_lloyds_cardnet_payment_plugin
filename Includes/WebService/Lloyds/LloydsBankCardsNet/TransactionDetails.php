<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\TransactionDetails.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class TransactionDetails
{
    public $AirlineDetails;

    public $AdditionalRequestParameters;

    public $CarRental;

    public $HotelLodging;

    public $PurchaseCard;

    public $Comments;

    public $InvoiceNumber;

    public $DynamicMerchantName;

    public $PONumber;

    public $OrderId;

    public $MerchantTransactionId;

    public $ReferencedMerchantTransactionId;

    public $IpgTransactionId;

    public $OfflineApprovalType;

    public $Ip;

    public $ReferenceNumber;

    public $SplitShipment;

    public $TDate;

    public $TransactionOrigin;

    public $Terminal;

    public $InquiryRateReference;

    public $Signature;

    public function getAirlineDetails()
    {

        return $this->AirlineDetails;

    }

    public function setAirlineDetails($AirlineDetails)
    {

        $this->AirlineDetails = $AirlineDetails;

        return $this;

    }

    public function getAdditionalRequestParameters()
    {

        return $this->AdditionalRequestParameters;

    }

    public function setAdditionalRequestParameters($AdditionalRequestParameters)
    {

        $this->AdditionalRequestParameters = $AdditionalRequestParameters;

        return $this;

    }

    public function getCarRental()
    {

        return $this->CarRental;

    }

    public function setCarRental($CarRental)
    {

        $this->CarRental = $CarRental;

        return $this;

    }

    public function getHotelLodging()
    {

        return $this->HotelLodging;

    }

    public function setHotelLodging($HotelLodging)
    {

        $this->HotelLodging = $HotelLodging;

        return $this;

    }

    public function getPurchaseCard()
    {

        return $this->PurchaseCard;

    }

    public function setPurchaseCard($PurchaseCard)
    {

        $this->PurchaseCard = $PurchaseCard;

        return $this;

    }

    public function getComments()
    {

        return $this->Comments;

    }

    public function setComments($Comments)
    {

        $this->Comments = $Comments;

        return $this;

    }

    public function getInvoiceNumber()
    {

        return $this->InvoiceNumber;

    }

    public function setInvoiceNumber($InvoiceNumber)
    {

        $this->InvoiceNumber = $InvoiceNumber;

        return $this;

    }

    public function getDynamicMerchantName()
    {

        return $this->DynamicMerchantName;

    }

    public function setDynamicMerchantName($DynamicMerchantName)
    {

        $this->DynamicMerchantName = $DynamicMerchantName;

        return $this;

    }

    public function getPONumber()
    {

        return $this->PONumber;

    }

    public function setPONumber($PONumber)
    {

        $this->PONumber = $PONumber;

        return $this;

    }

    public function getOrderId()
    {

        return $this->OrderId;

    }

    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }

    public function getMerchantTransactionId()
    {

        return $this->MerchantTransactionId;

    }

    public function setMerchantTransactionId($MerchantTransactionId)
    {

        $this->MerchantTransactionId = $MerchantTransactionId;

        return $this;

    }

    public function getReferencedMerchantTransactionId()
    {

        return $this->ReferencedMerchantTransactionId;

    }

    public function setReferencedMerchantTransactionId($ReferencedMerchantTransactionId)
    {

        $this->ReferencedMerchantTransactionId = $ReferencedMerchantTransactionId;

        return $this;

    }

    public function getIpgTransactionId()
    {

        return $this->IpgTransactionId;

    }

    public function setIpgTransactionId($IpgTransactionId)
    {

        $this->IpgTransactionId = $IpgTransactionId;

        return $this;

    }

    public function getOfflineApprovalType()
    {

        return $this->OfflineApprovalType;

    }

    public function setOfflineApprovalType($OfflineApprovalType)
    {

        $this->OfflineApprovalType = $OfflineApprovalType;

        return $this;

    }

    public function getIp()
    {

        return $this->Ip;

    }

    public function setIp($Ip)
    {

        $this->Ip = $Ip;

        return $this;

    }

    public function getReferenceNumber()
    {

        return $this->ReferenceNumber;

    }

    public function setReferenceNumber($ReferenceNumber)
    {

        $this->ReferenceNumber = $ReferenceNumber;

        return $this;

    }

    public function getSplitShipment()
    {

        return $this->SplitShipment;

    }

    public function setSplitShipment($SplitShipment)
    {

        $this->SplitShipment = $SplitShipment;

        return $this;

    }

    public function getTDate()
    {

        return $this->TDate;

    }

    public function setTDate($TDate)
    {

        $this->TDate = $TDate;

        return $this;

    }

    public function getTransactionOrigin()
    {

        return $this->TransactionOrigin;

    }

    public function setTransactionOrigin($TransactionOrigin)
    {

        $this->TransactionOrigin = $TransactionOrigin;

        return $this;

    }

    public function getTerminal()
    {

        return $this->Terminal;

    }

    public function setTerminal($Terminal)
    {

        $this->Terminal = $Terminal;

        return $this;

    }

    public function getInquiryRateReference()
    {

        return $this->InquiryRateReference;

    }

    public function setInquiryRateReference($InquiryRateReference)
    {

        $this->InquiryRateReference = $InquiryRateReference;

        return $this;

    }

    public function getSignature()
    {

        return $this->Signature;

    }

    public function setSignature($Signature)
    {

        $this->Signature = $Signature;

        return $this;

    }

    /**

     * Magic getter to expose protected properties.

     *

     * @param string $property

     * @return mixed

     */

    public function __get($property)
    {

        return $this->$property;

    }

    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }

}
