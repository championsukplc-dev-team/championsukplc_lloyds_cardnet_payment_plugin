<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Item.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Item 
{
	public $ID;

	public $Description;

	public $SubTotal;

	public $ValueAddedTax;

	public $localTax;

	public $DeliveryAmount;

	public $ChargeTotal;

	public $Currency;

	public $Quantity;

	public $Option;
    


    public function getID()
    {

        return $this->ID;

    }


    public function setID($ID)
    {

        $this->ID = $ID;

        return $this;

    }


    public function getDescription()
    {

        return $this->Description;

    }


    public function setDescription($Description)
    {

        $this->Description = $Description;

        return $this;

    }


    public function getSubTotal()
    {

        return $this->SubTotal;

    }


    public function setSubTotal($SubTotal)
    {

        $this->SubTotal = $SubTotal;

        return $this;

    }


    public function getValueAddedTax()
    {

        return $this->ValueAddedTax;

    }


    public function setValueAddedTax($ValueAddedTax)
    {

        $this->ValueAddedTax = $ValueAddedTax;

        return $this;

    }


    public function getLocalTax()
    {

        return $this->localTax;

    }


    public function setLocalTax($localTax)
    {

        $this->localTax = $localTax;

        return $this;

    }


    public function getDeliveryAmount()
    {

        return $this->DeliveryAmount;

    }


    public function setDeliveryAmount($DeliveryAmount)
    {

        $this->DeliveryAmount = $DeliveryAmount;

        return $this;

    }


    public function getChargeTotal()
    {

        return $this->ChargeTotal;

    }


    public function setChargeTotal($ChargeTotal)
    {

        $this->ChargeTotal = $ChargeTotal;

        return $this;

    }


    public function getCurrency()
    {

        return $this->Currency;

    }


    public function setCurrency($Currency)
    {

        $this->Currency = $Currency;

        return $this;

    }


    public function getQuantity()
    {

        return $this->Quantity;

    }


    public function setQuantity($Quantity)
    {

        $this->Quantity = $Quantity;

        return $this;

    }


    public function getOption()
    {

        return $this->Option;

    }


    public function setOption($Option)
    {

        $this->Option = $Option;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
