<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Error.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Error 
{
	public $ErrorMessage;

	public $Code;
    


    public function getErrorMessage()
    {

        return $this->ErrorMessage;

    }


    public function setErrorMessage($ErrorMessage)
    {

        $this->ErrorMessage = $ErrorMessage;

        return $this;

    }


    public function getCode()
    {

        return $this->Code;

    }


    public function setCode($Code)
    {

        $this->Code = $Code;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
