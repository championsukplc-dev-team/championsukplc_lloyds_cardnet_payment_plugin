<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\MerchantDetails.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class MerchantDetails 
{
	public $ExternalMerchantID;

	public $AlternateExternalMerchantID;

	public $EndpointID;

	public $Terminal;

	public $MerchantMasterData;
    


    public function getExternalMerchantID()
    {

        return $this->ExternalMerchantID;

    }


    public function setExternalMerchantID($ExternalMerchantID)
    {

        $this->ExternalMerchantID = $ExternalMerchantID;

        return $this;

    }


    public function getAlternateExternalMerchantID()
    {

        return $this->AlternateExternalMerchantID;

    }


    public function setAlternateExternalMerchantID($AlternateExternalMerchantID)
    {

        $this->AlternateExternalMerchantID = $AlternateExternalMerchantID;

        return $this;

    }


    public function getEndpointID()
    {

        return $this->EndpointID;

    }


    public function setEndpointID($EndpointID)
    {

        $this->EndpointID = $EndpointID;

        return $this;

    }


    public function getTerminal()
    {

        return $this->Terminal;

    }


    public function setTerminal($Terminal)
    {

        $this->Terminal = $Terminal;

        return $this;

    }


    public function getMerchantMasterData()
    {

        return $this->MerchantMasterData;

    }


    public function setMerchantMasterData($MerchantMasterData)
    {

        $this->MerchantMasterData = $MerchantMasterData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
