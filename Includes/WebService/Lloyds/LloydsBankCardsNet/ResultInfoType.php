<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ResultInfoType.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ResultInfoType 
{
	public $MoreResultsAvailable;
    


    public function getMoreResultsAvailable()
    {

        return $this->MoreResultsAvailable;

    }


    public function setMoreResultsAvailable($MoreResultsAvailable)
    {

        $this->MoreResultsAvailable = $MoreResultsAvailable;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
