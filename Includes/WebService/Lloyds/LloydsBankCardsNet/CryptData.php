<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CryptData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CryptData 
{
	public $SRED;

	public $PINBlock;

	public $MAC;
    


    public function getSRED()
    {

        return $this->SRED;

    }


    public function setSRED($SRED)
    {

        $this->SRED = $SRED;

        return $this;

    }


    public function getPINBlock()
    {

        return $this->PINBlock;

    }


    public function setPINBlock($PINBlock)
    {

        $this->PINBlock = $PINBlock;

        return $this;

    }


    public function getMAC()
    {

        return $this->MAC;

    }


    public function setMAC($MAC)
    {

        $this->MAC = $MAC;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
