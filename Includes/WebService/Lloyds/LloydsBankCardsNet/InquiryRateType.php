<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\InquiryRateType.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class InquiryRateType 
{
	public $InquiryRateId;

	public $ForeignCurrencyCode;

	public $ForeignAmount;

	public $ExchangeRate;

	public $DccApplied;

	public $DccOffered;

	public $ExpirationTimestamp;

	public $MarginRatePercentage;

	public $ExchangeRateSourceName;

	public $ExchangeRateSourceTimestamp;
    


    public function getInquiryRateId()
    {

        return $this->InquiryRateId;

    }


    public function setInquiryRateId($InquiryRateId)
    {

        $this->InquiryRateId = $InquiryRateId;

        return $this;

    }


    public function getForeignCurrencyCode()
    {

        return $this->ForeignCurrencyCode;

    }


    public function setForeignCurrencyCode($ForeignCurrencyCode)
    {

        $this->ForeignCurrencyCode = $ForeignCurrencyCode;

        return $this;

    }


    public function getForeignAmount()
    {

        return $this->ForeignAmount;

    }


    public function setForeignAmount($ForeignAmount)
    {

        $this->ForeignAmount = $ForeignAmount;

        return $this;

    }


    public function getExchangeRate()
    {

        return $this->ExchangeRate;

    }


    public function setExchangeRate($ExchangeRate)
    {

        $this->ExchangeRate = $ExchangeRate;

        return $this;

    }


    public function getDccApplied()
    {

        return $this->DccApplied;

    }


    public function setDccApplied($DccApplied)
    {

        $this->DccApplied = $DccApplied;

        return $this;

    }


    public function getDccOffered()
    {

        return $this->DccOffered;

    }


    public function setDccOffered($DccOffered)
    {

        $this->DccOffered = $DccOffered;

        return $this;

    }


    public function getExpirationTimestamp()
    {

        return $this->ExpirationTimestamp;

    }


    public function setExpirationTimestamp($ExpirationTimestamp)
    {

        $this->ExpirationTimestamp = $ExpirationTimestamp;

        return $this;

    }


    public function getMarginRatePercentage()
    {

        return $this->MarginRatePercentage;

    }


    public function setMarginRatePercentage($MarginRatePercentage)
    {

        $this->MarginRatePercentage = $MarginRatePercentage;

        return $this;

    }


    public function getExchangeRateSourceName()
    {

        return $this->ExchangeRateSourceName;

    }


    public function setExchangeRateSourceName($ExchangeRateSourceName)
    {

        $this->ExchangeRateSourceName = $ExchangeRateSourceName;

        return $this;

    }


    public function getExchangeRateSourceTimestamp()
    {

        return $this->ExchangeRateSourceTimestamp;

    }


    public function setExchangeRateSourceTimestamp($ExchangeRateSourceTimestamp)
    {

        $this->ExchangeRateSourceTimestamp = $ExchangeRateSourceTimestamp;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
