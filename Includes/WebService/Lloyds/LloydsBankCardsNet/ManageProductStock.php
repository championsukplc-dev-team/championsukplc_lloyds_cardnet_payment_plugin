<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ManageProductStock.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ManageProductStock 
{
	public $StoreId;

	public $Function;

	public $ProductStock;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getFunction()
    {

        return $this->Function;

    }


    public function setFunction($Function)
    {

        $this->Function = $Function;

        return $this;

    }


    public function getProductStock()
    {

        return $this->ProductStock;

    }


    public function setProductStock($ProductStock)
    {

        $this->ProductStock = $ProductStock;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
