<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IVRVerificationRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IVRVerificationRequest 
{
	public $IVRDeviceIdFormat;

	public $IVRDeviceId;

	public $IVRShoppingChannel;

	public $IVRAuthenticationChannel;
    


    public function getIVRDeviceIdFormat()
    {

        return $this->IVRDeviceIdFormat;

    }


    public function setIVRDeviceIdFormat($IVRDeviceIdFormat)
    {

        $this->IVRDeviceIdFormat = $IVRDeviceIdFormat;

        return $this;

    }


    public function getIVRDeviceId()
    {

        return $this->IVRDeviceId;

    }


    public function setIVRDeviceId($IVRDeviceId)
    {

        $this->IVRDeviceId = $IVRDeviceId;

        return $this;

    }


    public function getIVRShoppingChannel()
    {

        return $this->IVRShoppingChannel;

    }


    public function setIVRShoppingChannel($IVRShoppingChannel)
    {

        $this->IVRShoppingChannel = $IVRShoppingChannel;

        return $this;

    }


    public function getIVRAuthenticationChannel()
    {

        return $this->IVRAuthenticationChannel;

    }


    public function setIVRAuthenticationChannel($IVRAuthenticationChannel)
    {

        $this->IVRAuthenticationChannel = $IVRAuthenticationChannel;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
