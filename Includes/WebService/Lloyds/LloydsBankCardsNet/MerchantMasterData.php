<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\MerchantMasterData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class MerchantMasterData 
{
	public $Mcc;

	public $LegalName;

	public $Dba;

	public $Email;

	public $Url;

	public $DefaultCurrency;

	public $Timezone;

	public $Address;

	public $dccEnrollment;

	public $Document;

	public $MerchantID;
    


    public function getMcc()
    {

        return $this->Mcc;

    }


    public function setMcc($Mcc)
    {

        $this->Mcc = $Mcc;

        return $this;

    }


    public function getLegalName()
    {

        return $this->LegalName;

    }


    public function setLegalName($LegalName)
    {

        $this->LegalName = $LegalName;

        return $this;

    }


    public function getDba()
    {

        return $this->Dba;

    }


    public function setDba($Dba)
    {

        $this->Dba = $Dba;

        return $this;

    }


    public function getEmail()
    {

        return $this->Email;

    }


    public function setEmail($Email)
    {

        $this->Email = $Email;

        return $this;

    }


    public function getUrl()
    {

        return $this->Url;

    }


    public function setUrl($Url)
    {

        $this->Url = $Url;

        return $this;

    }


    public function getDefaultCurrency()
    {

        return $this->DefaultCurrency;

    }


    public function setDefaultCurrency($DefaultCurrency)
    {

        $this->DefaultCurrency = $DefaultCurrency;

        return $this;

    }


    public function getTimezone()
    {

        return $this->Timezone;

    }


    public function setTimezone($Timezone)
    {

        $this->Timezone = $Timezone;

        return $this;

    }


    public function getAddress()
    {

        return $this->Address;

    }


    public function setAddress($Address)
    {

        $this->Address = $Address;

        return $this;

    }


    public function getDccEnrollment()
    {

        return $this->dccEnrollment;

    }


    public function setDccEnrollment($dccEnrollment)
    {

        $this->dccEnrollment = $dccEnrollment;

        return $this;

    }


    public function getDocument()
    {

        return $this->Document;

    }


    public function setDocument($Document)
    {

        $this->Document = $Document;

        return $this;

    }


    public function getMerchantID()
    {

        return $this->MerchantID;

    }


    public function setMerchantID($MerchantID)
    {

        $this->MerchantID = $MerchantID;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
