<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Product.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Product 
{
	public $ProductID;

	public $Description;

	public $OfferStarts;

	public $OfferEnds;

	public $SubTotal;

	public $ValueAddedTax;

	public $localTax;

	public $DeliveryAmount;

	public $CashbackAmount;

	public $ChargeTotal;

	public $Currency;

	public $Choice;
    


    public function getProductID()
    {

        return $this->ProductID;

    }


    public function setProductID($ProductID)
    {

        $this->ProductID = $ProductID;

        return $this;

    }


    public function getDescription()
    {

        return $this->Description;

    }


    public function setDescription($Description)
    {

        $this->Description = $Description;

        return $this;

    }


    public function getOfferStarts()
    {

        return $this->OfferStarts;

    }


    public function setOfferStarts($OfferStarts)
    {

        $this->OfferStarts = $OfferStarts;

        return $this;

    }


    public function getOfferEnds()
    {

        return $this->OfferEnds;

    }


    public function setOfferEnds($OfferEnds)
    {

        $this->OfferEnds = $OfferEnds;

        return $this;

    }


    public function getSubTotal()
    {

        return $this->SubTotal;

    }


    public function setSubTotal($SubTotal)
    {

        $this->SubTotal = $SubTotal;

        return $this;

    }


    public function getValueAddedTax()
    {

        return $this->ValueAddedTax;

    }


    public function setValueAddedTax($ValueAddedTax)
    {

        $this->ValueAddedTax = $ValueAddedTax;

        return $this;

    }


    public function getLocalTax()
    {

        return $this->localTax;

    }


    public function setLocalTax($localTax)
    {

        $this->localTax = $localTax;

        return $this;

    }


    public function getDeliveryAmount()
    {

        return $this->DeliveryAmount;

    }


    public function setDeliveryAmount($DeliveryAmount)
    {

        $this->DeliveryAmount = $DeliveryAmount;

        return $this;

    }


    public function getCashbackAmount()
    {

        return $this->CashbackAmount;

    }


    public function setCashbackAmount($CashbackAmount)
    {

        $this->CashbackAmount = $CashbackAmount;

        return $this;

    }


    public function getChargeTotal()
    {

        return $this->ChargeTotal;

    }


    public function setChargeTotal($ChargeTotal)
    {

        $this->ChargeTotal = $ChargeTotal;

        return $this;

    }


    public function getCurrency()
    {

        return $this->Currency;

    }


    public function setCurrency($Currency)
    {

        $this->Currency = $Currency;

        return $this;

    }


    public function getChoice()
    {

        return $this->Choice;

    }


    public function setChoice($Choice)
    {

        $this->Choice = $Choice;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
