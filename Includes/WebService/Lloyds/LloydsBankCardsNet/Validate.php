<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Validate.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class Validate
{
    public $StoreId;
    public $CreditCardData;
    public $CustomerCardData;
    public $DE_DirectDebitData;
    public $Payment;
    public $TransactionDetails;
    public $Billing;

    public function getStoreId()
    {
        return $this->StoreId;
    }


    public function setStoreId($StoreId)
    {
        $this->StoreId = $StoreId;
        return $this;
    }


    public function getCreditCardData()
    {
        return $this->CreditCardData;
    }


    public function setCreditCardData($CreditCardData)
    {
        $this->CreditCardData = $CreditCardData;
        return $this;
    }


    public function getCustomerCardData()
    {
        return $this->CustomerCardData;
    }


    public function setCustomerCardData($CustomerCardData)
    {
        $this->CustomerCardData = $CustomerCardData;
        return $this;
    }


    public function getDE_DirectDebitData()
    {
        return $this->DE_DirectDebitData;
    }


    public function setDE_DirectDebitData($DE_DirectDebitData)
    {
        $this->DE_DirectDebitData = $DE_DirectDebitData;
        return $this;
    }


    public function getPayment()
    {
        return $this->Payment;
    }


    public function setPayment($Payment)
    {
        $this->Payment = $Payment;
        return $this;
    }


    public function getTransactionDetails()
    {
        return $this->TransactionDetails;
    }


    public function setTransactionDetails($TransactionDetails)
    {
        $this->TransactionDetails = $TransactionDetails;
        return $this;
    }


    public function getBilling()
    {
        return $this->Billing;
    }


    public function setBilling($Billing)
    {
        $this->Billing = $Billing;
        return $this;
    }


    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
