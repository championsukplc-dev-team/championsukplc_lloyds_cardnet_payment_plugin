<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\LineItemData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class LineItemData 
{
	public $CommodityCode;

	public $ProductCode;

	public $Description;

	public $Quantity;

	public $UnitOfMeasure;

	public $UnitPrice;

	public $VATAmountAndRate;

	public $DiscountAmountAndRate;

	public $LineItemTotal;
    


    public function getCommodityCode()
    {

        return $this->CommodityCode;

    }


    public function setCommodityCode($CommodityCode)
    {

        $this->CommodityCode = $CommodityCode;

        return $this;

    }


    public function getProductCode()
    {

        return $this->ProductCode;

    }


    public function setProductCode($ProductCode)
    {

        $this->ProductCode = $ProductCode;

        return $this;

    }


    public function getDescription()
    {

        return $this->Description;

    }


    public function setDescription($Description)
    {

        $this->Description = $Description;

        return $this;

    }


    public function getQuantity()
    {

        return $this->Quantity;

    }


    public function setQuantity($Quantity)
    {

        $this->Quantity = $Quantity;

        return $this;

    }


    public function getUnitOfMeasure()
    {

        return $this->UnitOfMeasure;

    }


    public function setUnitOfMeasure($UnitOfMeasure)
    {

        $this->UnitOfMeasure = $UnitOfMeasure;

        return $this;

    }


    public function getUnitPrice()
    {

        return $this->UnitPrice;

    }


    public function setUnitPrice($UnitPrice)
    {

        $this->UnitPrice = $UnitPrice;

        return $this;

    }


    public function getVATAmountAndRate()
    {

        return $this->VATAmountAndRate;

    }


    public function setVATAmountAndRate($VATAmountAndRate)
    {

        $this->VATAmountAndRate = $VATAmountAndRate;

        return $this;

    }


    public function getDiscountAmountAndRate()
    {

        return $this->DiscountAmountAndRate;

    }


    public function setDiscountAmountAndRate($DiscountAmountAndRate)
    {

        $this->DiscountAmountAndRate = $DiscountAmountAndRate;

        return $this;

    }


    public function getLineItemTotal()
    {

        return $this->LineItemTotal;

    }


    public function setLineItemTotal($LineItemTotal)
    {

        $this->LineItemTotal = $LineItemTotal;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
