<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\RequestMerchantRateForDynamicPricing.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class RequestMerchantRateForDynamicPricing 
{
	public $StoreId;

	public $ForeignCurrency;

	public $BaseAmount;

	public $MerchantDetails;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getForeignCurrency()
    {

        return $this->ForeignCurrency;

    }


    public function setForeignCurrency($ForeignCurrency)
    {

        $this->ForeignCurrency = $ForeignCurrency;

        return $this;

    }


    public function getBaseAmount()
    {

        return $this->BaseAmount;

    }


    public function setBaseAmount($BaseAmount)
    {

        $this->BaseAmount = $BaseAmount;

        return $this;

    }


    public function getMerchantDetails()
    {

        return $this->MerchantDetails;

    }


    public function setMerchantDetails($MerchantDetails)
    {

        $this->MerchantDetails = $MerchantDetails;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
