<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Payment.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Payment 
{
	public $HostedDataID;

	public $HostedDataStoreID;

	public $DeclineHostedDataDuplicates;

	public $SubTotal;

	public $ValueAddedTax;

	public $localTax;

	public $DeliveryAmount;

	public $CashbackAmount;

	public $ChargeTotal;

	public $Currency;

	public $numberOfInstallments;

	public $installmentsInterest;

	public $installmentDelayMonths;

	public $TokenType;

	public $AssignToken;
    


    public function getHostedDataID()
    {

        return $this->HostedDataID;

    }


    public function setHostedDataID($HostedDataID)
    {

        $this->HostedDataID = $HostedDataID;

        return $this;

    }


    public function getHostedDataStoreID()
    {

        return $this->HostedDataStoreID;

    }


    public function setHostedDataStoreID($HostedDataStoreID)
    {

        $this->HostedDataStoreID = $HostedDataStoreID;

        return $this;

    }


    public function getDeclineHostedDataDuplicates()
    {

        return $this->DeclineHostedDataDuplicates;

    }


    public function setDeclineHostedDataDuplicates($DeclineHostedDataDuplicates)
    {

        $this->DeclineHostedDataDuplicates = $DeclineHostedDataDuplicates;

        return $this;

    }


    public function getSubTotal()
    {

        return $this->SubTotal;

    }


    public function setSubTotal($SubTotal)
    {

        $this->SubTotal = $SubTotal;

        return $this;

    }


    public function getValueAddedTax()
    {

        return $this->ValueAddedTax;

    }


    public function setValueAddedTax($ValueAddedTax)
    {

        $this->ValueAddedTax = $ValueAddedTax;

        return $this;

    }


    public function getLocalTax()
    {

        return $this->localTax;

    }


    public function setLocalTax($localTax)
    {

        $this->localTax = $localTax;

        return $this;

    }


    public function getDeliveryAmount()
    {

        return $this->DeliveryAmount;

    }


    public function setDeliveryAmount($DeliveryAmount)
    {

        $this->DeliveryAmount = $DeliveryAmount;

        return $this;

    }


    public function getCashbackAmount()
    {

        return $this->CashbackAmount;

    }


    public function setCashbackAmount($CashbackAmount)
    {

        $this->CashbackAmount = $CashbackAmount;

        return $this;

    }


    public function getChargeTotal()
    {

        return $this->ChargeTotal;

    }


    public function setChargeTotal($ChargeTotal)
    {

        $this->ChargeTotal = $ChargeTotal;

        return $this;

    }


    public function getCurrency()
    {

        return $this->Currency;

    }


    public function setCurrency($Currency)
    {

        $this->Currency = $Currency;

        return $this;

    }


    public function getNumberOfInstallments()
    {

        return $this->numberOfInstallments;

    }


    public function setNumberOfInstallments($numberOfInstallments)
    {

        $this->numberOfInstallments = $numberOfInstallments;

        return $this;

    }


    public function getInstallmentsInterest()
    {

        return $this->installmentsInterest;

    }


    public function setInstallmentsInterest($installmentsInterest)
    {

        $this->installmentsInterest = $installmentsInterest;

        return $this;

    }


    public function getInstallmentDelayMonths()
    {

        return $this->installmentDelayMonths;

    }


    public function setInstallmentDelayMonths($installmentDelayMonths)
    {

        $this->installmentDelayMonths = $installmentDelayMonths;

        return $this;

    }


    public function getTokenType()
    {

        return $this->TokenType;

    }


    public function setTokenType($TokenType)
    {

        $this->TokenType = $TokenType;

        return $this;

    }


    public function getAssignToken()
    {

        return $this->AssignToken;

    }


    public function setAssignToken($AssignToken)
    {

        $this->AssignToken = $AssignToken;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
