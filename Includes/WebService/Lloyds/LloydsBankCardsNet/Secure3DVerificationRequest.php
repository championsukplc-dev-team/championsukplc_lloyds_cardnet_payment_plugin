<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Secure3DVerificationRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Secure3DVerificationRequest 
{
	public $IVRVerificationRequest;
    


    public function getIVRVerificationRequest()
    {

        return $this->IVRVerificationRequest;

    }


    public function setIVRVerificationRequest($IVRVerificationRequest)
    {

        $this->IVRVerificationRequest = $IVRVerificationRequest;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
