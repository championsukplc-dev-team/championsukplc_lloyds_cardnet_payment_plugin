<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IVRAuthData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IVRAuthData 
{
	public $IVRAuthDataName;

	public $IVRAuthDataMaxLen;

	public $IVRAuthDataType;

	public $IVRAuthDataLabel;

	public $IVRAuthDataPrompt;

	public $IVRAuthDataEncrypted;
    


    public function getIVRAuthDataName()
    {

        return $this->IVRAuthDataName;

    }


    public function setIVRAuthDataName($IVRAuthDataName)
    {

        $this->IVRAuthDataName = $IVRAuthDataName;

        return $this;

    }


    public function getIVRAuthDataMaxLen()
    {

        return $this->IVRAuthDataMaxLen;

    }


    public function setIVRAuthDataMaxLen($IVRAuthDataMaxLen)
    {

        $this->IVRAuthDataMaxLen = $IVRAuthDataMaxLen;

        return $this;

    }


    public function getIVRAuthDataType()
    {

        return $this->IVRAuthDataType;

    }


    public function setIVRAuthDataType($IVRAuthDataType)
    {

        $this->IVRAuthDataType = $IVRAuthDataType;

        return $this;

    }


    public function getIVRAuthDataLabel()
    {

        return $this->IVRAuthDataLabel;

    }


    public function setIVRAuthDataLabel($IVRAuthDataLabel)
    {

        $this->IVRAuthDataLabel = $IVRAuthDataLabel;

        return $this;

    }


    public function getIVRAuthDataPrompt()
    {

        return $this->IVRAuthDataPrompt;

    }


    public function setIVRAuthDataPrompt($IVRAuthDataPrompt)
    {

        $this->IVRAuthDataPrompt = $IVRAuthDataPrompt;

        return $this;

    }


    public function getIVRAuthDataEncrypted()
    {

        return $this->IVRAuthDataEncrypted;

    }


    public function setIVRAuthDataEncrypted($IVRAuthDataEncrypted)
    {

        $this->IVRAuthDataEncrypted = $IVRAuthDataEncrypted;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
