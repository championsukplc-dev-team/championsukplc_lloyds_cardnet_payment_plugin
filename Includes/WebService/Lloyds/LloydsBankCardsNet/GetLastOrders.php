<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\GetLastOrders.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class GetLastOrders 
{
	public $StoreId;

	public $Count;

	public $DateFrom;

	public $DateTo;

	public $OrderID;
    


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getCount()
    {

        return $this->Count;

    }


    public function setCount($Count)
    {

        $this->Count = $Count;

        return $this;

    }


    public function getDateFrom()
    {

        return $this->DateFrom;

    }


    public function setDateFrom($DateFrom)
    {

        $this->DateFrom = $DateFrom;

        return $this;

    }


    public function getDateTo()
    {

        return $this->DateTo;

    }


    public function setDateTo($DateTo)
    {

        $this->DateTo = $DateTo;

        return $this;

    }


    public function getOrderID()
    {

        return $this->OrderID;

    }


    public function setOrderID($OrderID)
    {

        $this->OrderID = $OrderID;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
