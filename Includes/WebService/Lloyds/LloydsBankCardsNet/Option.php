<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Option.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Option 
{
	public $Name;

	public $Choice;
    


    public function getName()
    {

        return $this->Name;

    }


    public function setName($Name)
    {

        $this->Name = $Name;

        return $this;

    }


    public function getChoice()
    {

        return $this->Choice;

    }


    public function setChoice($Choice)
    {

        $this->Choice = $Choice;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
