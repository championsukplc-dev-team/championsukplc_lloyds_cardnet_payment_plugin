<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Secure3DRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Secure3DRequest 
{
	public $Secure3DVerificationRequest;

	public $Secure3DAuthenticationRequest;
    


    public function getSecure3DVerificationRequest()
    {

        return $this->Secure3DVerificationRequest;

    }


    public function setSecure3DVerificationRequest($Secure3DVerificationRequest)
    {

        $this->Secure3DVerificationRequest = $Secure3DVerificationRequest;

        return $this;

    }


    public function getSecure3DAuthenticationRequest()
    {

        return $this->Secure3DAuthenticationRequest;

    }


    public function setSecure3DAuthenticationRequest($Secure3DAuthenticationRequest)
    {

        $this->Secure3DAuthenticationRequest = $Secure3DAuthenticationRequest;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
