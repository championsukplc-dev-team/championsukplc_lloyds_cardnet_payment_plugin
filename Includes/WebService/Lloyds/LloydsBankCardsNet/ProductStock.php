<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ProductStock.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ProductStock 
{
	public $ProductID;

	public $Choice;

	public $Quantity;
    


    public function getProductID()
    {

        return $this->ProductID;

    }


    public function setProductID($ProductID)
    {

        $this->ProductID = $ProductID;

        return $this;

    }


    public function getChoice()
    {

        return $this->Choice;

    }


    public function setChoice($Choice)
    {

        $this->Choice = $Choice;

        return $this;

    }


    public function getQuantity()
    {

        return $this->Quantity;

    }


    public function setQuantity($Quantity)
    {

        $this->Quantity = $Quantity;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
