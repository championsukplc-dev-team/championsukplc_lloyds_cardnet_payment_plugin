<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Basket.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Basket 
{
	public $ProductStock;

	public $Item;
    


    public function getProductStock()
    {

        return $this->ProductStock;

    }


    public function setProductStock($ProductStock)
    {

        $this->ProductStock = $ProductStock;

        return $this;

    }


    public function getItem()
    {

        return $this->Item;

    }


    public function setItem($Item)
    {

        $this->Item = $Item;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
