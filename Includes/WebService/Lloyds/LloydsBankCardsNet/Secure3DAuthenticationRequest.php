<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Secure3DAuthenticationRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */
class Secure3DAuthenticationRequest
{
    public $AcsResponse;

    public $IVRAuthenticationRequest;

    public function getAcsResponse()
    {
        return $this->AcsResponse;
    }

    public function setAcsResponse($AcsResponse)
    {
        $this->AcsResponse = $AcsResponse;

        return $this;
    }

    public function getIVRAuthenticationRequest()
    {
        return $this->IVRAuthenticationRequest;
    }

    public function setIVRAuthenticationRequest($IVRAuthenticationRequest)
    {
        $this->IVRAuthenticationRequest = $IVRAuthenticationRequest;

        return $this;
    }

    /**

     * Magic getter to expose protected properties.

     *

     * @param string $property

     * @return mixed

     */

    public function __get($property)
    {
        return $this->$property;
    }

    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
