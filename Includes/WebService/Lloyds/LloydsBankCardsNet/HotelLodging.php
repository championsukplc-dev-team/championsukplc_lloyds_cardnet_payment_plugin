<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\HotelLodging.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class HotelLodging 
{
	public $ArrivalDate;

	public $DepartureDate;

	public $FolioNumber;

	public $ExtraCharges;

	public $NoShowIndicator;
    


    public function getArrivalDate()
    {

        return $this->ArrivalDate;

    }


    public function setArrivalDate($ArrivalDate)
    {

        $this->ArrivalDate = $ArrivalDate;

        return $this;

    }


    public function getDepartureDate()
    {

        return $this->DepartureDate;

    }


    public function setDepartureDate($DepartureDate)
    {

        $this->DepartureDate = $DepartureDate;

        return $this;

    }


    public function getFolioNumber()
    {

        return $this->FolioNumber;

    }


    public function setFolioNumber($FolioNumber)
    {

        $this->FolioNumber = $FolioNumber;

        return $this;

    }


    public function getExtraCharges()
    {

        return $this->ExtraCharges;

    }


    public function setExtraCharges($ExtraCharges)
    {

        $this->ExtraCharges = $ExtraCharges;

        return $this;

    }


    public function getNoShowIndicator()
    {

        return $this->NoShowIndicator;

    }


    public function setNoShowIndicator($NoShowIndicator)
    {

        $this->NoShowIndicator = $NoShowIndicator;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
