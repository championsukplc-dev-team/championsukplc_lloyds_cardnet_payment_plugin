<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Mandate.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Mandate 
{
	public $Reference;

	public $Type;

	public $Date;

	public $Url;
    


    public function getReference()
    {

        return $this->Reference;

    }


    public function setReference($Reference)
    {

        $this->Reference = $Reference;

        return $this;

    }


    public function getType()
    {

        return $this->Type;

    }


    public function setType($Type)
    {

        $this->Type = $Type;

        return $this;

    }


    public function getDate()
    {

        return $this->Date;

    }


    public function setDate($Date)
    {

        $this->Date = $Date;

        return $this;

    }


    public function getUrl()
    {

        return $this->Url;

    }


    public function setUrl($Url)
    {

        $this->Url = $Url;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
