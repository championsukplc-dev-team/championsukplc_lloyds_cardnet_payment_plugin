<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\keyValuePair.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class keyValuePair 
{
	public $key;

	public $value;
    


    public function getKey()
    {

        return $this->key;

    }


    public function setKey($key)
    {

        $this->key = $key;

        return $this;

    }


    public function getValue()
    {

        return $this->value;

    }


    public function setValue($value)
    {

        $this->value = $value;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
