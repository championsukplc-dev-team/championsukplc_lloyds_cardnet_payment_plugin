<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\Billing.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class Billing 
{
	public $CustomerID;

	public $Name;

	public $Firstname;

	public $Surname;

	public $Company;

	public $Address1;

	public $StreetName;

	public $HouseNumber;

	public $HouseExtension;

	public $Address2;

	public $City;

	public $State;

	public $Zip;

	public $Country;

	public $Phone;

	public $Fax;

	public $Email;

	public $PersonalNumber;

	public $BirthDate;

	public $Gender;

	public $MobilePhone;

	public $Addrnum;
    


    public function getCustomerID()
    {

        return $this->CustomerID;

    }


    public function setCustomerID($CustomerID)
    {

        $this->CustomerID = $CustomerID;

        return $this;

    }


    public function getName()
    {

        return $this->Name;

    }


    public function setName($Name)
    {

        $this->Name = $Name;

        return $this;

    }


    public function getFirstname()
    {

        return $this->Firstname;

    }


    public function setFirstname($Firstname)
    {

        $this->Firstname = $Firstname;

        return $this;

    }


    public function getSurname()
    {

        return $this->Surname;

    }


    public function setSurname($Surname)
    {

        $this->Surname = $Surname;

        return $this;

    }


    public function getCompany()
    {

        return $this->Company;

    }


    public function setCompany($Company)
    {

        $this->Company = $Company;

        return $this;

    }


    public function getAddress1()
    {

        return $this->Address1;

    }


    public function setAddress1($Address1)
    {

        $this->Address1 = $Address1;

        return $this;

    }


    public function getStreetName()
    {

        return $this->StreetName;

    }


    public function setStreetName($StreetName)
    {

        $this->StreetName = $StreetName;

        return $this;

    }


    public function getHouseNumber()
    {

        return $this->HouseNumber;

    }


    public function setHouseNumber($HouseNumber)
    {

        $this->HouseNumber = $HouseNumber;

        return $this;

    }


    public function getHouseExtension()
    {

        return $this->HouseExtension;

    }


    public function setHouseExtension($HouseExtension)
    {

        $this->HouseExtension = $HouseExtension;

        return $this;

    }


    public function getAddress2()
    {

        return $this->Address2;

    }


    public function setAddress2($Address2)
    {

        $this->Address2 = $Address2;

        return $this;

    }


    public function getCity()
    {

        return $this->City;

    }


    public function setCity($City)
    {

        $this->City = $City;

        return $this;

    }


    public function getState()
    {

        return $this->State;

    }


    public function setState($State)
    {

        $this->State = $State;

        return $this;

    }


    public function getZip()
    {

        return $this->Zip;

    }


    public function setZip($Zip)
    {

        $this->Zip = $Zip;

        return $this;

    }


    public function getCountry()
    {

        return $this->Country;

    }


    public function setCountry($Country)
    {

        $this->Country = $Country;

        return $this;

    }


    public function getPhone()
    {

        return $this->Phone;

    }


    public function setPhone($Phone)
    {

        $this->Phone = $Phone;

        return $this;

    }


    public function getFax()
    {

        return $this->Fax;

    }


    public function setFax($Fax)
    {

        $this->Fax = $Fax;

        return $this;

    }


    public function getEmail()
    {

        return $this->Email;

    }


    public function setEmail($Email)
    {

        $this->Email = $Email;

        return $this;

    }


    public function getPersonalNumber()
    {

        return $this->PersonalNumber;

    }


    public function setPersonalNumber($PersonalNumber)
    {

        $this->PersonalNumber = $PersonalNumber;

        return $this;

    }


    public function getBirthDate()
    {

        return $this->BirthDate;

    }


    public function setBirthDate($BirthDate)
    {

        $this->BirthDate = $BirthDate;

        return $this;

    }


    public function getGender()
    {

        return $this->Gender;

    }


    public function setGender($Gender)
    {

        $this->Gender = $Gender;

        return $this;

    }


    public function getMobilePhone()
    {

        return $this->MobilePhone;

    }


    public function setMobilePhone($MobilePhone)
    {

        $this->MobilePhone = $MobilePhone;

        return $this;

    }


    public function getAddrnum()
    {

        return $this->Addrnum;

    }


    public function setAddrnum($Addrnum)
    {

        $this->Addrnum = $Addrnum;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
