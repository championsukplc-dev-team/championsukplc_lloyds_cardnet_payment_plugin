<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\InquiryRateReference.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class InquiryRateReference 
{
	public $InquiryRateId;

	public $DccApplied;
    


    public function getInquiryRateId()
    {

        return $this->InquiryRateId;

    }


    public function setInquiryRateId($InquiryRateId)
    {

        $this->InquiryRateId = $InquiryRateId;

        return $this;

    }


    public function getDccApplied()
    {

        return $this->DccApplied;

    }


    public function setDccApplied($DccApplied)
    {

        $this->DccApplied = $DccApplied;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
