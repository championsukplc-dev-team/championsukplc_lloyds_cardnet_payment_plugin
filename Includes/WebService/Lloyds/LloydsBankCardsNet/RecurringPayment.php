<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\RecurringPayment.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class RecurringPayment 
{
	public $Function;

	public $OrderId;

	public $StoreId;

	public $Comments;

	public $InvoiceNumber;

	public $DynamicMerchantName;

	public $PONumber;

	public $RecurringPaymentInformation;

	public $CreditCardData;

	public $DE_DirectDebitData;

	public $cardFunction;

	public $MandateReference;

	public $ReferencedOrderId;

	public $Payment;

	public $Basket;

	public $Billing;

	public $CreditCard3DSecure;

	public $Shipping;

	public $Ip;

	public $TransactionOrigin;

	public $Wallet;

	public $MerchantDetails;
    


    public function getFunction()
    {

        return $this->Function;

    }


    public function setFunction($Function)
    {

        $this->Function = $Function;

        return $this;

    }


    public function getOrderId()
    {

        return $this->OrderId;

    }


    public function setOrderId($OrderId)
    {

        $this->OrderId = $OrderId;

        return $this;

    }


    public function getStoreId()
    {

        return $this->StoreId;

    }


    public function setStoreId($StoreId)
    {

        $this->StoreId = $StoreId;

        return $this;

    }


    public function getComments()
    {

        return $this->Comments;

    }


    public function setComments($Comments)
    {

        $this->Comments = $Comments;

        return $this;

    }


    public function getInvoiceNumber()
    {

        return $this->InvoiceNumber;

    }


    public function setInvoiceNumber($InvoiceNumber)
    {

        $this->InvoiceNumber = $InvoiceNumber;

        return $this;

    }


    public function getDynamicMerchantName()
    {

        return $this->DynamicMerchantName;

    }


    public function setDynamicMerchantName($DynamicMerchantName)
    {

        $this->DynamicMerchantName = $DynamicMerchantName;

        return $this;

    }


    public function getPONumber()
    {

        return $this->PONumber;

    }


    public function setPONumber($PONumber)
    {

        $this->PONumber = $PONumber;

        return $this;

    }


    public function getRecurringPaymentInformation()
    {

        return $this->RecurringPaymentInformation;

    }


    public function setRecurringPaymentInformation($RecurringPaymentInformation)
    {

        $this->RecurringPaymentInformation = $RecurringPaymentInformation;

        return $this;

    }


    public function getCreditCardData()
    {

        return $this->CreditCardData;

    }


    public function setCreditCardData($CreditCardData)
    {

        $this->CreditCardData = $CreditCardData;

        return $this;

    }


    public function getDE_DirectDebitData()
    {

        return $this->DE_DirectDebitData;

    }


    public function setDE_DirectDebitData($DE_DirectDebitData)
    {

        $this->DE_DirectDebitData = $DE_DirectDebitData;

        return $this;

    }


    public function getCardFunction()
    {

        return $this->cardFunction;

    }


    public function setCardFunction($cardFunction)
    {

        $this->cardFunction = $cardFunction;

        return $this;

    }


    public function getMandateReference()
    {

        return $this->MandateReference;

    }


    public function setMandateReference($MandateReference)
    {

        $this->MandateReference = $MandateReference;

        return $this;

    }


    public function getReferencedOrderId()
    {

        return $this->ReferencedOrderId;

    }


    public function setReferencedOrderId($ReferencedOrderId)
    {

        $this->ReferencedOrderId = $ReferencedOrderId;

        return $this;

    }


    public function getPayment()
    {

        return $this->Payment;

    }


    public function setPayment($Payment)
    {

        $this->Payment = $Payment;

        return $this;

    }


    public function getBasket()
    {

        return $this->Basket;

    }


    public function setBasket($Basket)
    {

        $this->Basket = $Basket;

        return $this;

    }


    public function getBilling()
    {

        return $this->Billing;

    }


    public function setBilling($Billing)
    {

        $this->Billing = $Billing;

        return $this;

    }


    public function getCreditCard3DSecure()
    {

        return $this->CreditCard3DSecure;

    }


    public function setCreditCard3DSecure($CreditCard3DSecure)
    {

        $this->CreditCard3DSecure = $CreditCard3DSecure;

        return $this;

    }


    public function getShipping()
    {

        return $this->Shipping;

    }


    public function setShipping($Shipping)
    {

        $this->Shipping = $Shipping;

        return $this;

    }


    public function getIp()
    {

        return $this->Ip;

    }


    public function setIp($Ip)
    {

        $this->Ip = $Ip;

        return $this;

    }


    public function getTransactionOrigin()
    {

        return $this->TransactionOrigin;

    }


    public function setTransactionOrigin($TransactionOrigin)
    {

        $this->TransactionOrigin = $TransactionOrigin;

        return $this;

    }


    public function getWallet()
    {

        return $this->Wallet;

    }


    public function setWallet($Wallet)
    {

        $this->Wallet = $Wallet;

        return $this;

    }


    public function getMerchantDetails()
    {

        return $this->MerchantDetails;

    }


    public function setMerchantDetails($MerchantDetails)
    {

        $this->MerchantDetails = $MerchantDetails;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
