<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CustomerCardData.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CustomerCardData 
{
	public $CardNumber;

	public $ExpMonth;

	public $ExpYear;

	public $TrackData;
    


    public function getCardNumber()
    {

        return $this->CardNumber;

    }


    public function setCardNumber($CardNumber)
    {

        $this->CardNumber = $CardNumber;

        return $this;

    }


    public function getExpMonth()
    {

        return $this->ExpMonth;

    }


    public function setExpMonth($ExpMonth)
    {

        $this->ExpMonth = $ExpMonth;

        return $this;

    }


    public function getExpYear()
    {

        return $this->ExpYear;

    }


    public function setExpYear($ExpYear)
    {

        $this->ExpYear = $ExpYear;

        return $this;

    }


    public function getTrackData()
    {

        return $this->TrackData;

    }


    public function setTrackData($TrackData)
    {

        $this->TrackData = $TrackData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
