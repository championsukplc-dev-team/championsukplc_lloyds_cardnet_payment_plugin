<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\ProductChoice.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class ProductChoice 
{
	public $OptionName;

	public $Name;
    


    public function getOptionName()
    {

        return $this->OptionName;

    }


    public function setOptionName($OptionName)
    {

        $this->OptionName = $OptionName;

        return $this;

    }


    public function getName()
    {

        return $this->Name;

    }


    public function setName($Name)
    {

        $this->Name = $Name;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
