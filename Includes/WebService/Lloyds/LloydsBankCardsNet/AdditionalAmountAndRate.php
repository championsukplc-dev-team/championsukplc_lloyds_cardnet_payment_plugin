<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\AdditionalAmountAndRate.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class AdditionalAmountAndRate 
{
	public $Amount;
	public $Rate;
    
    public function getAmount()
    {
        return $this->Amount;
    }


    public function setAmount($Amount)
    {
        $this->Amount = $Amount;

        return $this;
    }


    public function getRate()
    {
        return $this->Rate;
    }


    public function setRate($Rate)
    {
        $this->Rate = $Rate;
        return $this;
    }




    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        return $this->$property;
    }


    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}
