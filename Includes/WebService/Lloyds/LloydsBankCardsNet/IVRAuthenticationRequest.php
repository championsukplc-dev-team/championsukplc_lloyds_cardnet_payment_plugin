<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\IVRAuthenticationRequest.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class IVRAuthenticationRequest 
{
	public $IVRAuthUserData;
    


    public function getIVRAuthUserData()
    {

        return $this->IVRAuthUserData;

    }


    public function setIVRAuthUserData($IVRAuthUserData)
    {

        $this->IVRAuthUserData = $IVRAuthUserData;

        return $this;

    }




    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
