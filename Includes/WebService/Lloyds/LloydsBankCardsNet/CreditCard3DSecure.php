<?php

namespace ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet;

/**
 *ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\CreditCard3DSecure.
 *
 * @Class object from https://test.ipg-online.com/ipgapi/services/order.wsdl
 * */ 
class CreditCard3DSecure 
{
	public $VerificationResponse;

	public $PayerAuthenticationResponse;

	public $DSRPECI;

	public $AuthenticationValue;

	public $XID;

	public $AuthenticateTransaction;

	public $Secure3DRequest;
    
    public $TermUrl;

    public $ThreeDSMethodNotificationURL;

    public $ThreeDSRequestorChallengeIndicator;

    public $ThreeDSRequestorChallengeWindowSize;

    public $Secure3DMethodCompleted;

    public $Secure3DMethodNotificationStatus;

    public function getVerificationResponse()
    {

        return $this->VerificationResponse;

    }


    public function setVerificationResponse($VerificationResponse)
    {

        $this->VerificationResponse = $VerificationResponse;

        return $this;

    }


    public function getPayerAuthenticationResponse()
    {

        return $this->PayerAuthenticationResponse;

    }


    public function setPayerAuthenticationResponse($PayerAuthenticationResponse)
    {

        $this->PayerAuthenticationResponse = $PayerAuthenticationResponse;

        return $this;

    }


    public function getDSRPECI()
    {

        return $this->DSRPECI;

    }


    public function setDSRPECI($DSRPECI)
    {

        $this->DSRPECI = $DSRPECI;

        return $this;

    }


    public function getAuthenticationValue()
    {

        return $this->AuthenticationValue;

    }


    public function setAuthenticationValue($AuthenticationValue)
    {

        $this->AuthenticationValue = $AuthenticationValue;

        return $this;

    }


    public function getXID()
    {

        return $this->XID;

    }


    public function setXID($XID)
    {

        $this->XID = $XID;

        return $this;

    }


    public function getAuthenticateTransaction()
    {

        return $this->AuthenticateTransaction;

    }


    public function setAuthenticateTransaction($AuthenticateTransaction)
    {

        $this->AuthenticateTransaction = $AuthenticateTransaction;

        return $this;

    }


    public function getSecure3DRequest()
    {

        return $this->Secure3DRequest;

    }


    public function setSecure3DRequest($Secure3DRequest)
    {

        $this->Secure3DRequest = $Secure3DRequest;

        return $this;

    }

    public function getTermUrl()
    {
        return $this->TermUrl;
    }

    public function setTermUrl($TermUrl)
    {
        $this->TermUrl = $TermUrl;
        return $this;
    }

    public function getThreeDSMethodNotificationURL()
    {
        return $this->ThreeDSMethodNotificationURL;
    }

    public function setThreeDSMethodNotificationURL($ThreeDSMethodNotificationURL)
    {
        $this->ThreeDSMethodNotificationURL = $ThreeDSMethodNotificationURL;
        return $this;
    }

    public function getThreeDSRequestorChallengeIndicator()
    {
        return $this->ThreeDSRequestorChallengeIndicator;
    }

    public function setThreeDSRequestorChallengeIndicator($ThreeDSRequestorChallengeIndicator)
    {
        $this->ThreeDSRequestorChallengeIndicator = $ThreeDSRequestorChallengeIndicator;
        return $this;
    }

    public function getThreeDSRequestorChallengeWindowSize()
    {
        return $this->ThreeDSRequestorChallengeWindowSize;
    }

    public function setThreeDSRequestorChallengeWindowSize($ThreeDSRequestorChallengeWindowSize)
    {
        $this->ThreeDSRequestorChallengeWindowSize = $ThreeDSRequestorChallengeWindowSize;
        return $this;
    }

    public function getSecure3DMethodCompleted()
    {
        return $this->Secure3DMethodCompleted;
    }

    public function setSecure3DMethodCompleted($Secure3DMethodCompleted)
    {
        $this->Secure3DMethodCompleted = $Secure3DMethodCompleted;
        return $this;
    }

    public function getSecure3DMethodNotificationStatus()
    {
        return $this->Secure3DMethodNotificationStatus;
    }

    public function setSecure3DMethodNotificationStatus($Secure3DMethodNotificationStatus)
    {
         $this->Secure3DMethodNotificationStatus = $Secure3DMethodNotificationStatus;    
         return $this;
    }
    
    /**

    * Magic getter to expose protected properties.

    *

    * @param string $property

    * @return mixed

    */

    public function __get($property)
    {

        return $this->$property;

    }


    /**

     * Magic setter to save protected properties.

     *

     * @param string $property

     * @param mixed $value

     */

    public function __set($property, $value)
    {

        $this->$property = $value;

    }


}
