<?php

/**

 ***
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\IndexController.
 */

namespace ChukplcCardnet\Includes;

use ChukplcCardnet\Includes\Utilities\Session\PaymentSession;
use ChukplcCardnet\Includes\Utilities\Services\PaymentsService;
use ChukplcCardnet\Includes\Utilities\LloydsCardnetRedirectUtility;
use ChukplcCardnet\Includes\WC_Cardnet_Gateway;

class CardnetRedirect
{
    public static function completePayment(&$transaction, array $settings)
    {
        $order = wc_get_order($transaction->service_id);

        $lloydsCardnetReDirectObj = new LloydsCardnetRedirectUtility(strtolower($settings['sel_mode']));
        $lloydsCardnetReDirectObj->setSharedSecret($settings['shared_secret']);

        if ($transaction->payment_cardnet_gone_to_gateway) {
            Utilities\MainUtility::addLog('Redirect Complete Start' . PHP_EOL);
            Utilities\MainUtility::addLog($_REQUEST, true);

            $verifyResponse = $lloydsCardnetReDirectObj->verifyResponse(
                get_query_var('response_hash'),
                $transaction->lloyds_transaction_time,
                get_query_var('approval_code'),
                number_format($transaction->service_amount, 2, '.', ''),
                Utilities\MainUtility::getIsoCurrencyCodeFromCurrencyCode($transaction->service_currency),
                $settings[strtolower($settings['sel_mode']) . '-store-id']
            );

            Utilities\MainUtility::addLog('Verify redirect response - ' . $verifyResponse . PHP_EOL, true);

            $transaction->lloyds_response_transaction_status = get_query_var('status') . '-' . get_query_var('approval_code');
            $transaction->lloyds_response_transaction_message = get_query_var('approval_code') . '|' . get_query_var('status') . '|' . get_query_var('response_code_3dsecure') . '|' . get_query_var('processor_response_code') . '|' . get_query_var('oid');
            $transaction->lloyds_response_payer_id = get_query_var('refnumber');

            //Check status of response
            if ($verifyResponse && (str_starts_with(get_query_var('approval_code'), 'Y:') || strstr(strtolower(get_query_var('approval_code')), 'waiting 3dsecure')) && get_query_var('status') === 'APPROVED') {
                $transaction->lloyds_pay_response = "success"; // Success
            } elseif (strstr(strtolower(get_query_var('approval_code')), 'cancel')) {
                $transaction->lloyds_pay_response = "cancelled"; // cancel
            } else {
                $transaction->lloyds_pay_response = "failed"; // failed
            }

            Utilities\MainUtility::addLog('Redirect Complete End' . PHP_EOL);

            if ($transaction->lloyds_pay_response == "success") {
                return true;
            }

            return false;
        }

        $lloydsCardnetReDirectObj->setFailureUrl(site_url(WC_Cardnet_Gateway::URL_BASE . WC_Cardnet_Gateway::URL_PROCESS))
            ->setSuccessUrl(site_url(WC_Cardnet_Gateway::URL_BASE . WC_Cardnet_Gateway::URL_PROCESS))
            ->setCheckoutOption('combinedpage')
            ->setTransactionNotificationUrl(site_url(WC_Cardnet_Gateway::URL_BASE . WC_Cardnet_Gateway::URL_NOTIFICATION . urlencode($transaction->service_id) . '/' . urlencode($transaction->lloyds_transaction_time)))
            ->setBillingAddress1($order->get_billing_address_1())
            ->setBillingAddress2($order->get_billing_address_2())
            ->setBillingCity($order->get_billing_city())
            ->setBillingCountry($order->get_billing_country())
            ->setBillingState($order->get_billing_state())
            ->setBillingPhone($order->get_billing_phone())
            ->setBillingPostCode($order->get_billing_postcode())
            ->setBillingFirstnames($order->get_billing_first_name())
            ->setBillingSurname($order->get_billing_last_name())
            ->setCompanyName($order->get_billing_company())
            ->setCustomerEmail($order->get_billing_email())
            ->setDeliveryAddress1($order->get_shipping_address_1())
            ->setDeliveryAddress2($order->get_shipping_address_2())
            ->setDeliveryCity($order->get_shipping_city())
            ->setDeliveryCountry($order->get_shipping_country())
            ->setDeliveryState($order->get_shipping_state())
            ->setDeliveryPostCode($order->get_shipping_postcode())
            ->setDeliveryFirstnames($order->get_shipping_first_name())
            ->setDeliverySurname($order->get_shipping_last_name())
            ->setOrderId($transaction->service_id);

        $transaction->payment_cardnet_gone_to_gateway = true;

        PaymentSession::getInstance()->setData($transaction);
        $lloydsCardnetReDirectObj->processRedirectPaymentForm(
            $settings[strtolower($settings['sel_mode']) . '-store-id'],
            $transaction->lloyds_transaction_time,
            number_format($transaction->service_amount, 2, '.', ''),
            Utilities\MainUtility::getIsoCurrencyCodeFromCurrencyCode($transaction->service_currency),
            $transaction->remote_reference
        );
    }


    public static function processPayment(&$transaction, $redirectUrl)
    {
        $order = wc_get_order($transaction->service_id);
        $transaction->lloyds_transaction_time = date('Y:m:d-H:i:s');

        //lets create the payment records in our database.
        if (!isset($transaction->payment_reference) || !$transaction->payment_reference) {
            $transaction->payment_reference = PaymentsService::getInstance()->createPayment($transaction, Utilities\MainUtility::getUserIp(), $transaction->service_account, 'LloydsCards: ttime:' . $transaction->lloyds_transaction_time);
        }

        if ($transaction->payment_reference) {
            $transaction->payment_gone_to_gateway = true;
            $transaction->payment_cardnet_gone_to_gateway = false;
        }

        $transaction->remote_reference = strtoupper('CH' . $transaction->service_id . '-' . time() . '-' . rand(0, 1000000));
        $transaction->order = $order;

        PaymentSession::getInstance()->setData($transaction);

        return ['result' => ((strstr("Payment Success", 'Success')) ? 'success' : 'failure'), 'redirect' => $redirectUrl];
    }
}
