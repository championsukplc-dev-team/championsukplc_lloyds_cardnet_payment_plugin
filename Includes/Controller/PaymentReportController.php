<?php

/**
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\AdminController.
 */

namespace ChukplcCardnet\Includes\Controller;

use ChukplcCardnet\Includes\Services\PaymentsService;

/**
 * PaymentReportController
 */
class PaymentReportController
{
    /**
     * listPayments
     * @var $pagenum
     */
    public function listPayments()
    {
        global $wpdb;
        $result = $wpdb->get_results("select * from " . PaymentsService::getInstance()->getEntityClass() . " ORDER BY id DESC");
        $rows = [];
        if ($result) {
            $counter = 0;
            foreach ($result as $content) {
                $rows[] =
                '<tr>
					<td>' . ++$counter . '</td>
					<td>' . $content->service_id . '</td>
					<td>' . $content->amount . '</td>
					<td>' . $content->status . '</td>
					<td>' . $content->payment_reference . '</td>
					<td>' . $content->remote_reference . '</td>
					<td>' . $content->remote_status_or_code . '</td>
					<td>' . $content->modified_date . '</td>
			     </tr>';
            }
        } else {
            $rows[] = '<tr><td colspan="12">No Transactions Presently in the database.</td></tr>';
        }

        $html = '<div class="wrap">
            <h3> Lloyds Cardnet Payment Report </h3>
            <table class="table table-hover dataTable table-striped width-full" id="dataSearchTable">
            <thead>
                <tr>
					<th>#</th>
					<th>Order ID</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Payment Reference</th>
					<th>Transaction Reference</th>
					<th>Transaction Status</th>
					<th>Payment Date</th>
				</tr>
            </thead>
            <tbody>';
        $html .= implode('', $rows);
        $html .= '</tbody></table></div>';

        echo wp_kses($html, [
            'tr' => [],
            'td' => ['colspan'],
            'div' => ['class'],
            'h3' => [],
            'th' => [],
            'thead' => [],
            'tbody' => [],
            'table' => ['class', 'id'],
        ]);
    }
}
