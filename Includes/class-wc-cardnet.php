<?php

/**

 ***
 * @file
 * Contains \ChukplcCardnet\Includes\Admin\Controller\IndexController.
 */

namespace ChukplcCardnet\Includes;

use ChukplcCardnet\ChukplcCardnet;
use ChukplcCardnet\Includes\Utilities\LloydsCardnetRedirectUtility;
use ChukplcCardnet\Includes\Utilities\Services\PaymentsService;
use ChukplcCardnet\Includes\Utilities\Session\PaymentSession;
use ChukplcCardnet\Includes\WebService\Lloyds\LloydsBankCardsNet\LloydsBankCardsNetWebService;

/**
 * WC_Cardnet_Gateway
 */
class WC_Cardnet_Gateway extends \WC_Payment_Gateway
{
    const ENVIRONMENT_LIVE = 'LIVE';
    const ENVIRONMENT_TEST = 'TEST';

    const PAYMENT_REDIRECT = 'redirect';
    const PAYMENT_DIRECT = 'direct';

    const URL_BASE = 'chukplc-cardnet/';
    const URL_PROCESS = 'process/';
    const URL_PROCESS_3DS = 'process3ds/';
    const URL_CONFIRMATION = 'confirmation/';
    const URL_NOTIFICATION = 'notification/';

    protected static $transactionObj;
    protected static $redirectUrl;
    protected static $block_config;
    protected static $input_validator;
    public static $payment_method_id = 8;
    public $input_required = true;
    public $char_min_lenth = 1;
    public $char_max_lenth = 900000;
    protected $options = [];

    protected $lloyds_pay_response = null;
    protected $transaction_reference = null;
    protected $payment_status = null;
    protected $instructions = null;

    public function __construct()
    {
        global $wp;
        $this->id = 'chukplccardnet';
        $this->has_fields = true;
        $this->method_title = __('Pay By Card', $this->id);
        $this->method_description = __('Lloyds Pay By Card', $this->id);
        $this->title = __("Lloyds Cardnet", $this->id);

        self::$redirectUrl = get_site_url(null, self::URL_BASE . self::URL_PROCESS, __FILE__);

        $this->init_form_fields();
        $this->init_settings();

        if (isset($this->settings['sel_type']) && $this->settings['sel_type'] === self::PAYMENT_DIRECT) {
            $this->supports = ['default_credit_card_form'];
        }

        // Turn these settings into variables we can use
        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }

        add_action('woocommerce_admin_field_file', [$this, 'add_admin_field_file']);
        add_action('woocommerce_update_options_payment_gateways_' . strtolower($this->id), [$this, 'process_admin_options']);
    }

    /**
     * Initialize Gateway Settings Form Fields
     */
    public function init_form_fields()
    {
        if (!isset($_REQUEST['set'])) {
            $this->form_fields = array(
                'enabled' => [
                    'title' => __('Enable/Disable', strtolower($this->id)),
                    'type' => 'checkbox',
                    'label' => __('Enable Lloyds Cardnet Payment', strtolower($this->id)),
                    'default' => 'yes',
                ],

                'title' => [
                    'title' => __('Title', $this->id),
                    'type' => 'text',
                    'description' => __('This controls the title for the payment method the customer sees during checkout.', $this->id),
                    'default' => __('Pay By Card', $this->id),
                    'desc_tip' => true,
                ],

                'description' => [
                    'title' => __('Description', $this->id),
                    'type' => 'textarea',
                    'description' => __('Payment method description that the customer will see on your checkout.', $this->id),
                    'default' => __('<p>Pay with Lloyds Cardnet</p>', $this->id),
                    'desc_tip' => true,
                ],

                'sel_mode' => [
                    'title' => __('Environment', $this->id),
                    'type' => 'select',
                    'options' => [
                        self::ENVIRONMENT_LIVE => __(self::ENVIRONMENT_LIVE, $this->id),
                        self::ENVIRONMENT_TEST => __(self::ENVIRONMENT_TEST, $this->id),
                    ],
                    'default' => self::ENVIRONMENT_TEST,
                ],

                'sel_type' => [
                    'title' => __('Payment Type', $this->id),
                    'type' => 'select',
                    'options' => [
                        self::PAYMENT_REDIRECT => ucfirst(self::PAYMENT_REDIRECT),
                        self::PAYMENT_DIRECT => ucfirst(self::PAYMENT_DIRECT),
                    ],
                    'default' => 'redirect',
                ],

                'shared_secret' => [
                    'title' => __('Shared Secret', $this->id),
                    'type' => 'password',
                ],

                'redirect_payment_text' => [
                    'title' => __('Redirect Payment Button Text', $this->id),
                    'type' => 'text',
                    'default' => 'Pay Now',
                ],

                'direct_payment_text' => [
                    'title' => __('Direct Payment Button Text', $this->id),
                    'type' => 'text',
                    'default' => 'Pay Now',
                ],

                'redirect_logo' => [
                    'title' => __('Redirect Payment Logo', $this->id),
                    'type' => 'text',
                    'default' => plugins_url('../images/credit-card-logo.png', __FILE__),
                ],

                'direct_logo' => [
                    'title' => __('Direct Payment Logo', $this->id),
                    'type' => 'text',
                    'default' => plugins_url('../images/credit-card-logo.png', __FILE__),
                ],
            );
        }

        $set = sanitize_text_field($_REQUEST['set'] ?? '');
        if ($set == strtolower(self::ENVIRONMENT_LIVE) || $set == strtolower(self::ENVIRONMENT_TEST)) {
            $envOptions = [
                $set . '-store-id' => [
                    'title' => __(ucfirst($set) . ' Store Id', $this->id),
                    'type' => 'text',
                ],

                $set . '-user-id' => [
                    'title' => __(ucfirst($set) . ' User Id', $this->id),
                    'type' => 'text',
                ],

                $set . '-user-password' => [
                    'title' => __(ucfirst($set) . ' User Password', $this->id),
                    'type' => 'password',
                ],

                $set . '-certificate' => [
                    'title' => __(ucfirst($set) . ' Certificate', $this->id),
                    'type' => 'textarea',
                    'tip' => 'This is the .pem file sent with your credentials',
                ],

                $set . '-certificate-password' => [
                    'title' => __(ucfirst($set) . ' Certificate Password', $this->id),
                    'type' => 'password',
                ],
                $set . '-certificate' => [
                    'title' => __(ucfirst($set) . ' Certificate', $this->id),
                    'type' => 'file',
                    'default' => '0',
                ],
            ];

            $this->form_fields = array_merge($this->form_fields, $envOptions);
        }
    }

    /**
     * process_payment function
     *
     * @param [type] $order_id
     * @return void
     */
    public function process_payment($order_id)
    {
        //Create a new payment session
        PaymentSession::getInstance()->clearSessionData();
        PaymentSession::getInstance()->renewSession();

        self::$transactionObj = PaymentSession::getInstance()->getData();

        $order = wc_get_order($order_id);

        self::$transactionObj->service_account = new \stdClass();
        self::$transactionObj->service_account->email = $order->get_billing_email();
        self::$transactionObj->service_account->last_name = $order->get_billing_last_name();
        self::$transactionObj->service_account->first_name = $order->get_billing_first_name();
        self::$transactionObj->service_account->id = get_current_user_id();
        self::$transactionObj->service_id = $order_id;
        self::$transactionObj->service_type = 2; //commerce
        self::$transactionObj->service_return_url = $this->get_return_url($order);
        self::$transactionObj->service_cancel_url = get_site_url() . '/checkout/';

        if (self::$payment_method_id == 8) {
            $paymentMethod = new \stdClass();
            $paymentMethod->id = self::$payment_method_id;
            $order_info = [];

            // Iterating through each WC_Order_Item_Product objects
            foreach ($order->get_items() as $item_values) {
                $item_data = $item_values->get_data();
                $order_info[] = $item_data['name'];
            }

            self::$transactionObj->service_currency = get_option('woocommerce_currency');
            self::$transactionObj->service_payment_method = $paymentMethod;
            self::$transactionObj->service_amount = $order->get_total();
            self::$transactionObj->service_information = implode(',', $order_info);

            if ($this->settings['sel_type'] === self::PAYMENT_DIRECT) {
                return CardnetDirect::ProcessPayment(self::$transactionObj, $this, self::$redirectUrl);
            }

            return CardnetRedirect::ProcessPayment(self::$transactionObj, self::$redirectUrl);
        }

        ChukplcCardnet::add_championsukplc_notification(("Invalid Payment Request"), '1');
        PaymentSession::getInstance()->setData(self::$transactionObj);
        return ['result' => 'failure', 'redirect' => self::$redirectUrl];
    }


    public function notificationUrlCompletion()
    {
        if (empty(get_query_var('orderid'))) {
            return false;
        }

        $order = wc_get_order(get_query_var('orderid'));

        Utilities\MainUtility::addLog('Transaction Report Start' . PHP_EOL);
        Utilities\MainUtility::addLog($_REQUEST, true);

        $lloydsCardnetReDirectObj = new LloydsCardnetRedirectUtility(strtolower($this->settings['sel_mode']));
        $lloydsCardnetReDirectObj->setSharedSecret($this->settings['shared_secret']);
        $verifyResponse = $lloydsCardnetReDirectObj->verifyResponseNotification(get_query_var('notification_hash'), get_query_var('transactionTime'), get_query_var('approval_code'), number_format(get_query_var('chargetotal'), 2, '.', ''), get_query_var('currency', null), $this->settings[$this->settings['sel_mode'] . '-store-id']);

        Utilities\MainUtility::addLog('verifyResponseNotification - ' . $verifyResponse . PHP_EOL, true);

        if ($verifyResponse && (str_starts_with(get_query_var('approval_code'), 'Y:') || strstr(strtolower(get_query_var('approval_code')), 'waiting 3dsecure')) && get_query_var('status') === 'APPROVED') {
            // Success
            $this->lloyds_pay_response = "success";
        } elseif (strstr(strtolower(get_query_var('approval_code')), 'cancel')) {
            // cancel
            $this->lloyds_pay_response = "cancelled";
        } else {
            // failed
            $this->lloyds_pay_response = "failed";
        }

        $new_data = [
            'remote_message' => PaymentsService::getInstance()->getPaymentByReference(get_query_var('merchantTransactionId'))->remote_message . ' ' . get_query_var('approval_code') . '|' . get_query_var('status') . '|' . get_query_var('response_code_3dsecure') . '|' . get_query_var('processor_response_code') . '|' . get_query_var('oid'),
            'remote_status_or_code' => get_query_var('status') . '-' . get_query_var('approval_code'),
            'remote_payer_id' => get_query_var('refnumber'),
        ];

        if ($this->lloyds_pay_response == "success") {
            $this->transaction_reference = get_query_var('merchantTransactionId');

            //success processing
            $processing = true;
            $new_data['status'] = "SUCCESSFUL";
            $new_data['remote_reference'] = $this->transaction_reference;
        } else {
            //failed processing
            $processing = false;
            $new_data['status'] = "FAILED";
        }

        PaymentsService::getInstance()->update(
            $new_data,
            PaymentsService::getInstance()->getPaymentByReference(get_query_var('merchantTransactionId'))->id
        );

        if ($processing) {
            $order_info = [];
            foreach ($order->get_items() as $item_values) {
                $item_data = $item_values->get_data();
                $order_info[] = $item_data['name'];
            }

            $this->payment_status = ($processing === true) ? "pending" : "success";
            $order->update_status('processing', __('Payment Successful', $this->id));
            wc_reduce_stock_levels(get_query_var('orderid'));
            WC()->cart->empty_cart();
            global $woocommerce;
            $woocommerce->cart->empty_cart();
        } else {
            $this->payment_status = "failed";
            $order->update_status('failed', __('Payment Failed', $this->id));
        }

        Utilities\MainUtility::addLog('Transaction Report End' . PHP_EOL);
    }



    /**
     * buildAddress function
     *
     * @param string $address - billing | shipping
     * @return array
     */
    public function buildAddress($address = 'billing')
    {
        $order = wc_get_order(self::$transactionObj->service_id);

        $orderAddress = $order->get_address($address);
        $extras = [
            'name' => $orderAddress['first_name'] . ' ' . $orderAddress['last_name'] ?? '',
            'fname' => $orderAddress['first_name'] ?? '',
            'lname' => $orderAddress['last_name'] ?? '',
            'address1' => $orderAddress['address_1'] . ' ' . $orderAddress['address_2'] ?? '',
            'zip' => $orderAddress['postcode'] ?? '',
        ];

        return array_merge($orderAddress, $extras);
    }

    public function confirmresponse()
    {
        if (get_query_var('return') === 'true') {
            $paymentService = PaymentsService::getInstance();
            $paymentSessionObj = PaymentSession::getInstance();
            self::$transactionObj = $paymentSessionObj->getData();

            $ipg_transaction_id = self::$transactionObj->lloyds_response_transaction_ref;
            $lloydsCardsWebServObj = $this->getLloydsCardWebServObject();

            $returnRespObj = $lloydsCardsWebServObj->Confirm3DResponseIframe($ipg_transaction_id, $this->buildAddress('shipping'), $this->buildAddress());

            self::$transactionObj->lloyds_response_transaction_ref = $returnRespObj->getIpgTransactionId();
            self::$transactionObj->lloyds_response_transaction_message = $returnRespObj->ErrorMessage;
            self::$transactionObj->lloyds_response_transaction_status = $returnRespObj->getTransactionResult() . '-' . $returnRespObj->getApprovalCode();
            self::$transactionObj->lloyds_response_payer_id = '';
            self::$transactionObj->lloyds_pay_response = "failed";

            if (is_object($returnRespObj)) {
                if ($returnRespObj->getTransactionResult() === 'APPROVED' && isset($returnRespObj->getSecure3DResponse()->ResponseCode3dSecure) && !str_starts_with($returnObj->getApprovalCode(), '?:waiting')) {
                    self::$transactionObj->lloyds3d_secure = true;
                    if (in_array($returnRespObj->getSecure3DResponse()->ResponseCode3dSecure, [1, 2, 4])) {
                        //success
                        $returnObj = $lloydsCardsWebServObj->ConfirmAuthenticationTransaction($ipg_transaction_id, $this->buildAddress(), $this->buildAddress('shipping'));
                        self::$transactionObj->lloyds_response_transaction_ref = $returnObj->getIpgTransactionId();
                        self::$transactionObj->lloyds_response_transaction_message = $returnObj->ErrorMessage;
                        self::$transactionObj->lloyds_response_transaction_status = $returnRespObj->getTransactionResult() . '-' . $returnRespObj->getApprovalCode();
                        if (is_object($returnObj) && $returnObj->getTransactionResult() === 'APPROVED' && str_starts_with($returnObj->getApprovalCode(), 'Y:')) {
                            self::$transactionObj->lloyds_pay_response = "success";
                            self::$transactionObj->lloyds_response_payer_id = $returnObj->getOrderId();
                        }
                    }
                    $paymentSessionObj->setData(self::$transactionObj);
                } else if (isset($returnRespObj->getSecure3DResponse()->Secure3DVerificationResponse)) {
                    $verifRedirectResp = $returnRespObj->getSecure3DResponse()->Secure3DVerificationResponse->VerificationRedirectResponse;

                    self::$transactionObj->lloyds3d_c_req = $verifRedirectResp->CReq ?? '';
                    self::$transactionObj->lloyds3d_pa_req = $verifRedirectResp->PaReq ?? '';
                    self::$transactionObj->lloyds3d_md = $verifRedirectResp->MD ?? '';
                    self::$transactionObj->lloyds3d_termurl = $verifRedirectResp->TermUrl;
                    self::$transactionObj->lloyds3d_redirect_url = $verifRedirectResp->AcsURL ?? '';
                    self::$transactionObj->lloyds3d_secure = true;
                    self::$transactionObj->lloyds3d_gone_to_gateway = true;
                    $paymentSessionObj->setData(self::$transactionObj);

                    if (self::$transactionObj->lloyds3d_redirect_url) {
                        $lloydsCardsWebServObj->process3DResponseRedirect(
                            self::$transactionObj->lloyds3d_redirect_url,
                            $verifRedirectResp->TermUrl,
                            self::$transactionObj->lloyds3d_c_req,
                            self::$transactionObj->lloyds3d_pa_req,
                            self::$transactionObj->lloyds3d_md,
                        );
                    }
                }
            }

            $new_data = [
                'remote_message' => $paymentService->getPaymentByReference(self::$transactionObj->payment_reference)->remote_message . ' ' . self::$transactionObj->lloyds_response_transaction_message,
                'remote_status_or_code' => self::$transactionObj->lloyds_response_transaction_status,
                'remote_payer_id' => self::$transactionObj->lloyds_response_payer_id,
                'status' => self::$transactionObj->lloyds_pay_response === 'success' ? "SUCCESSFUL" : "FAILED"
            ];

            if (self::$transactionObj->lloyds_pay_response === 'success') {
                self::$transactionObj->transaction_reference = self::$transactionObj->lloyds_response_transaction_ref; //success processing
                $new_data['remote_reference'] = self::$transactionObj->lloyds_response_transaction_ref;
            }

            $paymentService->update($new_data, $paymentService->getPaymentByReference(self::$transactionObj->payment_reference)->id);
            return wp_redirect(site_url(self::URL_BASE . self::URL_PROCESS));
        }
    }



    /**
     * paymentCompletion function
     *
     * @return void
     */
    public function paymentCompletion()
    {
        PaymentSession::getInstance()->renewSession();
        self::$transactionObj = PaymentSession::getInstance()->getData();

        if (isset(self::$transactionObj->payment_gone_to_gateway) && self::$transactionObj->payment_gone_to_gateway) {
            $order = wc_get_order(self::$transactionObj->service_id);
            $processing = false;

            if (self::$transactionObj->service_payment_method->id == 8) {
                //Lloyds
                Utilities\MainUtility::addLog($_REQUEST, true);

                if ($this->settings['sel_type'] === self::PAYMENT_DIRECT) {
                    CardnetDirect::completePayment(self::$transactionObj, $this);
                } else {
                    $redirectPayment = CardnetRedirect::completePayment(self::$transactionObj, $this->settings);
                    if (!$redirectPayment) {
                        ChukplcCardnet::add_championsukplc_notification('Payment declined, please try again or use another card', 1);
                        wp_safe_redirect(wc_get_checkout_url());
                        die;
                    }
                }
                $paymentObj = PaymentsService::getInstance()->getPaymentByReference(self::$transactionObj->payment_reference);

                $new_data = [
                    'remote_message' => $paymentObj->remote_message . ' ' . self::$transactionObj->lloyds_response_transaction_message,
                    'remote_status_or_code' => self::$transactionObj->lloyds_response_transaction_status,
                    'remote_payer_id' => self::$transactionObj->lloyds_response_payer_id,
                    'status' => self::$transactionObj->lloyds_pay_response == "success" ? 'SUCCESSFUL' : 'FAILED'
                ];

                if (self::$transactionObj->lloyds_pay_response == "success") { //success processing
                    $processing = true;
                    $new_data['remote_reference'] = self::$transactionObj->remote_reference;
                }

                PaymentsService::getInstance()->update($new_data, $paymentObj->id);
            }

            if ($processing) {
                self::$transactionObj->payment_status = ($processing === 'pending') ? "pending" : "success";

                $prefix = (self::$transactionObj->payment_status === 'pending') ? 'Pending' : 'Successful';

                // Mark as on-hold (we're awaiting the payment)
                if ($processing === 'pending') {
                    $order->update_status('on-hold', __($prefix . ' payment confirmation', $this->id));
                } else {
                    $order->update_status('processing', __('Payment ' . $prefix, $this->id));
                    wc_reduce_stock_levels(self::$transactionObj->service_id); // Reduce stock levels
                    WC()->cart->empty_cart(); // Remove cart
                    global $woocommerce;
                    $woocommerce->cart->empty_cart();
                }
            } else {
                ChukplcCardnet::add_championsukplc_notification(("Something went wrong!"), '1');
                self::$transactionObj->payment_status = "failed";
                $order->update_status(self::$transactionObj->payment_status, __('Payment Failed', $this->id));
            }

            PaymentSession::getInstance()->setData(self::$transactionObj);
            if (self::$transactionObj->payment_status === 'pending' || self::$transactionObj->payment_status === "success") {
                return wp_redirect(self::$transactionObj->service_return_url);
            }

            return wp_redirect(self::$transactionObj->service_cancel_url);
        }

        PaymentSession::getInstance()->clearSessionData();
        return wp_redirect("/");
    }

    /**
     * Output for the order received page.
     */
    public function thankyou_page()
    {
        if ($this->instructions) {
            echo wpautop(wptexturize(esc_html($this->instructions)));
        }
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order
     * @param bool $sent_to_admin
     * @param bool $plain_text
     */
    public function email_instructions($order, $sent_to_admin)
    {
        if ($this->instructions && !$sent_to_admin && $this->id === $order->payment_method && ($order->has_status('on-hold') || $order->has_status('processing'))) {
            echo wpautop(wptexturize(esc_html($this->instructions))) . PHP_EOL;
        }
    }

    /**
     * getLloydsCardConfigObject function
     *
     * @return stdClass
     */
    public function getLloydsCardConfigObject()
    {
        $lloydsConfig = new \stdClass();
        $lloydsConfig->mode = $this->settings['sel_mode'];
        $lloydsConfig->id = ($lloydsConfig->mode === self::ENVIRONMENT_LIVE) ? 2 : 1;
        $lloydsConfig->store_id = $this->settings[strtolower($lloydsConfig->mode) . '-store-id'];
        $lloydsConfig->user_id = $this->settings[strtolower($lloydsConfig->mode) . '-user-id'];
        $lloydsConfig->password = $this->settings[strtolower($lloydsConfig->mode) . '-user-password'];
        $lloydsConfig->cert_password = $this->settings[strtolower($lloydsConfig->mode) . '-certificate-password'];

        return $lloydsConfig;
    }

    /**
     * getLloydsCardWebServObject function
     *
     * @return LloydsBankCardsNetWebService
     */
    public function getLloydsCardWebServObject()
    {
        $lloydsConfig = $this->getLloydsCardConfigObject();

        $upload_dir = wp_upload_dir();
        $cert_path = $upload_dir['basedir'] . '/chukplc-cardnet/certificates/' . strtolower($lloydsConfig->mode) . '/';
        $cert_url = $upload_dir['basedir'] . '/chukplc-cardnet/certificates/' . strtolower($lloydsConfig->mode) . '/';

        if ($lloydsConfig != null && file_exists($cert_path)) {
            $user_name = "WS" . $lloydsConfig->store_id . "._.1";

            return LloydsBankCardsNetWebService::getInstance(
                $lloydsConfig->cert_password,
                $cert_url . 'geotrust.pem',
                $user_name,
                $lloydsConfig->password,
                ($lloydsConfig->mode == self::ENVIRONMENT_TEST) ? true : false
            );
        }

        return null;
    }



    public function admin_options()
    {
        echo wp_kses("<h2>" . _e('Lloyds Cardnet by Champions UK plc', 'woocommerce') . "</h2>", ['h2' => []]);
        echo wp_kses("<ul class=\"subsubsub\">
        <li><a href=\"" . remove_query_arg('set') . "\">General Settings</a></li> |
        <li><a href=\"" . add_query_arg(['set' => strtolower(self::ENVIRONMENT_TEST)]) . "\">Test Environment</a></li> |
        <li><a href=\"" . add_query_arg(['set' => strtolower(self::ENVIRONMENT_LIVE)]) . "\">Live Environment</a></li>
        </ul>", ['ul' => ['class' => []], 'li' => [], 'a' => ['href' => []]]);
        echo wp_kses("<table class=\"form-table\">", ['table' => ['class' => []]]);
        $this->generate_settings_html([], true);
        echo wp_kses("</table>", ['table' => []]);
    }

    public function add_admin_field_file($value)
    {
        $description = \WC_Admin_Settings::get_field_description($value);
        ?>

        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr($value['id']); ?>"><?php echo esc_html($value['title']); ?></label>
                <?php echo wp_kses($description['tooltip_html'], ['strong']); ?>
            </th>

            <td class="forminp forminp-<?php echo esc_html($value['type']) ?>">
                 <input
                        name ="<?php echo esc_attr($value['name']); ?>"
                        id   ="<?php echo esc_attr($value['id']); ?>"
                        type ="file"
                        style="<?php echo esc_attr($value['css']); ?>"
                        value="<?php echo esc_attr($value['name']); ?>"
                        class="<?php echo esc_attr($value['class']); ?>"
                />
                <?php echo esc_html($description['description']); ?>
            </td>
        </tr>

    <?php
    }

    public function process_admin_options()
    {
        $this->upload_certificate_file();
        return parent::process_admin_options();
    }

    public function upload_certificate_file()
    {
        if (empty($_FILES)) {
            return false;
        }
        $upload_dir = wp_upload_dir();
        if (isset($_FILES['woocommerce_chukplccardnet_test-certificate'])) {
            $environment = strtolower(self::ENVIRONMENT_TEST);
        } else {
            $environment = strtolower(self::ENVIRONMENT_LIVE);
        }
        $cardnetDir = $upload_dir['basedir'] . '/chukplc-cardnet/certificates/' . $environment . '/';
        if (!file_exists($cardnetDir)) {
            wp_mkdir_p($cardnetDir);
            chmod($cardnetDir, 0770);
        }

        return move_uploaded_file($_FILES['woocommerce_chukplccardnet_test-certificate']['tmp_name'], $cardnetDir . 'geotrust.pem');
    }
}
