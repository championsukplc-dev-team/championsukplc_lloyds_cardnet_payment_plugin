<?php

namespace ChukplcCardnet;

require_once __DIR__ . '/Includes/include.php';
require_once __DIR__ . '/Includes/autoloader.php';
require_once __DIR__ . "/Includes/Utilities/Services/MainService.php";
require_once __DIR__ . "/plugin-update-checker/plugin-update-checker.php";

use ChukplcCardnet\Includes\Controller\PaymentReportController;
use ChukplcCardnet\Includes\Utilities\Services\MainService;
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

/*
 * Plugin Name: Champions UK plc Lloyds Cardnet Payment Gateway
 * Plugin URI: https://store.championsdigital.agency/wp-8-cardnet-payment-gateway-integration.html
 * Description: Provides a Lloyds cardnet Payment Gateway; mainly for transaction purposes.
 * Author: Champions UK plc
 * Author URI: https://championsukplc.com/
 * Text Domain: championsukplc-cardnet
 * Version: 4.0.6
 *
 *
 * We load it later to ensure WC is loaded first since we're extending it.
 *
 * @class       ChukplcCardnet
 * @extends     WC_Payment_Gateway
 * @package     WooCommerce/Classes/Payment
 */
class ChukplcCardnet
{
    protected $options = [];
    protected $woocommerce;

    public function __construct()
    {
        $updateChecker = PucFactory::buildUpdateChecker(
            'https://bitbucket.org/championsukplc-dev-team/championsukplc_lloyds_cardnet_payment_plugin',
            __FILE__,
            'championsukplc-cardnet'
        );
        $updateChecker->setBranch('stable');

        add_action('plugins_loaded', [$this, 'initGatewayClass'], 11);
        add_filter('woocommerce_payment_gateways', [$this, 'addGatewayClass']);
        add_action('init', [$this, 'rewriteRules']);
        add_filter('query_vars', [$this, 'queryVars']);
        add_action('template_redirect', [$this, 'templateRedirect']);
        add_action('woocommerce_before_checkout_form', [$this, 'get_championsukplc_notifications'], 10);
        register_activation_hook(__FILE__, [$this, 'install']);
    }


    public function rewriteRules()
    {
        add_rewrite_tag('custom', '([^/]*)');
        add_rewrite_rule('chukplc-cardnet/confirmation/?$', 'index.php?pagename=chukplc-confirmation&custom=confirmresponse&return=true', 'top');
        add_rewrite_rule('chukplc-cardnet/process/?$', 'index.php?pagename=chukplc-process&custom=process&return=true', 'top');
        add_rewrite_rule('chukplc-cardnet/notification/([^/]*)/([^/]*)?$', 'index.php?pagename=chukplc-notification&custom=notification&orderid=$matches[1]&transactionTime=$matches[2]', 'top');

        // 3DS redirects
        add_rewrite_rule('chukplc-cardnet/confirmation3ds/?$', 'index.php?pagename=chukplc-confirmation&custom=confirmresponse3ds&return=true', 'top');
        add_rewrite_rule('chukplc-cardnet/process3ds/?$', 'index.php?pagename=chukplc-process3ds&custom=process3ds&return=true', 'top');
        add_rewrite_rule('chukplc-cardnet/threed/?$', 'index.php?pagename=chukplc-threed&custom=threed&return=true', 'top');
        add_rewrite_rule('chukplc-cardnet/threed/([^/]*)/([^/]*)?$', 'index.php?pagename=chukplc-threed&custom=threed&return=true', 'top');

        flush_rewrite_rules();
    }

    public function queryVars($query_vars)
    {
        $query_vars[] = 'custom';
        $query_vars[] = 'MD';
        $query_vars[] = 'PaRes';
        $query_vars[] = 'cres';
        $query_vars[] = 'approval_code';
        $query_vars[] = 'oid';
        $query_vars[] = 'refnumber';
        $query_vars[] = 'status';
        $query_vars[] = 'response_hash';
        $query_vars[] = 'processor_response_code';
        $query_vars[] = 'response_code_3dsecure';
        $query_vars[] = 'orderid';
        $query_vars[] = 'currency';
        $query_vars[] = 'merchantTransactionId';
        $query_vars[] = 'transactionTime';
        $query_vars[] = 'notification_hash';
        $query_vars[] = 'email';
        $query_vars[] = 'chargetotal';
        $query_vars[] = 'bname';
        $query_vars[] = 'pagenum';
        $query_vars[] = 'return';
        $query_vars[] = 'set';

        return $query_vars;
    }

    public function templateRedirect()
    {
        $custom = get_query_var('custom');
        if ($custom == 'confirmresponse') {
            include plugin_dir_path(__FILE__) . 'templates/confirmresponse.php';
            die;
        }
        if ($custom == 'confirmresponse3ds') {
            include plugin_dir_path(__FILE__) . 'templates/confirmresponse3ds.php';
            die;
        }
        if ($custom == 'process') {
            include plugin_dir_path(__FILE__) . 'templates/process_cardnet_payment.php';
            die;
        }
        if ($custom == 'process3ds') {
            include plugin_dir_path(__FILE__) . 'templates/three_d_process_cardnet_iframe_response.php';
            die;
        }
        if ($custom == 'threed') {
            include plugin_dir_path(__FILE__) . 'templates/three_d_process_cardnet_payment.php';
            die;
        }
        if ($custom == 'notification') {
            include plugin_dir_path(__FILE__) . 'templates/notificationUrl.php';
            die;
        }
    }

    public function uninstall()
    {
        MainService::getInstance()->customQuery("DROP TABLE IF EXISTS championsukplc_cardnet_payments");
        MainService::getInstance()->customQuery("DROP TABLE IF EXISTS championsukplc_lookup");
    }

    public function install()
    {
        require_once __DIR__ . '/data/championsukplc_cardnet_preset_data.php';
    }

    /**
     * addGatewayClass function
     *
     * @param [type] $gateways
     * @return void
     */
    public function addGatewayClass($gateways)
    {
        $gateways[] = 'ChukplcCardnet\Includes\WC_Cardnet_Gateway';

        return $gateways;
    }

    /**
     * initGatewayClass function
     *
     * @return void
     */
    public function initGatewayClass()
    {
        require_once __DIR__ . '/Includes/class-wc-cardnet.php';
    }

    public function paymentsReport()
    {
        if (!current_user_can('manage_options')) {
            return false;
        }

        $paymentReport = new PaymentReportController();
        $paymentReport->listPayments(get_query_var('pagenum', '0') <= 0 ? 1 : get_query_var('pagenum', '0'));
    }

    public static function add_championsukplc_notification($message, $code = 0)
    {
        $classes = [
            '0' => 'woocommerce-success',
            '1' => 'woocommerce-error',
            '2' => 'woocommerce-warning'
        ];

        if (!isset($_SESSION['championsukplc_notification'])) {
            $_SESSION['championsukplc_notification'] = [];
        }

        $_SESSION['championsukplc_notification'][] = "<div class=" . $classes[$code] . ">" . $message . "</div>";
    }

    public static function get_championsukplc_notifications()
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        if (isset($_SESSION['championsukplc_notification']) && !empty($_SESSION['championsukplc_notification'])) {
            foreach ($_SESSION['championsukplc_notification'] as $notice) {
                echo wp_kses($notice, ['div' => ['class']]);
            }
        }
        unset($_SESSION['championsukplc_notification']);
    }
}

new ChukplcCardnet();
